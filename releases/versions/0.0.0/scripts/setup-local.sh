#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description :                                                               #
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                        Setup HDFS Layer Directories                         #
###############################################################################

fn_create_local_directory "${AZKABAN_TMP_DIR}/orbi_full_load_girish_gold_ingestion"

# CREATING FLOW IN ENV USING COORDINATOR SCRIPTS

bash ${COORDINATOR_HOME}/bin/create_flow.sh FLOW_ID="" FLOW_NAME="" FLOW_DESC=""
bash ${COORDINATOR_HOME}/bin/add_job.sh JOB_ID="${DB_WORK}_raw_atg_kls_incentive_JOB" JOB_NAME="${DB_WORK}_raw_atg_kls_incentive_JOB" JOB_DESC="${DB_WORK}_raw_atg_kls_incentive_JOB" JOB_TYPE="shell" FLOW_ID="${DB_WORK}_raw_atg_kls_incentive_FLOW"

################################################################################
#                                     End                                      #
################################################################################


