#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     : It executes component specific setup scripts.                         #
#       Setup scripts should be present in the scripts folder. For very       #
#       compoenent there shall be seperate shell script. Setup scripts may    #
#       create, update, delete component specific metadata.                   #
#       User can only run component specific scripts by passing the second    #
#       argument to this script. All component scripts can be executed by     #
#       passign option all to this script.                                    #
#                                                                             #
#                                                                             #
###############################################################################
#                           Identify Script Home                              #
###############################################################################


################################################################################
#                                     End                                      #
################################################################################

