DROP TABLE IF EXISTS db_gold.INDUS_LOS_HFS_EE; CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_HFS_EE(
S_no string,
LOS_NUMBER string,
Branch string,
Login_date_DD_MM_YYYY string,
Channel_Name string,
Clix_Sales_Manager string,
FTR_Yes_No string,
If_not_list_what_is_incomplete_or_Pending string,
File_Accepted_as_Login_Y_N string,
EBanking_PDF_Y_N string,
Login_Queries_Yes_No string,
Queries_Initiated_Date_DD_MM_YYYY string,
Queries_Received_Date_DD_MM_YYYY string,
Are_there_login_query_which_impacts_credit_processing_Yes_No string,
Product string,
Asset string,
Assessment_Program_type_Login string,
Applicant_Name string,
DI_Non_DI string,
Start_up_Non_Start_up string,
Prop_Individual_Pvt_Ltd_Ltd_Society_Trust string,
Customer_Segment_SEP_SENP_Salaried string,
Applied_Amount_in_INR_Lacs string,
IMD_Cheque_No string,
IMD_Deposit_Date string,
IMD_Cheque_Amount string,
Status_of_IMD_Cheque string,
CAM_CET_Preperation_Date_DD_MM_YYYY string,
PD_or_Tele_PD_Date_DD_MM_YYYY string,
FI_initiation_Date_DD_MM_YYYY string,
FI_receiving_Date_DD_MM_YYYY string,
3rd_Party_PD_initiation_Date_DD_MM_YYYY string,
3rd_Party_PD_receiving_Date_DD_MM_YYYY string,
Credit_Decision_Date_DD_MM_YYYY string,
Credit_Decision_Approved_Reject_Hold_Recommended string,
If_recommended_then_Recommendation_Date_DD_MM_YYYY string,
If_rejected_Rejection_reason string,
Final_Loan_Amt_In_INR_Lac string,
Loan_Approved_By string,
PSL_Classification string,
Legal_Fired_Date string,
Legal_Recevie_date string,
Legal_Status_Remarks string,
Legal_TAT string,
Legal_Query_Initiate_Date_DD_MM_YYYY string,
Legal_Query_resolution_date_DD_MM_YYYY string,
Final_Legal_report_receipt_date_DD_MM_YYYY string,
Technical_1_Initiation_date_DD_MM_YYYY string,
Technical_1_report_receipt_date_DD_MM_YYYY string,
Technical_2_Initiation_date_DD_MM_YYYY string,
Technical_2_report_receipt_date_DD_MM_YYYY string,
Technical_Positive_Negative_Query string,
Technical_Query_Initiate_Date_DD_MM_YYYY string,
Technical_Query_resolution_date_DD_MM_YYYY string,
Final_Technical_report_receipt_date_DD_MM_YYYY string,
Final_Credit_Decision_Approved_Reject_Hold string,
Final_Credit_Decision_Date_DD_MM_YYYY string,
Final_Loan_Amt_In_INR_Lac_1 string,
Docket_Recevied_date_DD_MM_YYYY string,
FTR_FTNR string,
Queries string,
Queries_Issue_Date_DD_MM_YYYY string,
Query_Completed_Date_DD_MM_YYYY string,
Docket_Sent_to_OPS_date_DD_MM_YYYY string,
Disbursal_Date_DD_MM_YYYY string,
Loan_Lease_HP string,
Login_to_Login_Query string,
Query_Resolution_TAT string,
Final_Login_to_Credit_Decision string,
Login_to_credit_decision string,
Login_Query_till_Date string,
file_load_timestamp string
)
PARTITIONED BY (batch_id string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS  PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_HFS_EE'
tblproperties ("parquet.compression"="SNAPPY");
