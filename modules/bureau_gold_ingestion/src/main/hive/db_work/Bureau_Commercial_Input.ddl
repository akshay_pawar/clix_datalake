set hive.msck.path.validation=ignore;
DROP TABLE IF EXISTS db_work.Bureau_Commercial_Input;CREATE EXTERNAL TABLE IF NOT EXISTS db_work.Bureau_Commercial_Input (
application_no string,
customer_no string,
account_no string,
name string,
phone_no string,
email_id string,
address string,
city string,
state string,
pin_code string,
product string,
pan_of_entity string
)
PARTITIONED BY (batch_id string,data_date string) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' WITH SERDEPROPERTIES (   "separatorChar" = ",",   "quoteChar"     = "\"",   "escapeChar"    = "\\")  STORED AS TEXTFILE LOCATION '/raw/Bureau/db_work/version=0/Commercial_Input' tblproperties ("skip.header.line.count"="1");msck repair table db_work.Bureau_Commercial_Input;
