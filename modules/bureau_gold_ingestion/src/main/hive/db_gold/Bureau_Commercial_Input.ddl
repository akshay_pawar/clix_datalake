DROP TABLE IF EXISTS db_gold.Bureau_Commercial_Input;CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.Bureau_Commercial_Input (
application_no string,
customer_no string,
account_no string,
name string,
phone_no string,
email_id string,
address string,
city string,
state string,
pin_code string,
product string,
pan_of_entity string,
file_load_timestamp string
)
PARTITIONED BY (batch_id string,data_date string) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' STORED AS parquet LOCATION '/hc/Bureau/hive/db_gold/Bureau_Commercial_Input' tblproperties ("parquet.compression"="SNAPPY");
