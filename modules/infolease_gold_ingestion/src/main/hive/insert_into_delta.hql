
set hive.exec.dynamic.partition=true;
INSERT INTO TABLE ${hivevar:DB_GOLD}.${hivevar:GOLD_TABLE} PARTITION (batch_id=${hivevar:L2_BATCH},data_date)
SELECT ${hivevar:COLUMNS}, "${hivevar:file_load_timestamp}" AS file_load_timestamp,data_date
FROM ${hivevar:DB_WORK}.${hivevar:WORK_TABLE} WHERE batch_id > ${hivevar:L1_BATCH} ;