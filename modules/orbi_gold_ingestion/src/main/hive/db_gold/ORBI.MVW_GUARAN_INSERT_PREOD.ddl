DROP TABLE IF EXISTS db_gold.ORBI_MVW_GUARAN_INSERT_PREOD ;
CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.ORBI_MVW_GUARAN_INSERT_PREOD
( 
	MPA_ASSOCIATE_ID string, 
	MPA_ASSOCIATE_NAME string, 
	MPA_ASSOCIATE_PER decimal(10,7), 
	MPA_CUST_ID_or_MPA_CON_ID_or_MPA_P string, 
	MPA_CUST_ID string, 
	MPOD_BUSINESS string, 
	MCA_ADD_TYPE string, 
	MCA_ADD_LN1 string, 
	MCA_ADD_LN2 string, 
	MCA_ADD_LN3 string, 
	MCA_ADD_LN4 string, 
	MCA_CITY string, 
	MCA_STATE string, 
	MCA_ZIP_CD string, 
	MCA_TEL1 string, 
	MCA_TEL2 string, 
	MCA_TEL3 string, 
	MCA_TEL4 string, 
	MCA_FAX1 string, 
	MCA_FAX2 string, 
	MCA_EMAIL1 string, 
	MCA_EMAIL2 string, 
	I string, 
	COLLTRANS3 string,
        file_load_timestamp string 
)  
PARTITIONED BY (batch_id string) 
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\001' 
LINES TERMINATED BY '\n' 
STORED AS PARQUET
LOCATION '/hc/ORBI/hive/db_gold/orbi_mvw_guaran_insert_preod'
tblproperties ("parquet.compression"="SNAPPY");
