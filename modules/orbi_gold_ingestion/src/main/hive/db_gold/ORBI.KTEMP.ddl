DROP TABLE IF EXISTS db_gold.TEMP_ORBI_KTEMP ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.TEMP_ORBI_KTEMP ( LEGACY string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/TEMP_ORBI/hive/db_gold/TEMP_ORBI_KTEMP'
tblproperties ("parquet.compression"="SNAPPY")
