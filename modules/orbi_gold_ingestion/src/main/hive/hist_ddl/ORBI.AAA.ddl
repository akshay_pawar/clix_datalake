DROP TABLE IF EXISTS db_gold.TEMP_ORBI_AAA_history ;
CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.TEMP_ORBI_AAA_history
( 
	MPI_LEGACY_ID string, 
	Outstanding_Instalment decimal(3,0), 
	EMI_Amount decimal(31,6), 
	FUTURE_PRINCIPAL_plus_Debtors decimal(38,0), 
	Total_amt_received_from_cust decimal(38,0),  
	AFC_Charges_plus_BCC_plus_UMFC decimal(38,0) , file_load_timestamp STRING ) PARTITIONED BY (batch_id string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\001' 
LINES TERMINATED BY '\n' 
STORED AS PARQUET
LOCATION '/hc/TEMP_ORBI/hive/db_gold/TEMP_ORBI_AAA_history'
tblproperties ("parquet.compression"="SNAPPY");
