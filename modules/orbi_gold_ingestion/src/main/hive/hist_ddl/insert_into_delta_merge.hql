INSERT INTO TABLE ${hivevar:DB_GOLD}.${hivevar:GOLD_TABLE} PARTITION (batch_id=${hivevar:CURRENT_L2_BATCH})
SELECT ${hivevar:ALL_COLUMNS},"${hivevar:file_load_timestamp}" AS file_load_timestamp
FROM
  ( SELECT ${hivevar:ALL_COLUMNS},batch_id,ROW_NUMBER() over (PARTITION BY ${hivevar:PRIMARY_COLUMNS} ORDER BY batch_id DESC) AS row_num
   FROM
     (SELECT ${hivevar:ALL_COLUMNS},batch_id FROM ${hivevar:DB_WORK}.${hivevar:WORK_TABLE} WHERE batch_id > ${hivevar:L1_BATCH}
      UNION ALL
      SELECT ${hivevar:ALL_COLUMNS},batch_id FROM ${hivevar:DB_GOLD}.${hivevar:HIST_TABLE} WHERE batch_id=${hivevar:L2_BATCH} )query1 )ranked_rows
WHERE ranked_rows.row_num==1