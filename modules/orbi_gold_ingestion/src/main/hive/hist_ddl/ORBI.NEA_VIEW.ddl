DROP TABLE IF EXISTS db_gold.ORBI_NEA_VIEW_history ;
CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.ORBI_NEA_VIEW_history
( 
CUST_ID string,CON_ID string,PROJ_ID string,LEGACY string,MPI_LOAN_TYPE string,BRANCH string,CUST_NAME string,REGION string,PROD string,DISB_DATE string,PROJ_COST decimal(38,2),MAT_DT string,FUTURE_PRIN decimal(38,2),DEBT_DR decimal(38,2),DEBT_CR  decimal(38,2),NEA  decimal(38,2),DUES_OS  decimal(38,2),RPA  decimal(38,2),DELIN_RBI  decimal(38,2),FIX_FLOAT string,RATE decimal(38,2),COF decimal(38,2),RISK_FAC string,GROUP_CODE string,INDUSTRY  string,KMV string,RISK_PLUS string,DIVISION  string,STATUS    string,AGREEMENT_DATE string,FUTURE_RECEIVABLE  decimal(38,2),UMFC decimal(38,2),INT_ACCRUED_NOT_DUE decimal(38,2),SERVICE_TAX_OS decimal(38,2),LEASE_TAX_OS decimal(38,2),ACC_CHG_OS decimal(38,2),SOLD_FLAG string,RECOURSE_FLAG  string,REPO_FLAG string,DELIN_USG decimal(38,2),PRIN_INT decimal(38,2),REPO_FUTURE_PRIN   decimal(38,2),REPO_UMFC decimal(38,2),WRITEOFF decimal(38,2),PRIN_SHORTFALL decimal(38,2),SERVICE_TAX_BORNE_BY string,REPO_SHORTFALL decimal(38,2),FUTURE_UMFC decimal(38,2),REMAINING_TENOR decimal(38,2),DEAL_IRR   decimal(38,2),PORTFOLIO string,UMFC_WAIVED decimal(38,2),REPOSTOCK  decimal(38,2),RBI_INCOME_DERECGN decimal(38,2),USG_INCOME_DERECGN decimal(38,2),AFC_PAID decimal(38,2),NONACCCHG_PAID decimal(38,2),MPI_SOLD_FLAG string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\001' 
LINES TERMINATED BY '\n' 
STORED AS PARQUET
LOCATION '/hc/ORBI/hive/db_gold/orbi_nea_view_history'
tblproperties ("parquet.compression"="SNAPPY");
