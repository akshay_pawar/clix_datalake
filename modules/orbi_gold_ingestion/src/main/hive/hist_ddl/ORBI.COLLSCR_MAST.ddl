DROP TABLE IF EXISTS db_gold.TEMP_ORBI_COLLSCR_MAST_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.TEMP_ORBI_COLLSCR_MAST_history ( CSM_PROD_CODE string, CSM_SCR_NAME string, CSM_SCR_POS string, CSM_PARTOF string, CSM_LAYOUT string, CSM_EDIT_PGM string, CSM_HISTORY_PGM string, CSM_STATUS string, CSM_MAKER string, CSM_MAKER_DT string, CSM_AUTH string, CSM_AUTH_DT string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/TEMP_ORBI/hive/db_gold/TEMP_ORBI_COLLSCR_MAST_history'
tblproperties ("parquet.compression"="SNAPPY")
