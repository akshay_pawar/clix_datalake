DROP TABLE IF EXISTS db_work.ORBI_AAA ;
CREATE EXTERNAL TABLE IF NOT EXISTS db_work.ORBI_AAA (
    MPI_LEGACY_ID string,
	Outstanding_Instalment decimal(3,0),
	EMI_Amount decimal(31,6),
	FUTURE_PRINCIPAL_plus_Debtors decimal(38,0),
	Total_amt_received_from_cust decimal(38,0),
	AFC_Charges_plus_BCC_plus_UMFC decimal(38,0)
)
PARTITIONED BY (batch_id string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\001'
LINES TERMINATED BY '\n' STORED AS TEXTFILE
LOCATION '/raw/ORBI/db_work/version=0/ORBI_AAA' ;
msck repair table db_work.ORBI_AAA;
