#!/bin/bash
###############################################################################
#                               General Details                               #
###############################################################################
#                                                                             #
# Name         : L2_delta_load.sh                                             #
#                                                                             #
# Description  : This script ingests data from raw table to gold layer        #
#                                                                             #
# Author       : <AKSHAY PAWAR>                                               #
#                                                                             #
# Create Date  :                                                              #
#                                                                             #
###############################################################################
#                          Script Environment Setup                           #
###############################################################################

kinit -Vkt /home/dm_infra/dm_infra.keytab dm_infra@CLIXCDH.COM
SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# EXTRACT MODULE HOME FROM SCRIPT PATH
#MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"
MODULE_HOME=/clix/artifacts/clix-datalake/orbi_gold_ingestion
echo "MODULE_HOME = $MODULE_HOME"

###############################################################################
#                           Import Dependencies                               #
###############################################################################

. ${MODULE_HOME}/../common/bin/import-dependecies.sh
. ${MODULE_HOME}/etc/module.env.properties
. ${MODULE_HOME}/etc/column.list.properties

###############################################################################
#                               Module Start                                  #
###############################################################################

# READING PARAMETERS FROM AZKABAN
fn_read_command_line_parameters "$@"

echo "OWNER_NAME : ${OWNER_NAME}"
echo "TABLE_NAME : ${TABLE_NAME}"

L1_FLOW_ID=l1_flow_ORBI.${TABLE_NAME}_${INGESTION_ID}
L1_JOB_ID=l1_job_ORBI.${TABLE_NAME}

L2_FLOW_ID=l2_flow_ORBI.${TABLE_NAME}_${INGESTION_ID}
L2_JOB_ID=l2_job_ORBI.${TABLE_NAME}

echo "L1_FLOW_ID : ${L1_FLOW_ID}"
echo "L1_JOB_ID : ${L1_JOB_ID}"

echo "L2_FLOW_ID : ${L2_FLOW_ID}"
echo "L2_JOB_ID : ${L2_JOB_ID}"

# RUN JOB INSTANCE
fn_log_info "-------------- RUNNING JOB INSTANCE --------------"
RUNNING_JOB_INSTANCE_ID=$(fn_run_job ${L2_FLOW_ID} ${L2_JOB_ID})
fn_check_and_exit_error_response "${RUNNING_JOB_INSTANCE_ID}" "COORDINATOR COMMAND SUCCESSFUL" "COORDINATOR COMMAND FAILED"
fn_log_info "RUNNING_JOB_INSTANCE_ID : $RUNNING_JOB_INSTANCE_ID"

# GET CURRENT RUNNING BATCH
fn_log_info "-------------- GETTING CURRENT RUNNING BATCH --------------"
RUNNING_BATCH_ID_L2=$(fn_get_running_batch_id "${L2_FLOW_ID}")
fn_check_batch_id ${RUNNING_BATCH_ID_L2}

# EXPOSE A TEMPORARY TABLE AT L1 LOCATION
fn_log_info "-------------- EXPOSING A TEMPORARY TABLE AT L1 LOCATION --------------"
#hive
	beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \ 
	 --hivevar DB_WORK="db_work" \
     --hivevar WORK_TABLE=${OWNER_NAME}_${TABLE_NAME} \
     -f ${MODULE_HOME}/hive/db_work/${OWNER_NAME}.${TABLE_NAME}.ddl
if [ $? -eq 0 ] ;
then
    fn_log_info "SUCCESSFULLY EXPOSED TEMP TABLE db_work.${OWNER_NAME}_${TABLE_NAME} AT L1 LOCATION.";
else
    fn_log_error "FAILED TO EXPOSE TEMP TABLE db_work.${OWNER_NAME}_${TABLE_NAME} AT L1 LOCATION. ABORTING THE JOB !!";
    response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
    exit 100
fi

# GET LAST PROCESSED L1 BATCH BY L2 FLOW
LAST_PROCESSED_L1_BATCH=$(fn_get_job_event_value "${L2_JOB_ID}" "LAST_PROCESSED_L1_BATCH")
fn_log_info "LAST_PROCESSED_L1_BATCH : ${LAST_PROCESSED_L1_BATCH}"
if [ -z "${LAST_PROCESSED_L1_BATCH}" ] || [ "${LAST_PROCESSED_L1_BATCH}" = "" ] || [[ "${LAST_PROCESSED_L1_BATCH}" = *"error"* ]] || [[ "${LAST_PROCESSED_L1_BATCH}" = *"ERROR"* ]];
then
     fn_log_error "LAST_PROCESSED_L1_BATCH IS NULL ...!!!"
     response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
     fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
     exit 1
fi

# GET LAST_SUCCESSFUL_BATCH OF L1 FLOW
fn_log_info "-------------- FETCHING LAST_SUCCESSFUL_BATCH OF L1 FLOW --------------"
LAST_SUCCESSFUL_L1_BATCH_ID=$(fn_get_last_successful_batch_id ${L1_FLOW_ID} ${L1_JOB_ID})
if [ -z "${LAST_SUCCESSFUL_L1_BATCH_ID}" ] || [ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = "" ] || [[ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = *"error"* ]] || [[ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = *"ERROR"* ]] ;
then
     fn_log_error "LAST_SUCCESSFUL_L1_BATCH_ID : ${LAST_SUCCESSFUL_L1_BATCH_ID}"
     fn_log_error "LAST_SUCCESSFUL_L1_BATCH_ID IS NULL. FIRST RUN L1 FLOW THEN RUN L2 FLOW. THERE IS NO DATA TO LOAD IN GOLD. ABORTING THE JOB !!"
     response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
     fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
     exit 100
else
    fn_log_info "LAST_SUCCESSFUL_L1_BATCH_ID : ${LAST_SUCCESSFUL_L1_BATCH_ID}"
fi

# GET LAST_SUCCESSFUL_BATCH OF L2 FLOW
fn_log_info "-------------- FETCHING LAST_SUCCESSFUL_BATCH OF L2 FLOW --------------"
LAST_SUCCESSFUL_L2_BATCH_ID=$(fn_get_last_successful_batch_id ${L2_FLOW_ID} ${L2_JOB_ID})
if [ -z "${LAST_SUCCESSFUL_L2_BATCH_ID}" ] || [ "${LAST_SUCCESSFUL_L2_BATCH_ID}" = "" ] || [[ "${LAST_SUCCESSFUL_L2_BATCH_ID}" = *"error"* ]] || [[ "${LAST_SUCCESSFUL_L2_BATCH_ID}" = *"ERROR"* ]] ;
then
     LAST_SUCCESSFUL_L2_BATCH_ID=0
fi
fn_log_info "LAST_SUCCESSFUL_L2_BATCH_ID : ${LAST_SUCCESSFUL_L2_BATCH_ID}"

# MOVE LAST_SUCCESSFUL_L2_BATCH_ID FROM GOLD_TABLE TO HISTORY_TABLE
fn_log_info "-------------- MOVE LAST_SUCCESSFUL_L2_BATCH_ID FROM GOLD_TABLE TO HISTORY_TABLE --------------"
#gold_table_path=`hive -e "describe extended db_gold.${OWNER_NAME}_${TABLE_NAME}" | perl -n -e'while(/location:([\_0-9a-zA-Z:\/\/-]+),/g) {print "$1\n"}'`
gold_table_path=`beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -e "describe extended db_gold.${OWNER_NAME}_${TABLE_NAME}" | perl -n -e'while(/location:([\_0-9a-zA-Z:\/\/-]+),/g) {print "$1\n"}'`
echo "gold_table_path = $gold_table_path"
#history_table_path=`hive -e "describe extended db_gold.${OWNER_NAME}_${TABLE_NAME}_history" | perl -n -e'while(/location:([\_0-9a-zA-Z:\/\/-]+),/g) {print "$1\n"}'`
history_table_path=`beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -e "describe extended db_gold.${OWNER_NAME}_${TABLE_NAME}_history" | perl -n -e'while(/location:([\_0-9a-zA-Z:\/\/-]+),/g) {print "$1\n"}'`
echo "history_table_path = $history_table_path"
hdfs dfs -mv ${gold_table_path}/batch_id=${LAST_SUCCESSFUL_L2_BATCH_ID} ${history_table_path}/
if [ $? -eq 0 ];
then
    fn_log_info "SUCCESSFULLY MOVED DATA FROM db_gold.${OWNER_NAME}_${TABLE_NAME} TO db_gold.${OWNER_NAME}_${TABLE_NAME}_history"
    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	-e "use db_gold; MSCK REPAIR TABLE ${OWNER_NAME}_${TABLE_NAME}_history;"
    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	-e "use db_gold; ALTER TABLE ${OWNER_NAME}_${TABLE_NAME} DROP IF EXISTS PARTITION(batch_id=${LAST_SUCCESSFUL_L2_BATCH_ID})"
else
    if [ "${LAST_SUCCESSFUL_L2_BATCH_ID}" -eq 0 ]
    then
        fn_log_warn "FAILED TO MOVE DATA FROM db_gold.${OWNER_NAME}_${TABLE_NAME} TO db_gold.${OWNER_NAME}_${TABLE_NAME}_history, AS THIS IS 1ST RUN."
    else
        fn_log_error "FAILED TO MOVE DATA FROM db_gold.${OWNER_NAME}_${TABLE_NAME} TO db_gold.${OWNER_NAME}_${TABLE_NAME}_history. ABORTING THE JOB !!"
        response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
        fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
        exit 100
    fi
fi

file_load_tmst=`date +"%Y-%m-%d %H:%M:%S"`

# INSERT DATA INTO GOLD TABLE
fn_log_info "-------------- INSERT DATA INTO GOLD TABLE --------------"
ALL_COL=${OWNER_NAME}_${TABLE_NAME}_COL
PK_COL=${OWNER_NAME}_${TABLE_NAME}_PK
#hive 
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	 --hivevar DB_WORK="db_work" \
     --hivevar DB_GOLD="db_gold" \
     --hivevar WORK_TABLE="${OWNER_NAME}_${TABLE_NAME}" \
     --hivevar GOLD_TABLE="${OWNER_NAME}_${TABLE_NAME}" \
     --hivevar HIST_TABLE="${OWNER_NAME}_${TABLE_NAME}_history" \
     --hivevar CURRENT_L2_BATCH="${RUNNING_BATCH_ID_L2}" \
     --hivevar L1_BATCH="${LAST_PROCESSED_L1_BATCH}" \
     --hivevar L2_BATCH="${LAST_SUCCESSFUL_L2_BATCH_ID}" \
     --hivevar ALL_COLUMNS="${!ALL_COL}" \
     --hivevar PRIMARY_COLUMNS="${!PK_COL}" \
     --hivevar file_load_timestamp="${file_load_tmst}" \
     -f ${MODULE_HOME}/hive/insert_into_delta_merge.hql
if [ $? -ne 0 ];
then
    fn_log_error "FAILED TO PERFORM INSERT INTO TABLE : ${OWNER_NAME}_${TABLE_NAME}"

    # DROP TEMP TABLE FROM L1 PATH
    fn_log_info "-------------- DROPPING TEMP TABLE FROM L1 PATH --------------"
    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	--hivevar DB_WORK="db_work" \
	--hivevar WORK_TABLE="${OWNER_NAME}_${TABLE_NAME}" \
	-f ${MODULE_HOME}/hive/drop_table.hql
    if [ $? -eq 0 ];
    then
        fn_log_info "DROPPED TABLE db_work.${OWNER_NAME}_${TABLE_NAME} FROM L1 PATH."
    else
        fn_log_warn "FAILED TO DROP TABLE db_work.${OWNER_NAME}_${TABLE_NAME} FROM L1 PATH."
    fi

    # REVERT MOVE
    fn_log_info "-------------- REVERT MOVE --------------"
    hdfs dfs -mv ${history_table_path}/batch_id=${LAST_SUCCESSFUL_L2_BATCH_ID} ${gold_table_path}/
    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	-e "use db_gold; ALTER TABLE ${OWNER_NAME}_${TABLE_NAME}_history DROP IF EXISTS PARTITION(batch_id=${LAST_SUCCESSFUL_L2_BATCH_ID})"
    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	-e "use db_gold; MSCK REPAIR TABLE ${OWNER_NAME}_${TABLE_NAME};"

    # FAIL JOB INSTANCE
    fn_log_info "-------------- MARKING THE JOBINSTANCE FAILED --------------"
    response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 100
else
    fn_log_info "SUCCESSFULLY PERFORMED INSERT INTO TABLE : ${OWNER_NAME}_${TABLE_NAME}"
fi

# DROP TEMP TABLE FROM L1 PATH
fn_log_info "-------------- DROPPING TEMP TABLE FROM L1 PATH --------------"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
 --hivevar DB_WORK="db_work" \
 --hivevar WORK_TABLE="${OWNER_NAME}_${TABLE_NAME}" \
 -f ${MODULE_HOME}/hive/drop_table.hql
if [ $? -eq 0 ];
then
    fn_log_info "DROPPED TABLE db_work.${OWNER_NAME}_${TABLE_NAME} FROM L1 PATH."
else
    fn_log_warn "FAILED TO DROP TABLE db_work.${OWNER_NAME}_${TABLE_NAME} FROM L1 PATH."
fi


#DATA PURGING
#WORK_TABLE_PATH=`hive -e "show create table db_work.${OWNER_NAME}_${TABLE_NAME}"| grep "LOCATION" -A1|grep 'hdfs'|sed "s/'//g"`
WORK_TABLE_PATH=`beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -e "show create table db_work.${OWNER_NAME}_${TABLE_NAME}"| grep "LOCATION" -A1|grep 'hdfs'|sed "s/'//g"`
fn_log_info "WORK_TABLE_PATH : ${WORK_TABLE_PATH}"

last_successful_value=1970-01-01
last_successful_value=$(mysql -u ${USER_NAME} -p${PASSWORD} -e "set  @i:=0;use ${DB_NAME};select * from(select end_time, @i:=@i+1 AS rank from(SELECT end_time  FROM FLOWINSTANCE  where flow_id like 'l2_flow_CLIX_LOS_PROD.${TABLE_NAME}%' and status='successful')a ORDER BY a.end_time DESC)get_end_time where get_end_time.rank=${NUMBER_DAYS}"|tail -n 1|cut -d' ' -f1)

fn_log_info "PURGING ALL FILES BEFORE ${last_successful_value}"

hdfs dfs -ls ${WORK_TABLE_PATH} | grep "^d" | while read line ; do
dir_date=$(echo ${line} | awk '{print $6}')
#difference=$(( ( ${today} - $(date -d ${dir_date} +%s) ) / ( 24*60*60 ) ))
filePath=$(echo ${line} | awk '{print $8}')
if [[ ${dir_date} < ${last_successful_value} ]]; then
   hdfs dfs -rm -r $filePath
fn_log_info "DELETED PATH : $filePath"
fi
done


# MARK THE JOBINSTANCE SUCCESSFUL
fn_log_info "-------------- MARKING THE JOBINSTANCE SUCCESSFUL --------------"
fn_add_job_event "LAST_PROCESSED_L1_BATCH" "${LAST_SUCCESSFUL_L1_BATCH_ID}" ${L2_JOB_ID}
response=$(fn_successful_running_job_instance "${L2_JOB_ID}")
fn_check_and_exit_error_response "${response}" "The job instance is made successful" "Unable to make the job instance successful"

###############################################################################
#                               Module End                                    #
###############################################################################