#!/bin/bash
###############################################################################
#                               General Details                               #
###############################################################################
#                                                                             #
# Name         :                                                              #
#                                                                             #
# Description  : This script ingests data from ATG table and puts on L1 layer #
#                                                                             #
# Author       : <AKSHAY PAWAR>                                               #
#                                                                             #
# Create Date  :                                                              #
#                                                                             #
###############################################################################
#                          Script Environment Setup                           #
###############################################################################
kinit -Vkt /home/dm_infra/dm_infra.keytab dm_infra@CLIXCDH.COM
SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# EXTRACT MODULE HOME FROM SCRIPT PATH
MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"
fn_log_info  "MODULE_HOME = $MODULE_HOME"

###############################################################################
#                           Import Dependencies                               #
###############################################################################

. ${MODULE_HOME}/../common/bin/import-dependecies.sh
. ${MODULE_HOME}/etc/row_id.properties
. ${MODULE_HOME}/etc/module.env.properties

###############################################################################
#                               Module Start                                  #
###############################################################################

# READING PARAMETERS FROM AZKABAN
fn_read_command_line_parameters "$@"

fn_log_info "OWNER = ${OWNER}"
fn_log_info "TABLE_NAME = ${TABLE_NAME}"

L1_FLOW_ID=l1_flow_${OWNER}.${TABLE_NAME}_${INGESTION_ID}
L1_JOB_ID=l1_job_${OWNER}.${TABLE_NAME}

fn_log_info  "L1_FLOW_ID : ${L1_FLOW_ID}"
fn_log_info  "L1_JOB_ID : ${L1_JOB_ID}"

# RUN BATCH_ID_GENERATOR
bash ${COMMON_MODULE_HOME}/bin/batch_id_generator.sh "${L1_FLOW_ID}"

# RUN JOB INSTANCE
fn_log_info "-------------- RUNNING JOB INSTANCE --------------"
RUNNING_JOB_INSTANCE_ID=$(fn_run_job ${L1_FLOW_ID} ${L1_JOB_ID})
fn_check_and_exit_error_response "${RUNNING_JOB_INSTANCE_ID}" "COORDINATOR COMMAND SUCCESSFUL" "COORDINATOR COMMAND FAILED"
fn_log_info "RUNNING_JOB_INSTANCE_ID : $RUNNING_JOB_INSTANCE_ID"

# GET CURRENT RUNNING BATCH
fn_log_info "-------------- GETTING CURRENT RUNNING BATCH --------------"
RUNNING_BATCH_ID_L1=$(fn_get_running_batch_id "${L1_FLOW_ID}")
fn_check_batch_id ${RUNNING_BATCH_ID_L1}

# CALL SQOOP
cols=${TABLE_NAME}_cols
rowid=${TABLE_NAME}_rowid

table_name_cols=${!cols}
table_name_row_id=${!rowid}

fn_log_info "table_name_cols = ${table_name_cols}"
fn_log_info "table_name_rowid = ${table_name_row_id}"

echo "sqoop import -Dmapred.job.queue.name=${JOB_QUEUE_NAME}  --connect '${CONNECTION_STRING}'  --username ${SOURCE_USER_NAME} --password ${SOURCE_PASSWORD}  --fetch-size 5000 --table ${OWNER}.${TABLE_NAME} --columns "\"${table_name_row_id}\""   --as-textfile  --target-dir /raw/${OWNER}/db_work/version=0/${OWNER}_${TABLE_NAME}/batch_id=${RUNNING_BATCH_ID_L1}   -m 1   --fields-terminated-by '${FIELD_TERMINATOR}' --null-string '\\N' --null-non-string '\\N' --hive-delims-replacement ' ' --map-column-java ${table_name_row_id}=String"

sqoop import -Dmapred.job.queue.name=${JOB_QUEUE_NAME}  --connect ${CONNECTION_STRING}  --username ${SOURCE_USER_NAME} --password ${SOURCE_PASSWORD}  --fetch-size 5000 --table ${OWNER}.${TABLE_NAME} --columns "\"${table_name_row_id}\""   --as-textfile  --target-dir /raw/${OWNER}/db_work/version=0/${OWNER}_${TABLE_NAME}/batch_id=${RUNNING_BATCH_ID_L1}   -m 1   --fields-terminated-by "${FIELD_TERMINATOR}" --null-string '\\N' --null-non-string '\\N' --hive-delims-replacement ' ' --map-column-java ${table_name_row_id}=String

if [ $? -ne 0 ];
then
    # FAIL JOB INSTANCE
    fn_log_info "-------------- MARKING THE JOBINSTANCE FAILED --------------"
    response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 100
else
    fn_log_info "SUCCESS"
fi

# MARK THE JOBINSTANCE SUCCESSFUL
fn_log_info "-------------- MARKING THE JOBINSTANCE SUCCESSFUL --------------"
response=$(fn_successful_running_job_instance "${L1_JOB_ID}")
fn_check_and_exit_error_response "${response}" "The job instance is made successful" "Unable to make the job instance successful"

# RUN END_JOB
bash ${COMMON_MODULE_HOME}/bin/end_job.sh "${L1_FLOW_ID}"