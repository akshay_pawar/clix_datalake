import openpyxl as op
import csv
import pandas as pd
import os
import time
import datetime
import subprocess
from decimal import Decimal
import sys

from openpyxl import load_workbook




def run_command(args_list):
        # import subprocess
        print('Running system command: {0}'.format(' '.join(args_list)))
        proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        s_output, s_err = proc.communicate()
        s_return =  proc.returncode
        return s_return, s_output, s_err

curr_batch=sys.argv[1]
partition = '/raw/INDUS/db_work/version=0/INDUS_SME_EE/batch_id='+curr_batch

create_dir = run_command(['mkdir','/datalake/SME_EE_bkp/'+curr_batch])
create_tmp_dir = run_command(['mkdir','/tmp/SME_EE_csvs/'+curr_batch])



fileList=[]
for root, dirs, files in os.walk("/datalake/SME_EE"):  
    for filename in files:
        fileList.append(filename)
        


def execute(file_name):
    wb=load_workbook('/datalake/SME_EE/'+file_name,data_only=True) 
    path = '/datalake/SME_EE/'+file_name
    print(wb.sheetnames)
    df_pune=pd.DataFrame()
    df_chandigarh=pd.DataFrame()
    df_delhi=pd.DataFrame()
    df_netambit=pd.DataFrame()
    df_jaipur=pd.DataFrame()
    df_bengaluru=pd.DataFrame()
    df_hyderabad=pd.DataFrame()
    df_mumbai=pd.DataFrame()
    df_chennai=pd.DataFrame()
    
    
    for i in wb.sheetnames:
        if (i=="Pune"):
            sh=wb['Pune']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Pune.csv', 'a', newline="") as f:
               c=csv.writer(f)
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
        elif (i=="Chandigarh"):
            sh=wb['Chandigarh']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Chandigarh.csv', 'a', newline="") as f:
               c=csv.writer(f)
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
               f.close()    
        elif (i=="Delhi"):
            sh=wb['Delhi']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Delhi.csv', 'a', newline="") as f:
               c=csv.writer(f)
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
               f.close()
        elif (i=="Netambit"):
            sh=wb['Netambit']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Netambit.csv', 'a', newline="") as f:
               c=csv.writer(f)
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
               f.close()
        elif (i=="Jaipur"):
            sh=wb['Jaipur']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Jaipur.csv', 'a', newline="") as f:
               c=csv.writer(f)
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
               f.close()
        elif (i=="Bangalore"):
            sh=wb['Bangalore']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Bangalore.csv', 'a', newline="") as f:
               c=csv.writer(f)
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
               f.close()
        elif (i=="Hyderabad"):
            sh=wb['Hyderabad']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Hyderabad.csv', 'a', newline="") as f:
               c=csv.writer(f)
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
               f.close()
        elif (i=="Mumbai"):
            sh=wb['Mumbai']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Mumbai.csv', 'a', newline="") as f:
               c=csv.writer(f)
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
               f.close()
        elif (i=="Chennai"):
            sh=wb['Chennai']
            total_cols=sh.max_column
            total_rows=sh.max_row
            with open('/tmp/SME_EE_csvs/'+curr_batch+'/Chennai.csv', 'a', newline="") as f:
               c=csv.writer(f) 
               for i in range(1,total_rows+1):
                 fileList=[]
                 for j in range(1,total_cols+1):
                   cell_obj = sh.cell(row=i,column=j)
                   fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
                 c.writerow(fileList)
               f.close()
 

for name in fileList:
    if name.endswith(".xlsx"):     
        print(name)
        execute(name)
    else:
        print(str(name)+" - This is not xlsx format")

out = run_command(['hadoop', 'fs', '-mkdir', partition])
#copy_to_datalake = run_command(['cp','/tmp/SME_EE_csvs/'+curr_batch+'/*.csv','/datalake/SME_EE_bkp/'+curr_batch])
#copy = run_command(['hadoop','fs','-copyFromLocal','/tmp/SME_EE_csvs/'+curr_batch+'/*',partition]) 
#move_excel = run_command(['mv','/datalake/SME_EE/*.xlsx','/datalake/SME_EE_bkp'])
