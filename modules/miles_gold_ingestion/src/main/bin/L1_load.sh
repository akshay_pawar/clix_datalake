#!/bin/bash
###############################################################################
#                               General Details                               #
###############################################################################
#                                                                             #
# Name         :                                                              #
#                                                                             #
# Description  : This script ingests data from ATG table and puts on L1 layer #
#                                                                             #
# Author       : <AKSHAY PAWAR>                                               #
#                                                                             #
# Create Date  :                                                              #
#                                                                             #
###############################################################################
#                          Script Environment Setup                           #
###############################################################################
kinit -Vkt /home/dm_infra/dm_infra.keytab dm_infra@CLIXCDH.COM
SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# EXTRACT MODULE HOME FROM SCRIPT PATH
MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"
fn_log_info "MODULE_HOME = $MODULE_HOME"

###############################################################################
#                           Import Dependencies                               #
###############################################################################

. ${MODULE_HOME}/../common/bin/import-dependecies.sh
. ${MODULE_HOME}/etc/tables.properties
. ${MODULE_HOME}/etc/module.env.properties

###############################################################################
#                               Module Start                                  #
###############################################################################

# READING PARAMETERS FROM AZKABAN
fn_read_command_line_parameters "$@"

fn_log_info "OWNER : ${OWNER}"
fn_log_info "TABLE_NAME = ${TABLE_NAME}"

table_name_cols=`cat ${MODULE_HOME}/etc/tables.properties | grep ${TABLE_NAME}_cols | cut -d'=' -f2`
fn_log_info "table_name_cols : ${table_name_cols}"

L1_FLOW_ID=l1_flow_${OWNER}.${TABLE_NAME}_${INGESTION_ID}
L1_JOB_ID=l1_job_${OWNER}.${TABLE_NAME}
END_JOB_KEY=${OWNER}_${TABLE_NAME}_KEY
fn_log_info "L1_FLOW_ID : ${L1_FLOW_ID}"
fn_log_info "L1_JOB_ID : ${L1_JOB_ID}"

bash ${COMMON_MODULE_HOME}/bin/batch_id_generator.sh ${L1_FLOW_ID}

# RUN JOB INSTANCE
fn_log_info "-------------- RUNNING JOB INSTANCE --------------"
RUNNING_JOB_INSTANCE_ID=$(fn_run_job ${L1_FLOW_ID} ${L1_JOB_ID})
fn_check_and_exit_error_response "${RUNNING_JOB_INSTANCE_ID}" "COORDINATOR COMMAND SUCCESSFUL" "COORDINATOR COMMAND FAILED"
fn_log_info "RUNNING_JOB_INSTANCE_ID : $RUNNING_JOB_INSTANCE_ID"

# GET CURRENT RUNNING BATCH
fn_log_info "-------------- GETTING CURRENT RUNNING BATCH --------------"
RUNNING_BATCH_ID_L1=$(fn_get_running_batch_id "${L1_FLOW_ID}")
fn_check_batch_id ${RUNNING_BATCH_ID_L1}

#SQOOP_HDFS_LOCATION=/raw/MILES/db_work/version=0/${OWNER}_${TABLE_NAME}/batch_id=${RUNNING_BATCH_ID_L1}
SQOOP_HDFS_LOCATION=/raw/MILES/db_work/version=0/${OWNER}_${TABLE_NAME}/batch_id=${RUNNING_BATCH_ID_L1}
if [[ ${LOAD_TYPE} == "FULL" ]];
then
# CALL SQOOP
fn_log_info "sqoop import -Dmapred.job.queue.name=${JOB_QUEUE_NAME}  --connect ${CONNECTION_STRING} --username ${SOURCE_USER_NAME} --password ${SOURCE_PASSWORD}  --fetch-size 5000 --query 'select ${table_name_cols} from ${TABLE_NAME} where \$CONDITIONS'  --as-textfile  --target-dir /raw/MILES/db_work/version=0/${OWNER}_${TABLE_NAME}/batch_id=${RUNNING_BATCH_ID_L1}   -m 1   --fields-terminated-by ${FIELD_TERMINATOR} --null-string '\\N' --null-non-string '\\N' --hive-delims-replacement ' '"
export HADOOP_CLASSPATH=${MSSQL_JDBC_LIBJAR}
sqoop import -Dmapred.job.queue.name=${JOB_QUEUE_NAME}  --connect ${CONNECTION_STRING} --username ${SOURCE_USER_NAME} --password ${SOURCE_PASSWORD}  --fetch-size 5000 --query "select ${table_name_cols} from ${TABLE_NAME} where \$CONDITIONS"  --as-textfile  --target-dir ${SQOOP_HDFS_LOCATION}   -m 1   --fields-terminated-by ${FIELD_TERMINATOR} --null-string '\\N' --null-non-string '\\N' --hive-delims-replacement ' '

if [ $? -ne 0 ];
then
    # FAIL JOB INSTANCE
    fn_log_info "-------------- MARKING THE JOBINSTANCE FAILED --------------"
    response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 100
else
    fn_log_info "SUCCESS"
    fn_log_info "MSCK REPAIR ON WORK TABLE"
#hive -e "use ${HIVE_DATABASE_NAME_STAGE}; MSCK REPAIR TABLE ${TABLE_NAME};"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -n dm_infra -e "use ${HIVE_DATABASE_NAME_STAGE}; MSCK REPAIR TABLE ${OWNER}_${TABLE_NAME};"
if [ $? -eq 0 ];
then
    info "MSCK REPAIR successful."
else
    error "MSCK REPAIR command failed to execute"
    if [ ! -z $SQOOP_HDFS_LOCATION ];
    then
        hadoop fs -rm -r ${SQOOP_HDFS_LOCATION}
    fi
    response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 1
fi

fi


elif [[ ${LOAD_TYPE} == "DELTA" ]]
then
table_name_delta=`cat ${MODULE_HOME}/etc/tables.properties | grep ${TABLE_NAME}_delta | cut -d'=' -f2`
LAST_SUCCESSFUL_BATCH_ID=$(fn_get_last_successful_batch_id "${L1_FLOW_ID}" "${L1_JOB_ID}")
fn_log_info "LAST_SUCCESSFUL_BATCH_ID : ${LAST_SUCCESSFUL_BATCH_ID}"
if [ -z "${LAST_SUCCESSFUL_BATCH_ID}" ]
then
fn_log_info "No Last successful batch id as this is the 1st batch for l1."
LAST_SUCCESSFUL_BATCH_ID="0000000000000"
MAX_MODIFIED_DATE='1950-12-31'
sqoop import -Dmapred.job.queue.name=${JOB_QUEUE_NAME}  --connect ${CONNECTION_STRING} --username ${SOURCE_USER_NAME} --password ${SOURCE_PASSWORD}  --fetch-size 5000 --query "select ${table_name_cols} from ${TABLE_NAME} where ${table_name_delta}>${MAX_MODIFIED_DATE} and \$CONDITIONS" --as-textfile  --target-dir $SQOOP_HDFS_LOCATION -m 1   --fields-terminated-by ${FIELD_TERMINATOR} --null-string '\\N' --null-non-string '\\N' --hive-delims-replacement ' '


if [ $? -ne 0 ];
then
    # FAIL JOB INSTANCE
    fn_log_info "-------------- MARKING THE JOBINSTANCE FAILED --------------"
    response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 100
else
    fn_log_info "SUCCESS"
fi


fn_log_info "MSCK REPAIR ON WORK TABLE"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -n dm_infra -e "use ${HIVE_DATABASE_NAME_STAGE}; MSCK REPAIR TABLE MILES_${TABLE_NAME};"
if [ $? -eq 0 ];
then
    info "MSCK REPAIR successful."
else
    error "MSCK REPAIR command failed to execute"
    if [ ! -z $SQOOP_HDFS_LOCATION ];
    then
        hadoop fs -rm -r ${SQOOP_HDFS_LOCATION}
    fi
    response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 1
fi


else
MAX_MODIFIED_DATE=$(fn_get_job_instance_event_value ${LAST_SUCCESSFUL_BATCH_ID} ${L1_FLOW_ID} ${L1_JOB_ID} "${END_JOB_KEY}")
fn_log_info "${MAX_MODIFIED_DATE}"
sqoop import -Dmapred.job.queue.name=${JOB_QUEUE_NAME}  --connect ${CONNECTION_STRING} --username ${SOURCE_USER_NAME} --password ${SOURCE_PASSWORD}  --fetch-size 5000 --query "select ${table_name_cols} from ${TABLE_NAME} where ${table_name_delta}>'${MAX_MODIFIED_DATE}' and \$CONDITIONS" --as-textfile  --target-dir $SQOOP_HDFS_LOCATION   -m 1   --fields-terminated-by ${FIELD_TERMINATOR} --null-string '\\N' --null-non-string '\\N' --hive-delims-replacement ' '

if [ $? -ne 0 ];
then
    # FAIL JOB INSTANCE
    fn_log_info "-------------- MARKING THE JOBINSTANCE FAILED --------------"
    response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 100
else
    fn_log_info "SUCCESS"
fi



fn_log_info "MSCK REPAIR ON WORK TABLE"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -n dm_infra -e "use ${HIVE_DATABASE_NAME_STAGE}; MSCK REPAIR TABLE ${OWNER}_${TABLE_NAME};"
if [ $? -eq 0 ];
then
    info "MSCK REPAIR successful."
else
    error "MSCK REPAIR command failed to execute"
    if [ ! -z $SQOOP_HDFS_LOCATION ];
    then
        hadoop fs -rm -r ${SQOOP_HDFS_LOCATION}
    fi
    response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 1
fi



fi

else
fn_log_info "LOAD_TYPE NOT MENTIONED"
exit 100


fi

echo "BEFORE COORDINATOR ###########################################################################"

if [[ ${LOAD_TYPE} == "DELTA" ]];
    then
        MAX_MODIFIED_DATE=`beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -n dm_infra -e "select max(${table_name_delta}) from ${HIVE_DATABASE_NAME_STAGE}.${OWNER}_${TABLE_NAME}"`
        MAX_MODIFIED_DATE=`echo $MAX_MODIFIED_DATE |cut -d'|' -f4`
        if [ $? -ne 0 ]
        then
            error "FAILED TO GET CURRENT MAX_MODIFIED_DATE"
            info "DELETING DATA FOR CURRENT BATCH"
            hadoop fs -rm -r $SQOOP_HDFS_LOCATION
            response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
            fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
            exit 1
        fi

        info "CURRENT MAX_MODIFIED_DATE : $MAX_MODIFIED_DATE"
        if [ -z "${MAX_MODIFIED_DATE}" ];
        then
		fn_log_info "CURRENT_MAX_MODIFIED_DATE IS NULL . Hence restoring the earlier max values only"
	else
	    fn_add_job_instance_event "${END_JOB_KEY}" "${MAX_MODIFIED_DATE}" ${RUNNING_JOB_INSTANCE_ID}
        fi

fi

# MARK THE JOBINSTANCE SUCCESSFUL
fn_log_info "-------------- MARKING THE JOBINSTANCE SUCCESSFUL --------------"
response=$(fn_successful_running_job_instance "${L1_JOB_ID}")
fn_check_and_exit_error_response "${response}" "The job instance is made successful" "Unable to make the job instance successful"

# RUN END_JOB
bash ${COMMON_MODULE_HOME}/bin/end_job.sh "${L1_FLOW_ID}"
