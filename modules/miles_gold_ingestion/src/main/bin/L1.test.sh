#!/bin/bash
###############################################################################
#                               General Details                               #
###############################################################################
#                                                                             #
# Name         : Generic Table Ingestion                                      #
#                                                                             #
# Description  : Table Ingestion                                              #
#                                                                             #
# Author       : Manish Bajpai                                                #
#                                                                             #
###############################################################################
#                          Script Environment Setup                           #
###############################################################################

# Extract SCRIPT_HOME
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname ${SCRIPT_PATH}`
MODULE_HOME=`dirname ${SCRIPT_HOME}`
PROJECT_HOME=`dirname ${MODULE_HOME}`
COMMON_HOME=${PROJECT_HOME}/common
. ${MODULE_HOME}/bin/build_sqoop_command.sh
. ${COMMON_HOME}/bin/import_dependencies.sh ${SCRIPT_HOME}

usr=`whoami`
kinit -Vkt /home/dm_infra/dm_infra.keytab dm_infra@CLIXCDH.COM

SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"
EPOCH="`date +"%Y%m%d%H%M%S"`"

###############################################################################
#                               Module Start                                  #
###############################################################################

#####
# READING PARAMETERS FROM COMMAND LINE AND PROPERTY FILE
#####
export $1
. ${TABLE_PROP_PATH}
readParameters "$@"

TABLE=${TABLE_NAME}
SRC_SCHEMA_NAME=${SRC_SCHEMA_NAME}
UPPER_SRC_SCHEMA_NAME=`convertToUpperCase ${SRC_SCHEMA_NAME}`
USER_NAME=${USER_NAME}
UPPER_USER_NAME=`convertToUpperCase ${USER_NAME}`

UPPERCASE_TABLE_NAME="`convertToUpperCase ${TABLE}`"
UPPERCASE_SCHEMA_NAME="`convertToUpperCase ${SRC_SCHEMA_NAME}`"
#FLOW_ID="l1_flow_${UPPERCASE_SCHEMA_NAME}.${UPPERCASE_TABLE_NAME}_id${DATA_SOURCE_ID}"
FLOW_ID="l1_flow_${SUBJ_AREA_NAME}.${UPPERCASE_TABLE_NAME}_id${DATA_SOURCE_ID}"
#JOB_ID="l1_job_${UPPERCASE_SCHEMA_NAME}.${UPPERCASE_TABLE_NAME}"
JOB_ID="l1_job_${SUBJ_AREA_NAME}.${UPPERCASE_TABLE_NAME}"
END_JOB_KEY="${SUBJ_AREA_NAME}_${UPPERCASE_TABLE_NAME}_KEY"
LOWERCASE_TABLE_NAME="`convertToLowerCase ${SRC_SCHEMA_NAME}_${TABLE}`"
LOWERCASE_SRC_NAME=`convertToLowerCase ${SUBJ_AREA_NAME}`
#HIVE_TABLE_NAME="stg_${LOWERCASE_SRC_NAME}_${LOWERCASE_TABLE_NAME}"
HIVE_TABLE_NAME="${SUBJ_AREA_NAME}_${TABLE_NAME}"
UPPER_HIVE_TAB_NAME=`convertToUpperCase ${HIVE_TABLE_NAME}`
UPPER_SUBJ_AREA_NAME=`convertToUpperCase ${SUBJ_AREA_NAME}`
UPPER_HIVE_TAB_NAME_UNDERSCORE=`echo ${UPPER_HIVE_TAB_NAME} | sed 's/\./_/g'`
#MOD_JOB_ID=${UPPER_HIVE_TAB_NAME_UNDERSCORE}_JOB
#MOD_JOB_ID=${HIVE_TABLE_NAME}_JOB
#START_JOB_ID=${UPPER_SUBJ_AREA_NAME}_CYCLE_START_JOB
#START_DATE_KEY=${UPPER_SUBJ_AREA_NAME}_CYCLE_START_DATE_KEY
UPPER_TABLE=`convertToUpperCase ${TABLE_NAME}`
UPPER_TABLE=`convertToUpperCase ${TABLE_NAME}`
MODULE_DATE_KEY=${UPPER_SUBJ_AREA_NAME}_${UPPER_TABLE}_DATE_KEY
#CYCLE_START_DATE=$(fn_get_job_event_value ${START_JOB_ID} ${START_DATE_KEY})
#echo "CYCLE_START_DATE:${CYCLE_START_DATE}"
#AZKABAN_PROJECT=${LOWERCASE_SRC_NAME}
AZKABAN_PROJECT=${SUBJ_AREA_NAME}
AZKABAN_USER=${AZ_USER}
AZKABAN_PASSWORD=${AZ_PASSWORD}
AZKABAN_URL=${AZ_URL}
AZKABAN_FLOW_NAME=${FLOW_ID}
AZKABAN_JOB_NAME=${JOB_ID}

info "HIVE_TABLE_NAME=$HIVE_TABLE_NAME"
info "FLOW_ID=${FLOW_ID}"
info "JOB_ID=${JOB_ID}"

# RUN BATCH ID GENERATOR
sh ${COORDINATOR_HOME}/bin/batch_id_generator.sh "${FLOW_ID}"

# INITIATE THE JOB INSTANCE
info "Check and fail if any job instance for ${JOB_ID} is running"
response=$(fn_fail_running_job_instance "${JOB_ID}")
RUNNING_JOB_INSTANCE_ID=$(fn_run_job ${FLOW_ID} ${JOB_ID});
checkVariableIfSet "RUNNING_JOB_INSTANCE_ID" "${RUNNING_JOB_INSTANCE_ID}"
fn_check_numeric "${RUNNING_JOB_INSTANCE_ID}"
info "Running job instance id of ${JOB_ID}  : ${RUNNING_JOB_INSTANCE_ID}"

RUNNING_BATCH_ID=`fn_get_running_batch_id "${FLOW_ID}"`
checkVariableIfSet "RUNNING_BATCH_ID" "${RUNNING_BATCH_ID}"
fn_check_numeric "${RUNNING_BATCH_ID}"
info "Running batch_id for flow ${FLOW_ID} = ${RUNNING_BATCH_ID}"

# CREATE SELECT QUERY AS PER LOAD TYPE

    if [[ ${LOAD_TYPE} == "FULL" ]];
    then
        if [ -z $WHERE_CLAUSE ];
        then
            SELECT_QUERY="${SELECT_QUERY} WHERE \\\$CONDITIONS"
        else
            SELECT_QUERY="${SELECT_QUERY} WHERE $WHERE_CLAUSE and \\\$CONDITIONS"
        fi
    else
               LAST_SUCCESSFUL_BATCH_ID=$(fn_get_last_successful_batch_id "${FLOW_ID}" "${JOB_ID}")
                info "LAST_SUCCESSFUL_BATCH_ID : ${LAST_SUCCESSFUL_BATCH_ID}"

                if fn_check_number ${LAST_SUCCESSFUL_BATCH_ID};
                then
                    info "No Last successful batch id as this is the 1st batch for l1."
                    LAST_SUCCESSFUL_BATCH_ID="0000000000000"
                    MAX_MODIFIED_DATE='1950-12-31'
                else
                    MAX_MODIFIED_DATE=$(fn_get_job_instance_event_value ${LAST_SUCCESSFUL_BATCH_ID} ${FLOW_ID} ${JOB_ID} "${END_JOB_KEY}")
                    fn_check_format_and_valid_date "${MAX_MODIFIED_DATE}"
                    if [ $? -ne 0 ];
                    then
                        info "INVALID DATE(${MAX_MODIFIED_DATE}) GETTING FROM COORDINATOR"
                        info "FETCHING MAX_MODIFIED_DATE FROM TABLE ${HIVE_DATABASE_NAME_STAGE}.${HIVE_TABLE_NAME}"
                        MAX_MODIFIED_DATE=`hive -e "select max(${DELTA_COLUMN}) from ${HIVE_DATABASE_NAME_STAGE}.${HIVE_TABLE_NAME}"`
                        if [ $? -ne 0 ]
                        then
                            error "FAILED TO GET MAX_MODIFIED_DATE FROM TABLE ${HIVE_DATABASE_NAME_STAGE}.${HIVE_TABLE_NAME}"
                            response=$(fn_fail_running_job_instance "${JOB_ID}")
                            fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
                            exit 1
                        fi
                        fn_check_format_and_valid_date "${MAX_MODIFIED_DATE}"
                        if [ $? -ne 0 ];
                        then
                            info "FETCHED INVALID DATE FORMAT FROM TABLE ${HIVE_DATABASE_NAME_STAGE}.${HIVE_TABLE_NAME}. PLEASE PASS MAX_MODIFIED_DATE FROM AZKABAN PARAMETERS AND RERUN THE FLOW"
                            response=$(fn_fail_running_job_instance "${JOB_ID}")
                            fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
                            exit 1
                        fi
                    fi
                fi
            
        fi

        MAX_MODIFIED_DATE=$(date -d "${MAX_MODIFIED_DATE}" +'%Y-%m-%d#%H:%M:%S:%9N')
        info "MAX_MODIFIED_DATE: ${MAX_MODIFIED_DATE}"

        if [ -z $WHERE_CLAUSE ];
        then
            SELECT_QUERY="${SELECT_QUERY} WHERE ${DELTA_COLUMN}>to_timestamp('${MAX_MODIFIED_DATE}', 'yyyy-mm-dd#hh24:mi:ss:ff9') and \\\$CONDITIONS"
        else
            SELECT_QUERY="${SELECT_QUERY} WHERE $WHERE_CLAUSE and ${DELTA_COLUMN}>to_timestamp('${MAX_MODIFIED_DATE}', 'yyyy-mm-dd#hh24:mi:ss:ff9') and \\\${CONDITIONS}"
        fi
    

    if [ -z $WHERE_CLAUSE ];
    then
        SELECT_QUERY="${SELECT_QUERY} WHERE ${DELTA_COLUMN} >= to_timestamp('${START_DT}', 'yyyy-mm-dd#hh24:mi:ss:ff9') AND ${DELTA_COLUMN} <= to_timestamp('${END_DT}', 'yyyy-mm-dd#hh24:mi:ss:ff9') AND \\\$CONDITIONS"
    else
        SELECT_QUERY="${SELECT_QUERY} WHERE $WHERE_CLAUSE AND ${DELTA_COLUMN} >= to_timestamp('${START_DT}', 'yyyy-mm-dd#hh24:mi:ss:ff9') AND ${DELTA_COLUMN} <= to_timestamp('${END_DT}', 'yyyy-mm-dd#hh24:mi:ss:ff9') AND \\\$CONDITIONS"
    fi

info "SELECT_QUERY CREATED: ${SELECT_QUERY}"



# CREATE SQOOP COMMAND
fn_build_sqoop_command "${SQOOP_HDFS_LOCATION}/batch_id=${RUNNING_BATCH_ID}" "${UPPER_SUBJ_AREA_NAME}" "${UPPER_SRC_SCHEMA_NAME}" "${UPPER_USER_NAME}"
scoop_info_command=$(echo $sqoop_command|sed "s/--password '${PASSWORD}'/--password xxxxxxxx/g")
info "$scoop_info_command"

# RUN SQOOP COMMAND
export HADOOP_CLASSPATH=${MSSQL_JDBC_LIBJAR}
eval $sqoop_command
if [ $? -eq 0 ];
then
    info "Sqoop command successful."
else
    error "Sqoop command failed to execute"
    if [ ! -z $SQOOP_HDFS_LOCATION ];
    then
        hadoop fs -rm -r ${SQOOP_HDFS_LOCATION}
    fi
    response=$(fn_fail_running_job_instance "${JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 1
fi

# MSCK REPAIR TABLE
info "MSCK REPAIR ON STAGE TABLE"
hive -e "use ${HIVE_DATABASE_NAME_STAGE}; MSCK REPAIR TABLE ${HIVE_TABLE_NAME};"
if [ $? -eq 0 ];
then
    info "MSCK REPAIR successful."
else
    error "MSCK REPAIR command failed to execute"
    if [ ! -z $SQOOP_HDFS_LOCATION ];
    then
        hadoop fs -rm -r ${SQOOP_HDFS_LOCATION}
    fi
    response=$(fn_fail_running_job_instance "${JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 1
fi


# AUDIT ENTRIES IN COORDINATOR AS PER LOAD TYPE
    if [[ ${LOAD_TYPE} == "DELTA" ]];
    then
        MAX_MODIFIED_DATE=`hive -e "select max(${DELTA_COLUMN}) from ${HIVE_DATABASE_NAME_STAGE}.${HIVE_TABLE_NAME} where batch_id=${RUNNING_BATCH_ID}"`
        if [ $? -ne 0 ]
        then
            error "FAILED TO GET MAX_MODIFIED_DATE"
            info "DELETING DATA FOR CURRENT BATCH"
            hadoop fs -rm -r ${SQOOP_HDFS_LOCATION}
            response=$(fn_fail_running_job_instance "${JOB_ID}")
            fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
            exit 1
        fi

        info "MAX_MODIFIED_DATE : $MAX_MODIFIED_DATE"
        if ! [[ ${MAX_MODIFIED_DATE} == "NULL" ]];
        then
            fn_add_job_instance_event "${END_JOB_KEY}" "${MAX_MODIFIED_DATE}" ${RUNNING_JOB_INSTANCE_ID}
        fi
        TABLE_DATE_KEY="${UPPER_HIVE_TAB_NAME}_${CYCLE_START_DATE}"
        fn_add_job_event "${MODULE_DATE_KEY}" "${CYCLE_START_DATE}" "${MOD_JOB_ID}"
        fn_add_job_event "$TABLE_DATE_KEY" "${RUNNING_BATCH_ID}" "${MOD_JOB_ID}"
    else
        TABLE_DATE_KEY="${UPPER_HIVE_TAB_NAME}_${CYCLE_START_DATE}"
        fn_add_job_event "${MODULE_DATE_KEY}" "${CYCLE_START_DATE}" "${MOD_JOB_ID}"
        fn_add_job_event "$TABLE_DATE_KEY" "${RUNNING_BATCH_ID}" "${MOD_JOB_ID}"
    fi


# MARK JOB SUCCESSFUL
response=$(fn_successful_running_job_instance "${JOB_ID}")
info "Marking flow(FLOW_ID) and batch(${RUNNING_BATCH_ID}) successful"
fn_check_and_exit_error_response "${response}" "The job instance is made Successfull" "Unable to make the job instance Successfull"

# END BATCH
sh ${COORDINATOR_HOME}/bin/end_job.sh ${FLOW_ID}
###############################################################################
#                               Module End                                  #
###############################################################################
