function fn_build_sqoop_command() {

HDFS_DIR=$1
SPLIT_BY_COLUMN=$7
if [ -z "${6}"  ] 
 then
  echo "No argument supplied"
  NUM_MAPPERS="1"
  NUM_MAP_SPLIT_ARG="-m $NUM_MAPPERS"

else
 NUM_MAPPERS=$6
  NUM_MAP_SPLIT_ARG="-m $NUM_MAPPERS --split-by $SPLIT_BY_COLUMN"
fi
PASSWORD=${5}
sqoop_command="sqoop import -D mapreduce.job.queuename=default --connect $CONNECTION_URL --username $USER_NAME --password $PASSWORD --fetch-size 5000 --query \"$SELECT_QUERY\" --null-string '\\\N' --null-non-string '\\\N' $NUM_MAP_SPLIT_ARG  --hive-delims-replacement ' ' --target-dir $HDFS_DIR --fields-terminated-by \"\001\""
}
