CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.MILES_dmSegment_history (
SegmentAlternateKey bigint , SegmentName string , ExchangeCode double , ExchangeName string , Type string , create_timestamp timestamp , update_timestamp timestamp ,file_load_timestamp string ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/MILES/hive/db_gold/MILES_dmSegment_history'
tblproperties ("parquet.compression"="SNAPPY")
