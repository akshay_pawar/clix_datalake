CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.MILES_dmDocumentMaster (
DoucmentAlternateKey bigint , DocumentDescription string , DocumentFor string , DocumentForCode string , Product string , WefDate timestamp , Compulsory string , Validity string , DocumentType string , create_timestamp timestamp , update_timestamp timestamp ,file_load_timestamp string ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/MILES/hive/db_gold/MILES_dmDocumentMaster'
tblproperties ("parquet.compression"="SNAPPY")
