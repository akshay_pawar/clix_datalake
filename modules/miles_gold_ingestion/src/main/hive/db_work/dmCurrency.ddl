CREATE EXTERNAL TABLE IF NOT EXISTS db_work.MILES_dmCurrency (
CurrencyAlternateKey bigint , CurrencyCode string , CurrencyName string , create_timestamp timestamp , update_timestamp timestamp , NoOfDecimal double )
PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS TEXTFILE LOCATION '/raw/MILES/db_work/version=0/MILES_dmCurrency';
