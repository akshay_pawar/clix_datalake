CREATE EXTERNAL TABLE IF NOT EXISTS db_work.MILES_dmSegment (
SegmentAlternateKey bigint , SegmentName string , ExchangeCode double , ExchangeName string , Type string , create_timestamp timestamp , update_timestamp timestamp )
PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS TEXTFILE LOCATION '/raw/MILES/db_work/version=0/MILES_dmSegment';
