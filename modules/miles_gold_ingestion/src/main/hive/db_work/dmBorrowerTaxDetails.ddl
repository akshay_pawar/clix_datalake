CREATE EXTERNAL TABLE IF NOT EXISTS db_work.MILES_dmBorrowerTaxDetails (
BorrowerTaxDetailsId bigint , BorrowerTaxDetailsAlternateKey bigint , BorrowerId bigint , StateCode string , State string , GSTIN string , DefaultLocation boolean , CreatedBy string , CreatedDate timestamp , UpdatedBy string , UpdatedDate timestamp )
PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS TEXTFILE LOCATION '/raw/MILES/db_work/version=0/MILES_dmBorrowerTaxDetails';
