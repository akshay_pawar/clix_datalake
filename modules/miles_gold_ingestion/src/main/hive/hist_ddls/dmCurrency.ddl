CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.MILES_dmCurrency_history (
CurrencyAlternateKey bigint , CurrencyCode string , CurrencyName string , create_timestamp timestamp , update_timestamp timestamp , NoOfDecimal double ,file_load_timestamp string ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/MILES/hive/db_gold/MILES_dmCurrency_history'
tblproperties ("parquet.compression"="SNAPPY")
