CREATE EXTERNAL TABLE IF NOT EXISTS db_work.MILES_factNBFCNetOwnFund (
NBFCNetOwnFundSid double , NBFCNetOwnFundkey double , CapitalMarketExposure double , ClientCap double , CompanyId string , FamilyCap double , NetOwnFund double , WEFDate timestamp , BorrowedFund double , Remark string , create_timestamp timestamp , update_timestamp timestamp )
PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS TEXTFILE LOCATION '/raw/MILES/db_work/version=0/MILES_factNBFCNetOwnFund';
