CREATE EXTERNAL TABLE IF NOT EXISTS db_work.MILES_dmGuarantorMapping (
GuarantorMappingAlternateKey bigint , DateId timestamp , BorrowerId bigint , LoanAccountId bigint , GuarantorAlternateKey bigint , BorrowerAlternateKey bigint , LoanAccountAlternateKey bigint , EntityType string , AccountStatus string , ClientGuarantor string , create_timestamp timestamp , update_timestamp timestamp )
PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS TEXTFILE LOCATION '/raw/MILES/db_work/version=0/MILES_dmGuarantorMapping';
