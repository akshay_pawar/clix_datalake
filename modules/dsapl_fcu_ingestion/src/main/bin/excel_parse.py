import openpyxl as op
import csv
import pandas as pd
import os
import time
import datetime
import subprocess
from decimal import Decimal
import sys

from openpyxl import load_workbook



def run_command(args_list):
        # import subprocess
        print('Running system command: {0}'.format(' '.join(args_list)))
        proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        s_output, s_err = proc.communicate()
        s_return =  proc.returncode
        return s_return, s_output, s_err

curr_batch=sys.argv[1]
raw_location=sys.argv[2]
table_name=sys.argv[3]
#partition = '/raw/INDUS/db_work/version=0/INDUS_HFS_EE/batch_id='+curr_batch
partition = raw_location

create_dir = run_command(['mkdir','/datalake/FCU_data_bkp/'+table_name+'/'+curr_batch])
create_tmp_dir = run_command(['mkdir','/tmp/FCU_csvs/'+table_name+'/'+curr_batch])



fileList=[]
for root, dirs, files in os.walk("/datalake/FCU_Data/"+table_name):
    for filename in files:
        fileList.append(filename)



def execute(file_name):
    wb=load_workbook('/datalake/FCU_Data/'+table_name+'/'+file_name,data_only=True)
    path = '/datalake/FCU_Data/'+table_name+'/'+file_name
    print(wb.sheetnames)
    sh=wb[file_name]   
    total_cols=sh.max_column
    total_rows=sh.max_row
    with open('/tmp/FCU_csvs/'+table_name+'/'+curr_batch+'/'+table_name+'.csv', 'w', newline="") as f:
      c=csv.writer(f)
      #fileList=[]
      for i in range(1,total_rows+1):
       fileList=[]
       for j in range(1,total_cols+1):
          cell_obj = sh.cell(row=i,column=j)
          fileList.append(str(str(cell_obj.value).strip()).replace(",","").replace("\n"," "))
      	  #c.writerow(str(cell_obj.value).replace(",","").replace("\n"," "))
       c.writerow(fileList)
      f.close()
#    for i in wb.sheetnames:
#        if (i=="E2E Tracker"):
#            sh=wb['E2E Tracker']
#            with open('/home/dm_infra/HFS_EE/CSVs/HFS_EE.csv', 'w', newline="") as f: 
#                c = csv.writer(f)
#                for r in sh.rows:
#                    c.writerow([cell.value for cell in r])
#            f.close()
    






for name in fileList:
    if name.endswith(".xlsx"):
        print(name)
        execute(name)
    else:
        print(str(name)+" - This is not xlsx format")


#out = run_command(['hadoop', 'fs', '-mkdir', partition])
