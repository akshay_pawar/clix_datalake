DROP TABLE IF EXISTS db_work.INDUS_LOS_DSAPL_FCU_SAMPLING;CREATE EXTERNAL TABLE IF NOT EXISTS db_work.INDUS_LOS_DSAPL_FCU_SAMPLING(Sr_No string,
Sample_Month_MM_YY string,
Location string,
Region string,
Product string,
Application_Reference_Number string,
Applicants_Name string,
Risk_Category string,
Employed_with string,
DSA_Name_ string,
FCU_Agency_Name string,
Loan_Amount_Applied_in_INR string,
Date_Of_Pickup_DD_MM_YY string,
Date_Of_Reporting_DD_MM_YY string,
TAT_Date_Pickup_to_Date_Reporting string,
Sample_Documents string,
Status string,
Negative_Case_Documents string,
Remarks string
) PARTITIONED BY (batch_id string) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' WITH SERDEPROPERTIES (   "separatorChar" = ",",   "quoteChar"     = "\"",   "escapeChar"    = "\\") STORED AS TEXTFILE LOCATION '/raw/INDUS/db_work/version=0/INDUS_LOS_DSAPL_FCU_SAMPLING' tblproperties ("skip.header.line.count"="1");msck repair table db_work.INDUS_LOS_DSAPL_FCU_SAMPLING;
