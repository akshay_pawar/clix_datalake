#!/bin/bash
################################################################################
#                               General Details                                #
################################################################################
#                                                                              #
# Name                                                                         #
#     : Add job script                                                         #
# File                                                                         #
#     : add_job.sh                                                             #
#                                                                              #
# Description                                                                  #
#     : this script is created to add jobs to flow                             #
#                                                                              #
# Author                                                                       #
#     : Pradeep Chandra                                                        #
#                                                                              #
################################################################################
#                           Script Environment Setup                           #
################################################################################

SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# Extract SCRIPT_HOME
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`

# Set module, project, subject area home paths.
MODULE_HOME=`dirname ${SCRIPT_HOME}`
PROJECT_HOME=`dirname ${MODULE_HOME}`
SUBJECT_AREA_HOME=`dirname ${PROJECT_HOME}`
HOME=`dirname ${SUBJECT_AREA_HOME}`
COMMON_HOME=${HOME}/Common/common

# Load namespace property file
. ${COMMON_HOME}/shell_functions/bin/namespace_functions.sh

# Load all environment properties files. Order Should not be changed.
user_namespace_param_file=$(getNamespaceFileForCurrentUser)
. ${HOME}/etc/${user_namespace_param_file}
. ${HOME}/etc/default.env.properties
. ${SUBJECT_AREA_HOME}/etc/subject-area.env.properties
. ${PROJECT_HOME}/etc/project.env.properties
. ${MODULE_HOME}/etc/module.env.properties

# Load all utility functions. Order should not be changed.
. ${COMMON_HOME}/shell_functions/bin/utility_functions.sh
. ${COMMON_HOME}/shell_functions/bin/environment_functions.sh
. ${COMMON_HOME}/shell_functions/bin/audit_functions.sh
. ${COMMON_HOME}/coordinator_functions/bin/functions.sh

###############################################################################
#                               Module Start                                  #
###############################################################################

USER_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $USER_PATH`

readParameters "$@"

info "JOB_ID=${JOB_ID}"
info "JOB_NAME=${JOB_NAME}"
info "JOB_DESC=${JOB_DESC}"
info "JOB_TYPE=${JOB_TYPE}"
info "FLOW_ID=${FLOW_ID}"

JOB_TYPE_ID=$(fn_get_job_type "${JOB_TYPE}")
if [ -z ${JOB_TYPE_ID} ] ; then
    error "JOB_TYPE $JOB_TYPE not found"
    exit 100
else
    fn_check_and_exit_error_response "${JOB_TYPE_ID}" "JOB_TYPE_ID fetched successfully." "Failed to Fetch JOB_TYPE_ID."
fi

#create flow
command_response=$(fn_add_job ${JOB_ID} "${JOB_NAME}" "${JOB_DESC}" ${JOB_TYPE_ID} ${FLOW_ID})
fn_check_and_exit_error_response "${command_response}" "${JOB_NAME} added successfully" "Failed to add job ${JOB_NAME}"

###############################################################################
#                               Module End                                    #
###############################################################################
