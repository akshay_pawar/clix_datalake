#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     : This script is used to trigger azkaban flow from ESP.                 #
#                                                                             #
#                                                                             #
###############################################################################
#                             Function Definitions                            #
###############################################################################

SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

#EXTRACT MODULE HOME FROM SCRIPT PATH
MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"

###############################################################################
#                           Import Dependencies                               #
###############################################################################

. ${MODULE_HOME}/bin/import-dependecies.sh

###############################################################################
#                               Module Start                                  #
###############################################################################
PROJECT=$1
FLOW_NAME=$2

bash ${AZKABAN_GENERIC_FLOW}/generic_trigger_job_flow.sh azkaban_host=${AZKABAN_HOST} project=${PROJECT} flow_name=${FLOW_NAME} username=${AZKABANUSERNAME} password=${AZKABANPASSWORD} wait_for_completion=true

###############################################################################
#                                     End                                     #
###############################################################################