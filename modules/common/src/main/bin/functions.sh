#!/bin/bash
################################################################################
#                               General Details                                #
################################################################################
#                                                                              #
# Name                                                                         #
#     : Coordinator                                                            #
# File                                                                         #
#     : functions.sh                                                           #
#                                                                              #
# Description                                                                  #
#     : This script contains all the functions that are required for           #
#        following functionalities                                             #
#                                                                              #
#        Set up steps to create blueprint of a Flow                            #
#                                                                              #
#        1. Create a Flow (which is a set of jobs which are dependent on each  #
#           other on logic basis)                                              #
#        2. Create Job Types                                                   #
#        3. Add multiple Job of a Job Type in a flow by providing their ids    #
#           and description                                                    #
#                                                                              #
#        Running a Batch                                                       #
#        4. Create Batch Id which will be used through out the current running #
#           instance of the flow                                               #
#        5. Create a Flow Instance                                             #
#                                                                              #
#        Marking                                                               #
#        6. Create Job Instances and set tables in them which will be used for #
#           marking the inputs.                                                #
#        7. Success or fail a job instance                                     #
#        8. Add Job Instance Events like COUNT and other events specific to    #
#           a job.                                                             #
#        9. Add job level events whose value will be updated if the same event #
#           is added again                                                     #
#                                                                              #
#        Success/Fail                                                          #
#        10. flag the Flow Instance failed or successful                       #                                               
#                                                                              #
#                                                                              #
#                                                                              #
# Author                                                                       #
#     : Sachin Gupta                                                           #
#                                                                              #
################################################################################
#                     Global Environment Base Properties                       #
################################################################################

###                                                                           
# string representation of hostname of the server where coordinator server is running
# @Type  :  String
# @Final :  true
COORDINATOR_SERVER_HOST=$COORDINATOR_SERVER_HOST
echo "COORDINATOR_SERVER_HOST: $COORDINATOR_SERVER_HOST"

###
# Integer representation of port on which coordinator server is listening
# @Type  :  Integer
# @Final :  true
COORDINATOR_SERVER_PORT=$COORDINATOR_SERVER_PORT
echo "COORDINATOR_SERVER_PORT: $COORDINATOR_SERVER_PORT"

###
# Integer representation of fail exit code
# @Type  :  Integer
# @Final :  true
EXIT_CODE_FAIL=1

###
# Base url to which all the rest calls will be made
# @Type  :  String
# @Final :  true
BASE_URL="http://$COORDINATOR_SERVER_HOST:$COORDINATOR_SERVER_PORT"


###
# Integer representation of exit code that will be returned when value for
# a required variable is not set
# @Type  :  Integer
# @Final :  true
EXIT_CODE_VARIABLE_NOT_SET=-10

###
# Integer representation of coordinator server status when it
# is not running
# @Type  :  Integer
# @Final :  true
SERVER_NOT_RUNNING=2

###
# String representation of POST request type
# @Type  :  String
# @Final :  true
REQUEST_TYPE_POST="POST"

###
# String representation of GET request type
# @Type  :  String
# @Final :  true
REQUEST_TYPE_GET="GET"

###
# String representation of PUT request type
# @Type  :  String
# @Final :  true
REQUEST_TYPE_PUT="PUT"

###
# String representation of DELETE request type
# @Type  :  String
# @Final :  true
REQUEST_TYPE_DELETE="DELETE"

###
# rest server url to check connection
# @Type  :  String
# @Final :  true
URL_CONSTANT_TO_CHECK_CONNECTION="/api/flows"

###
# rest server url to authenticate i.e to
# get the session id
# @Type  :  String
# @Final :  true
AUTHENTICATION_URL="/api/authentication"

###
# Content type for the authentication request
# @Type  :  String
# @Final :  true
AUTHENTICATION_CONTENT_TYPE="Content-Type: application/x-www-form-urlencoded"

###
# Data to be sent for the authentication request
# @Type  :  String
# @Final :  true
AUTHENTICATION_DATA="j_username=admin&j_password=admin&submit=Login&remember-me=true"

###
# Ineger representation for instance of a flow
# already running
# @Type  :  Integer
# @Final :  true
FLOW_INSTANCE_RUNNING=3

###
# Successful status
# @Type  :  String
# @Final :  true
STATUS_SUCCESSFUL="successful"

###
# Failed status
# @Type  :  String
# @Final :  true
STATUS_FAILED="failed"


################################################################################
#                             Function Definitions                             #
################################################################################

###
# Log any message
#
# Arguments:
#   message*
#     - string message
#
function fn_log(){

  message=$1

  echo ${message}

}


###
# Log info message
#
# Arguments:
#   message*
#     - string message
#
function fn_log_info(){

  message=$1

  fn_log "INFO ${message}"

}


###
# Log warning message
#
# Arguments:
#   message*
#     - string message
#
function fn_log_warn(){

  message=$1

  fn_log "WARN ${message}"

}


###
# Log error message
#
# Arguments:
#   message*
#     - string message
#
function fn_log_error(){

  message=$1

  fn_log "ERROR ${message}"

}


###
# Log given error message and exit the script with given exit code
#
# Arguments:
#   exit_code*
#     - exit code to be checked and exited with in case its not zero
#   failure_message*
#     - message to log if the exit code is non zero
#
function fn_exit_with_failure_message(){

  exit_code=$1

  failure_message=$2

  fn_log_error "${failure_message}"

  exit "${exit_code}"

}


###
# Verify if the variable is not-set/empty or not. In case it is non-set or empty,
# exit with failure message.
#
# Arguments:
#   variable_name*
#     - name of the variable to be check for being not-set/empty
#   variable_value*
#     - value of the variable
#
function assert_variable_is_set(){

  variable_name=$1

  variable_value=$2

  if [ "${variable_value}" == "" ]
  then

	exit_code=${EXIT_CODE_VARIABLE_NOT_SET}

    failure_messages="Value for ${variable_name} variable is not set"

    fn_exit_with_failure_message "${exit_code}" "${failure_messages}"

  fi

}


###
# Curl given URL and fetches the response given by server before
# doing curl there will always be a check just to make sure that
# the connection to the server is not lost
# if the connection is lost a constant SERVER_NOT_RUNNING will be
# returned with status message as "server is not running"
# Arguments:
#   request_type*
#     - type of request like GET,POST,DELETE,PUT
#   url*
#     - url to which the curl will be made
#   post_request_data*
#     - if the request type is POST then the data which needs
#       to be sent to server
#
function fn_curl(){

  fn_check_URL ${URL_CONSTANT_TO_CHECK_CONNECTION}

  if [ "$SERVER_NOT_RUNNING_VAR" == "$SERVER_NOT_RUNNING" ] && [ "$SESSION_ID" == "" ];
  then
    fn_exit_with_failure_message $SERVER_NOT_RUNNING "server is not running";
  else
    request_type=$1
    url=$2

    assert_variable_is_set "request_type" ${request_type}
    assert_variable_is_set "url" ${url}

    if [ "${request_type}" == "${REQUEST_TYPE_POST}" ];
    then
      post_request_data=$3

      assert_variable_is_set "post_request_data" ${post_request_data}

      curl_response=$(curl -s -X $request_type -H "Content-Type: application/json" --data "${post_request_data}" --cookie "JSESSIONID=$SESSION_ID" $url);

      echo $curl_response

    else
      curl_response=$(curl -s -X $request_type -H "Content-Type: application/json" --cookie "JSESSIONID=$SESSION_ID" $url);

      echo $curl_response
    fi
  fi
}


###
# To curl on AUTHENTICATION_URL to get
# session id which will be used through out the
# all the functions
#
function fn_authenticate(){

  authenticate=$(  \
    curl -o /dev/null  \
         --silent \
         --write-out  '%{http_code}\n'  \
         -X ${REQUEST_TYPE_POST}  \
         -H "${AUTHENTICATION_CONTENT_TYPE}"  \
         --data "${AUTHENTICATION_DATA}"  \
         ${BASE_URL}${AUTHENTICATION_URL} \
    );
  if [ $authenticate -ne 200 ];
  then
    echo "";
  else
    SESSION_ID=$(  \
      curl  \
        -i    \
        -X ${REQUEST_TYPE_POST}   \
        -H "Accept: application/json"   \
        -s  \
        --data "${AUTHENTICATION_DATA}" \
        $BASE_URL${AUTHENTICATION_URL} |  \
        grep JSESSIONID |  \
        awk '{print $2}' |   \
        sed s/\;//g |   \
        awk -F[=] '{print $2}'  \
      );
    echo $SESSION_ID;
  fi

}


###
# To get the session id from the server if the
# current session has expired it. If the current session has expired means on the
# argument URL_CONSTANT_TO_CHECK_CONNECTION the status server was 401,404 or 501
# then a request for new session id will be made by calling the fn_authenticate() function
# Arguments:
#   URL_CONSTANT_TO_CHECK_CONNECTION*
#     - a constant url to validate the session id
#
function fn_check_URL(){

  assert_variable_is_set "COORDINATOR_SERVER_HOST" "${COORDINATOR_SERVER_HOST}"
  assert_variable_is_set "COORDINATOR_SERVER_PORT" "${COORDINATOR_SERVER_PORT}"
  assert_variable_is_set "NAMESPACE" "${NAMESPACE}"

  SERVER_NOT_RUNNING_VAR="";

  server_response=$(  \
    curl -o /dev/null   \
      --silent   \
      --write-out '%{http_code}\n'   \
      --cookie "JSESSIONID=$SESSION_ID"   \
      $BASE_URL$1  \
    );
  if [ $server_response -ne 200 ] ;
  then
    if [ $server_response -eq 401 ] || [ $server_response -eq 404 ] || [ $server_response -eq 501 ] ;
    then
      SESSION_ID=$(fn_authenticate);
    else
      SERVER_NOT_RUNNING_VAR=$SERVER_NOT_RUNNING;
    fi
  fi

}


###
# To get the ID of running flow instance for the given flow_id
# This tool does not support running of more then one instance
# of a flow at a time.
# Arguments:
#   flow_id*
#     - flow id for which the running instance id is to be fetched
#
function fn_get_current_flow_instance_running() {

  flow_id=$1

  assert_variable_is_set "flow_id" "${flow_id}"

  running_flow_id=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/flowInstances/anyInstanceRunning/$NAMESPACE/${flow_id})

  echo $running_flow_id

}


###
# To get the ID of running flow instance for the given flow_id
# and mark the last running flow instance as failed if there
# were any failed job instances found in mysql.
# This tool does not support running of more then one instance
# of a flow at a time.
# Arguments:
#   flow_id*
#     - flow id for which the running instance id is to be fetched
#
function fn_check_for_instance_running() {

  flow_id=$1

  assert_variable_is_set "flow_id" "${flow_id}"

  running_flow_instance_id=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/flowInstances/checkForInstanceRunning/$NAMESPACE/${flow_id})

  echo $running_flow_instance_id

}


###
# Generates a batch_id which is nothing but a linux timestamp
# which can be consumed in pig scripts of hive scripts to mark
# data like what data processed in a particular batch_id
#
function fn_generate_batch_id() {

  new_batch_id=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/batchs/newBatchId)

  echo $new_batch_id

}

###
# This function is a one time function or can be called on demand
# to add type of job in mysql. This should not be called on each and
# every instance of a job or flow.
# eg: there can be some types like pig,hive,sh,java,hadoop,scala etc
# Arguments:
#   job_type_name*
#     - name of job type
#   job_type_name_description*
#     - a shor description for the job type
#
function fn_create_new_job_type(){

  job_type_name=$1
  job_type_name_description=$2

  assert_variable_is_set "job_type_name" "${job_type_name}"
  assert_variable_is_set "job_type_name_description" "${job_type_name_description}"

  job_type_creation_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/jobTypes '{"name":"'"${job_type_name}"'","description":"'"${job_type_name_description}"'","namespace":"'"$NAMESPACE"'"}')
  echo $job_type_creation_response

}

###
# This function is used to get the job tyep id from Mysql
# eg: there can be some types like pig,hive,sh,java,hadoop,scala etc
# Arguments:
#   job_type_name*
#     - name of job type
#
function fn_get_job_type(){

  job_type_name=$1

  assert_variable_is_set "job_type_name" "${job_type_name}"

  job_type_creation_response=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/jobTypes/${job_type_name}/${NAMESPACE} )
  echo $job_type_creation_response

}


###
# A Flow is a collection of Jobs which are dependent on each
# other on the basis of some logic.
# Creation of new Flow can be achieved by this function
# Arguments:
#   flow_id*
#     - give a unique id to flow
#   flow_name*
#     - name of flow
#   flow_description*
#     - a shor description for flow
#
function fn_create_flow(){

  flow_id=$1
  flow_name=$2
  flow_description=$3


  assert_variable_is_set "flow_id" "${flow_id}"
  assert_variable_is_set "flow_name" "${flow_name}"
  assert_variable_is_set "flow_description" "${flow_description}"

  flow_creation_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/flows '{"flowId":"'"${flow_id}"'","name":"'"${flow_name}"'","description":"'"${flow_description}"'","namespace":"'"$NAMESPACE"'"}')
  echo $flow_creation_response

}

###
# A Flow is a collection of Jobs which are dependent on each
# other on the basis of some logic.
# Creation of new Flow can be achieved by this function
# Arguments:
#   job_id*
#     - a unique id for job
#   job_name*
#     - name of job
#   job_description*
#     - a shor description for job
#   job_type_id (Int)*
#     - the type of the newly created job
#   job_flow_id*
#     - flow id of the Flow to which the job will belong
#
function fn_add_job(){

  job_id=$1
  job_name=$2
  job_description=$3
  job_type_id=$4
  job_flow_id=$5

  assert_variable_is_set "job_id" "${job_id}"
  assert_variable_is_set "job_name" "${job_name}"
  assert_variable_is_set "job_description" "${job_description}"
  assert_variable_is_set "job_type_id" "${job_type_id}"
  assert_variable_is_set "job_flow_id" "${job_flow_id}"

  job_creation_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/jobs '{"jobId":"'"${job_id}"'","name":"'"${job_name}"'","description":"'"${job_description}"'","jobType":{"id":"'${job_type_id}'"},"flow":{"flowId":"'"${job_flow_id}"'"},"namespace":"'"$NAMESPACE"'"}')
  echo $job_creation_response

}

###
# To run a flow instance of a particular Flow.
# function will exit with error message if there
# is already an instance running of the given flow_id
# Arguments:
#   flow_instance_flow_id*
#     - Flow id corresponding to which run an instance
#
function fn_run_flow_instance(){

  flow_instance_flow_id=$1

  assert_variable_is_set "flow_instance_flow_id" "${flow_instance_flow_id}"

  instance_running=$(fn_get_current_flow_instance_running ${flow_instance_flow_id});
  if [ "$instance_running" != "" ];
  then

   exit_code=${FLOW_INSTANCE_RUNNING}
   failure_messages="A Flow Instance with ID ${instance_running} already running"

   fn_exit_with_failure_message "${exit_code}" "${failure_messages}"
  else
   running_flow_instance_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/flowInstances '{"startTime":0,"endTime":0,"status":"running","flow":{"flowId":"'"${flow_instance_flow_id}"'"},"namespace":"'"$NAMESPACE"'"}')

   echo $running_flow_instance_response;
  fi

}

###
# Fails a running instance of the Flow with given flow_id
# Arguments:
#   fail_instance_flow_id*
#     - Flow id corresponding to which fail running instance
#
function fn_fail_running_flow_instance(){

  fail_instance_flow_id=$1

  assert_variable_is_set "fail_instance_flow_id" "${fail_instance_flow_id}"

  fail_flow_instance_response=$(fn_curl $REQUEST_TYPE_POST ${BASE_URL}/api/flowInstances/changeStatusOfRunningFlowInstance/${NAMESPACE}/${fail_instance_flow_id} '{"startTime":0,"endTime":0,"status":"'"$STATUS_FAILED"'","flow":{"flowId":"'"${fail_instance_flow_id}"'"},"namespace":"'"$NAMESPACE"'"}')
  echo $fail_flow_instance_response;

}

###
# Flag a successful instance of the Flow with given flow_id
# Arguments:
#   successful_instance_flow_id*
#     - Flow id corresponding to which successful running instance
#
function fn_successful_running_flow_instance(){

  successful_instance_flow_id=$1

  assert_variable_is_set "successful_instance_flow_id" "${successful_instance_flow_id}"

  success_flow_instance_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/flowInstances/changeStatusOfRunningFlowInstance/$NAMESPACE/${successful_instance_flow_id} '{"startTime":0,"endTime":0,"status":"'"$STATUS_SUCCESSFUL"'","flow":{"flowId":"'"${successful_instance_flow_id}"'"},"namespace":"'"$NAMESPACE"'"}')
  echo $success_flow_instance_response;

}

###
# To run a job instance for the given Job_id
# Arguments:
#   job_instance_job_id*
#     - job id corresponding to which run an instance
#   flow_instance_id*
#     - flow instance id to which this job instance will belong
#
function fn_run_job_instance (){

  job_instance_job_id=$1
  flow_instance_id=$2

  assert_variable_is_set "job_instance_job_id" "${job_instance_job_id}"
  assert_variable_is_set "flow_instance_id" "${flow_instance_id}"

  creation_job_instance_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/jobInstances '{"startTime":0,"job":{"jobId":"'"${job_instance_job_id}"'"},"flowInstance":{"id":"'"${flow_instance_id}"'"},"endTime":0,"status":"running","namespace":"'"$NAMESPACE"'"}')
  echo $creation_job_instance_response;

}

###
# Flag a running job instance as successful
# Arguments:
#   successful_instance_job_id*
#     - job id of the running job instance
#
function fn_successful_running_job_instance() {

  successful_instance_job_id=$1

  assert_variable_is_set "successful_instance_job_id" "${successful_instance_job_id}"

  successful_job_instance_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/jobInstances/changeStatusOfRunningJobInstance/$NAMESPACE/${successful_instance_job_id} '{"startTime":0,"job":{"jobId":"'"${successful_instance_job_id}"'"},"flowInstance":{"id":0},"endTime":0,"status":"'"$STATUS_SUCCESSFUL"'","namespace":"'"$NAMESPACE"'"}')
  echo $successful_job_instance_response;

}

###
# Flag a running job instance as failed
# Arguments:
#   fail_instance_job_id*
#     - job id of the running job instance
#
function fn_fail_running_job_instance() {

  fail_instance_job_id=$1

  assert_variable_is_set "fail_instance_job_id" "${fail_instance_job_id}"

  fail_job_instance_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/jobInstances/changeStatusOfRunningJobInstance/$NAMESPACE/${fail_instance_job_id} '{"startTime":0,"job":{"jobId":"'"${fail_instance_job_id}"'"},"flowInstance":{"id":0},"endTime":0,"status":"'"$STATUS_FAILED"'","namespace":"'"$NAMESPACE"'"}')
  echo $fail_job_instance_response;

}

###
# Add muliple events for a job instance
# Multiple events can be added by calling this
# function multiple times
# Arguments:
#   job_instance_event_key*
#     - event key
#   job_instance_event_value*
#     - event value
#   job_instance_event_for_given_instance_id*
#     - job instance id to which the event key and value will be associated
#
function fn_add_job_instance_event() {

  job_instance_event_key=$1
  job_instance_event_value=$2
  job_instance_event_for_given_instance_id=$3

  assert_variable_is_set "job_instance_event_key" "${job_instance_event_key}"
  assert_variable_is_set "job_instance_event_value" "${job_instance_event_value}"
  assert_variable_is_set "job_instance_event_for_given_instance_id" "${job_instance_event_for_given_instance_id}"

  addition_events_to_job_instance_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/jobInstanceEvents '{"key":"'"${job_instance_event_key}"'","value":"'"${job_instance_event_value}"'","jobInstance":{"id":"'"${job_instance_event_for_given_instance_id}"'"},"namespace":"'"$NAMESPACE"'"}')
  echo $addition_events_to_job_instance_response;

}

###
# Add muliple events for a job
# Multiple events can be added by calling this
# function multiple times
# Arguments:
#   job_event_key*
#     - event key
#   job_event_value*
#     - event value
#   job_event_for_given_job_id*
#     - Job id to which the event key and value will be associated
#
function fn_add_job_event() {

  job_event_key=$1
  job_event_value=$2
  job_event_for_given_job_id=$3

  assert_variable_is_set "job_event_key" "${job_event_key}"
  assert_variable_is_set "job_event_value" "${job_event_value}"
  assert_variable_is_set "job_event_for_given_job_id" "${job_event_for_given_job_id}"

  addition_events_to_job_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/jobEvents '{"propertyKey":"'"${job_event_key}"'","propertyValue":"'"${job_event_value}"'","job":{"jobId":"'"${job_event_for_given_job_id}"'"},"namespace":"'"$NAMESPACE"'"}')
  echo $addition_events_to_job_response;

}


###
# Run a new batch for a flow
# Arguments:
#   run_batch_id*
#     - batch id for the current batch
#   run_batch_flow_id*
#     - flow id of the Flow for which to run the batch
#   run_batch_flow_instance_id*
#     - flow instance id of the Flow Instance for which to run the batch
#
function fn_run_new_batch (){

  run_batch_id=$1
  run_batch_flow_id=$2
  run_batch_flow_instance_id=$3

  assert_variable_is_set "run_batch_id" "${run_batch_id}"
  assert_variable_is_set "run_batch_flow_id" "${run_batch_flow_id}"
  assert_variable_is_set "run_batch_flow_instance_id" "${run_batch_flow_instance_id}"

  run_batch_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/batchs '{"batchId":"'"${run_batch_id}"'","startTime":0,"flow":{"flowId":"'"${run_batch_flow_id}"'"},"flowInstance":{"id":"'"${run_batch_flow_instance_id}"'"},"endTime":0,"status":"running","namespace":"'"$NAMESPACE"'"}')
  echo $run_batch_response;

}

###
# Flag a batch as failed
# Arguments:
#   fail_batch_id*
#     - batch id to which fail the batch
#   fail_batch_flow_id*
#     - flow id corresponding to which fail the batch
#
function fn_fail_batch (){

  fail_batch_id=$1
  fail_batch_flow_id=$2

  assert_variable_is_set "fail_batch_id" "${fail_batch_id}"
  assert_variable_is_set "fail_batch_flow_id" "${fail_batch_flow_id}"

  fail_batch_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/batchs/changeStatusOfRunningbatch/$NAMESPACE/${fail_batch_flow_id} '{"batchId":"'"${fail_batch_id}"'","startTime":0,"flow":{"flowId":"'"${fail_batch_flow_id}"'"},"flowInstance":{"id":0},"endTime":0,"status":"'"$STATUS_FAILED"'","namespace":"'"$NAMESPACE"'"}')
  echo $fail_batch_response;

}

###
# Flag a batch as successful
# Arguments:
#   successful_batch_id*
#     - batch id to which successful the batch
#   successful_batch_flow_id*
#     - flow id corresponding to which fail the batch
#
function fn_successful_batch (){

  successful_batch_id=$1
  successful_batch_flow_id=$2

  assert_variable_is_set "successful_batch_id" "${successful_batch_id}"
  assert_variable_is_set "successful_batch_flow_id" "${successful_batch_flow_id}"

  successful_batch_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/batchs/changeStatusOfRunningbatch/$NAMESPACE/${successful_batch_flow_id} '{"batchId":"'"${successful_batch_id}"'","startTime":0,"flow":{"flowId":"'"${successful_batch_flow_id}"'"},"flowInstance":{"id":0},"endTime":0,"status":"'"$STATUS_SUCCESSFUL"'","namespace":"'"$NAMESPACE"'"}')
  echo $successful_batch_response;

}

###
# To get the running batch id which can be consumed
# in further pig or hive scripts
# Arguments:
#   flow_id_of_running_batch*
#     - flow id for which to get the running batch id
#
function fn_get_running_batch_id(){

  flow_id_of_running_batch=$1

  assert_variable_is_set "flow_id_of_running_batch" "${flow_id_of_running_batch}"

  running_batch_id_response=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/batchs/runningBatchId/$NAMESPACE/${flow_id_of_running_batch})
  echo $running_batch_id_response;

}

###
# To get the last successful batch id which can be consumed
# in further pig or hive scripts
# Arguments:
#   flow_id_of_successful_batch*
#     - flow id for which to get the last successful batch id
#   job_id_of_successful_batch*
#     - job id of the given flow
#
function fn_get_last_successful_batch_id(){

  flow_id_of_successful_batch=$1
  job_id_of_successful_batch=$2

  assert_variable_is_set "flow_id_of_successful_batch" "${flow_id_of_successful_batch}"
  assert_variable_is_set "job_id_of_successful_batch" "${job_id_of_successful_batch}"

  last_successful_batch_id_response=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/batchs/lastSuccessfulBatchId/$NAMESPACE/${flow_id_of_successful_batch}/${job_id_of_successful_batch})
  echo $last_successful_batch_id_response;

}

###
# Get new partition range for the current batch
# Arguments:
#   new_partition_for_flow_id*
#     - flow id to which associate the new partitions
#   new_partition_for_batch_id*
#     - partition range for batch id
#   new_partition_for_jobId*
#     - job id in which this function will be called to mark a table
#
function fn_get_new_partition_range(){

  new_partition_for_flow_id=$1
  new_partition_for_batch_id=$2
  new_partition_for_jobId=$3

  assert_variable_is_set "new_partition_for_flow_id" "${new_partition_for_flow_id}"
  assert_variable_is_set "new_partition_for_batch_id" "${new_partition_for_batch_id}"
  assert_variable_is_set "new_partition_for_jobId" "${new_partition_for_jobId}"

  assert_variable_is_set "CURRENT_DB" "${CURRENT_DB}"
  assert_variable_is_set "CURRENT_TABLE" "${CURRENT_TABLE}"

  new_partition_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/marks/${new_partition_for_jobId} '{"partitionContainsColumnHeader":"'"$PARTITION_CONTAINS_EQUAL_SIGN"'","table":"'"$CURRENT_DB.$CURRENT_TABLE"'","flow":{"flowId":"'"${new_partition_for_flow_id}"'"},"batch":{"batchId":"'"${new_partition_for_batch_id}"'"},"namespace":"'"$NAMESPACE"'"}')
  echo $new_partition_response;

}


###
# Get old partition range for the given batchId
# Arguments:
#   old_partition_for_flow_id*
#     - flow id to which associate the new partitions
#   old_partition_for_batch_id*
#     - partition range for batch id
#
function fn_get_old_partition_range(){

  old_partition_for_flow_id=$1
  old_partition_for_batch_id=$2

  assert_variable_is_set "old_partition_for_flow_id" "${old_partition_for_flow_id}"
  assert_variable_is_set "old_partition_for_batch_id" "${old_partition_for_batch_id}"

  assert_variable_is_set "CURRENT_DB" "${CURRENT_DB}"
  assert_variable_is_set "CURRENT_TABLE" "${CURRENT_TABLE}"

  old_partition_response=$(fn_curl $REQUEST_TYPE_GET ${BASE_URL}/api/marks/${NAMESPACE}/${old_partition_for_batch_id}/${old_partition_for_flow_id}/${CURRENT_DB}.${CURRENT_TABLE})
  echo $old_partition_response;

}


###
# To get a value for a key which was stored
# as an event for a job instance in job instance
# events
# Arguments:
#   get_value_for_batch_id*
#     - batch_id of batch in which the events were inserted
#   get_value_for_flow_id*
#     - flow_id to which the job belongs
#   get_value_for_job_id*
#     - job_id of the job corresponding to which the
#       events were inserted
#   get_value_for_key*
#     - the key for which value needs to be extracted
#
function fn_get_job_instance_event_value(){

  get_value_for_batch_id=$1
  get_value_for_flow_id=$2
  get_value_for_job_id=$3
  get_value_for_key=$4

  assert_variable_is_set "get_value_for_batch_id" "${get_value_for_batch_id}"
  assert_variable_is_set "get_value_for_flow_id" "${get_value_for_flow_id}"
  assert_variable_is_set "get_value_for_job_id" "${get_value_for_job_id}"
  assert_variable_is_set "get_value_for_key" "${get_value_for_key}"

  get_value_response=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/marks/${NAMESPACE}/${get_value_for_batch_id}/${get_value_for_flow_id}/${get_value_for_job_id}/${get_value_for_key})
  echo $get_value_response

}

###
# To get a value for a key which was stored
# as an event for a job in job events
# Arguments:
#   get_value_for_job_id*
#     - job_id of the job corresponding to which the
#       events were inserted
#   get_value_for_key*
#     - the key for which value needs to be extracted
#
function fn_get_job_event_value(){
  get_value_for_job_id=$1
  get_value_for_key=$2

  assert_variable_is_set "get_value_for_job_id" "${get_value_for_job_id}"
  assert_variable_is_set "get_value_for_key" "${get_value_for_key}"

  get_value_response=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/jobEvents/${NAMESPACE}/${get_value_for_job_id}/${get_value_for_key})
  echo $get_value_response

}

###
# Set a table for a job
# multiple tables can be set at job level
# Arguments:
#   current_table*
#     - set table which will be used to mark inputs at every batch
#       the table should be give in db.table format
#   partition_contains_column_headers*
#     - to signify the physical partitions contains column headers
#
function fn_set_input_table_for_current_mark(){

  current_table=$1
  partition_contains_column_headers=$2

  assert_variable_is_set "current_table" "${current_table}"
  assert_variable_is_set "partition_contains_column_headers" "${partition_contains_column_headers}"

  if [[ ${current_table} == *"."* ]];
  then

    CURRENT_DB=$(echo ${current_table} | cut -d'.' -f1);

    CURRENT_TABLE=$(echo ${current_table} | cut -d'.' -f2);

    PARTITION_CONTAINS_EQUAL_SIGN=${partition_contains_column_headers};

  else
    exit_code=${TABLE_NOT_GIVEN_IN_RIGHT_FORMAT}
    failure_messages="please provide the table name in the format database_name.table_name and information of physical partitions containing column headers or not eg: true or false"
    fn_exit_with_failure_message "${exit_code}" "${failure_messages}"
  fi

}

###
# Get current table which is set at job level
#
function fn_get_current_table(){

  assert_variable_is_set "CURRENT_DB" "${CURRENT_DB}"
  assert_variable_is_set "CURRENT_TABLE" "${CURRENT_TABLE}"
  assert_variable_is_set "PARTITION_CONTAINS_EQUAL_SIGN" "${PARTITION_CONTAINS_EQUAL_SIGN}"

  echo $CURRENT_DB.$CURRENT_TABLE $PARTITION_CONTAINS_EQUAL_SIGN

}

###
#
# Arguments:
#   run_job_flow_id*
#     - flowId to which the job belongs
#   run_job_job_id*
#     - jobId of the job
#
function fn_run_job(){

  run_job_flow_id=$1
  run_job_job_id=$2

  assert_variable_is_set "run_job_flow_id" "${run_job_flow_id}"
  assert_variable_is_set "run_job_job_id" "${run_job_job_id}"

  run_job_running_flow_instance_id=$(fn_get_current_flow_instance_running $run_job_flow_id);

  run_job_running_job_instance_id=$(fn_run_job_instance $run_job_job_id $run_job_running_flow_instance_id);

  echo $run_job_running_job_instance_id

}

###
#
# Arguments:
#   partition_range*
#     - partition range in the format lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       Here lower_year=2014
#       upper_year=2015
#       lower_month=201402
#       upper_month=201502
#       lower_day=20140201
#       upper_day=20150201
#       lower_hour=2014020100
#       upper_hour=2015020100
#       lower_minute=201402010050
#       upper_minute=201502010050
#       so that lower_bound and upper_bound can be exported and be used in other functions
#
function fn_create_lower_bound_and_upper_bound(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  IFS=",";
  export $partition_range;

}


###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate lower year
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where lower_year=2014
#
function fn_get_lower_year(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  lower_year=`echo $lower_bound | cut -c1-4`

  echo $lower_year

}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate upper year
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where upper_year=2015
#
function fn_get_upper_year(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  upper_year=`echo $upper_bound | cut -c1-4`

  echo $upper_year
}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate lower month
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where lower_month=201402
#
function fn_get_lower_month(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  lower_month=`echo $lower_bound | cut -c5-10`

  echo $lower_month

}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate upper month
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where upper_month=201502
#
function fn_get_upper_month(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  upper_month=`echo $upper_bound | cut -c5-10`

  echo $upper_month

}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate lower day
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where lower_day=20140201
#
function fn_get_lower_day(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  lower_day=`echo $lower_bound | cut -c11-18`

  echo $lower_day

}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate upper day
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where upper_day=20150201
#
function fn_get_upper_day(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  upper_day=`echo $upper_bound | cut -c11-18`

  echo $upper_day

}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate lower hour
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where lower_hour=2014020100
#
function fn_get_lower_hour(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  lower_hour=`echo $lower_bound | cut -c19-28`

  echo $lower_hour

}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate upper hour
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where upper_hour=2015020100
#
function fn_get_upper_hour(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound "${partition_range}"

  upper_hour=`echo $upper_bound | cut -c19-28`

  echo $upper_hour

}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate lower minute
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where lower_minute=201402010050
#
function fn_get_lower_minute(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  lower_minute=`echo $lower_bound | cut -c29-40`

  echo $lower_minute

}

###
#
# Arguments:
#   partition_range*
#     - partition range from which to calculate upper minute
#       partition range must be give in the format
#       lower_bound=2014201402201402012014020100201402010050,upper_bound=2015201502201502012015020100201502010050
#       where upper_minute=201502010050
#
function fn_get_upper_minute(){

  partition_range=$1
  assert_variable_is_set "partition_range" "${partition_range}"

  fn_create_lower_bound_and_upper_bound $partition_range

  upper_minute=`echo $upper_bound | cut -c29-40`

  echo $upper_minute

}

###
#
# Add dependency of single/multiple batches and their
# corresponding jobs on one batch and its job
# Arguments:
#   batchId*
#     - batch Id whose dependency is to be found
#   jobId*
#     - job Id of the batch Id whose dependency is to be found
#   dependent_batch_job_list*
#     - batch Id and job Id on which the asked batch has the dependency
#     - format : batchId1:jobid1,batchid2:jobId2
#
function fn_add_batch_dependency(){

  batchId=$1
  jobId=$2
  dependent_batch_job_list=$3

  assert_variable_is_set "batchId" "${batchId}"
  assert_variable_is_set "jobId" "${jobId}"
  assert_variable_is_set "dependent_batch_job_list" "${dependent_batch_job_list}"

  batch_dependency_add_response=$(fn_curl $REQUEST_TYPE_POST $BASE_URL/api/batchDependencys/validateAndAdd '{"jobId":"'"${jobId}"'","namespace":"'"$NAMESPACE"'","batch":{"batchId":"'"${batchId}"'"}, "dependentBatchJobList":"'"${dependent_batch_job_list}"'"}')

  echo $batch_dependency_add_response
}

###
#
# To get the dependency of a batch and its corresponding job
# Arguments:
#   batchId*
#     - batch Id whose dependency is to be found
#   jobId*
#     - job Id of the batch Id whose dependency is to be found
#
function fn_get_batch_dependency(){

  batchId=$1
  jobId=$2

  assert_variable_is_set "batchId" "${batchId}"
  assert_variable_is_set "jobId" "${jobId}"

  get_batch_dependency_response=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/batchDependencys/getBatchDependencies/${batchId}/${jobId}/${NAMESPACE})

  echo $get_batch_dependency_response

}

###
# To parse the ouput string from fn_get_batch_dependency
# and return one batch Id and its job Id at a time
# Arguments:
#   batchIdJobIdList*
#     - the result from fn_get_batch_dependency, i.e. the batch dependency string
#
function fn_get_batch_dependency_pair(){

  batchIdJobIdList=$1

  if [[ $check -eq 0 ]]
  then
   export check=0
  fi

  IFS=',' read -ra NAMES <<< "$batchIdJobIdList"
  echo ${NAMES[$check]}

  if [[ ${NAMES[$check]} == "" ]]
  then
   echo "No more Batch Dependency exists"
  fi

  check=$(( check+1 ))
}

###
#
# To get all the unprocessed batch Ids for a flow
# or for a job
# Arguments
#   batchId*
#     - last successfully processed batch Id
#   jobId
#     - jobId corresponding to batchId
#
function fn_get_unprocessed_batch_id(){

  if [ "$#" -eq 1 ]
  then

  batchId=$1
  assert_variable_is_set "batchId" "${batchId}"

  getAllUnprocessedSuccessfulBatchId=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/batchs/getUnprocessedSuccessfulBatchId/${batchId}/${NAMESPACE})

  echo $getAllUnprocessedSuccessfulBatchId

  elif [ "$#" -eq 2 ]
  then

  batchId=$1
  jobId=$2

  assert_variable_is_set "batchId" "${batchId}"
  assert_variable_is_set "jobId" "${jobId}"

  getAllUnprocessedSuccessfulBatchIdWithJobId=$(fn_curl $REQUEST_TYPE_GET $BASE_URL/api/batchs/getUnprocessedSuccessfulBatchIdWithJobId/${batchId}/${jobId}/${NAMESPACE})

  echo $getAllUnprocessedSuccessfulBatchIdWithJobId

  fi
}

###
#
#   Check the response, exit with error if response contains error or print success message
# Arguments:
#   response
#   success_message
#   failure_message
#
function fn_check_and_exit_error_response(){

  response="$1"
  success_message="$2"
  failure_message="$3"

  echo ${response} | grep '"error":'
  if [ $? -eq 0 ]; then
    fn_log_error "$failure_message"
    fn_log_error "Response : ${response}"
    exit 100;
  else
    fn_log_info "$success_message"
  fi

}

###
#
#   Check the exit status, exit with error if status is non zero or print success message
# Arguments:
#   exit_status
#   success_message
#   failure_message
#
function validateExitStatusWithCoordinator(){
   if [ $1 -eq 0 ];
   then
       fn_log_info "$2";
   else
       fn_log_error "$3";
               fn_fail_running_job_instance "$4";
       exit 100;
   fi
}
