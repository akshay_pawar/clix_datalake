#!/bin/bash
###############################################################################
#                               General Details                               #
###############################################################################
#                                                                             #
# Name         : Generic TDCH Export Script                                   #
#                                                                             #
# Description  : This script to load data into TD work table                  #
#                                                                             #
# Author       : <Vishakha Sadawarte-tkmacmt>                                 #
#                                                                             #
###############################################################################
#                          Script Environment Setup                           #
###############################################################################
SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

#EXTRACT MODULE HOME FROM SCRIPT PATH
MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"

###############################################################################
#                           Import Dependencies                               #
###############################################################################
. ${MODULE_HOME}/bin/import-dependecies.sh
. ${COORDINATOR_HOME}/bin/functions.sh
. ${MODULE_HOME}/bin/project-functions.sh
. ${MODULE_HOME}/bin/module_functions.sh
. /kohls/file/is/ksetdblogin.dat

###############################################################################
#                               Module Start                                  #
###############################################################################

# READING PARAMETERS FROM AZKABAN

fn_read_command_line_parameters "$@"

SRC_TABLE_NAME_LOWER=`echo "$SRC_TABLE_NAME" | awk '{print tolower($0)}'`
. ${MODULE_HOME}/etc/${SRC_TABLE_NAME_LOWER}_td_export.properties

############## EXPORT TDCH CLASSPATHS ####################################

if [ -z $TDCH_JAR_PATH ];then
    export TDCH_JAR_PATH="${TDCH_JAR_PATH}"
fi

############## COORDINATOR RELATED JOBS #################################

fn_log_info "RUNNING JOB : ${TD_EXPORT_JOB} FROM FLOW : ${TD_EXPORT_FLOW}"

RUNNING_JOB_INSTANCE_ID=$(fn_run_job ${TD_EXPORT_FLOW} ${TD_EXPORT_JOB})
fn_check_and_exit_error_response "${RUNNING_JOB_INSTANCE_ID}" "Successfully created job instance." "Failed to create job instance. Please check!!"
fn_log_info "RUNNING_JOB_INSTANCE_ID : $RUNNING_JOB_INSTANCE_ID"

BATCH_ID=$(fn_get_running_batch_id $TD_EXPORT_FLOW)
fn_check_and_exit_error_response "${BATCH_ID}" "Successfully fetched running batch_id." "Failed to fetch running batch_id. Please check!!"
fn_log_info "RUNNING_BATCH_ID: ${BATCH_ID}"

############## CHECK FOR INPUT DATA ####################################
fn_log_info "Checking for input data."

RECORD_COUNT=`executeHiveQuery -e "SELECT COUNT(*) FROM $HIVE_DATABASE_NAME_INSIGHT.$HIVE_TABLE ;"`
if [ $? -eq 0 ];
then
    fn_log_info "RECORD_COUNT : ${RECORD_COUNT}"
else
    fn_log_warn "FAILED TO GET RECORD COUNT FROM $HIVE_DATABASE_NAME_INSIGHT.$HIVE_TABLE"
fi

if [ $RECORD_COUNT -eq 0 ];
then
    fn_log_warn "No data present at source table $HIVE_DATABASE_NAME_INSIGHT.$HIVE_TABLE"
    fn_successful_running_job_instance $TD_EXPORT_JOB
    exit 0
fi

fn_log_info "CREATING TABLE AT TEMP LOCATION"
fn_run_hive_and_return_exit_status --hivevar HIVE_DATABASE_NAME_WORK="${HIVE_DATABASE_NAME_WORK}" --hivevar TEMP_TABLE_NAME="${TEMP_TABLE_NAME}" --hivevar DATA_LAYER_DIR_WORK="${DATA_LAYER_DIR_WORK}" -f ${MODULE_HOME}/hive/create_${HIVE_TABLE}_td_export.hql

if [ $? -ne 0 ]
then
    fn_log_info   "Failed to create hive temp table";
    fn_fail_running_job_instance $TD_EXPORT_JOB
    exit 1
fi

############## MOVING DATA TO TEMP TABLE ################################
FILE_LOAD_TIME=`date +"%Y-%m-%d %H:%M:%S"`
FILE_LOAD_TIME_MS=`echo "\`date +%N\` / 1000" | bc`
FILE_LOAD_TIMESTAMP="$FILE_LOAD_TIME.$FILE_LOAD_TIME_MS"

fn_log_info "[COMMAND] executeHive --hivevar HIVE_DATABASE_NAME_INSIGHT=${HIVE_DATABASE_NAME_INSIGHT} --hivevar HIVE_TABLE=${HIVE_TABLE} --hivevar HIVE_DATABASE_NAME_WORK=${HIVE_DATABASE_NAME_WORK} --hivevar TEMP_TABLE_NAME=${TEMP_TABLE_NAME} --hivevar FILE_LOAD_TIMESTAMP=\"${FILE_LOAD_TIMESTAMP}\" -f ${MODULE_HOME}/hive/insert_${HIVE_TABLE}_td_export.hql"

fn_run_hive_and_return_exit_status --hivevar HIVE_DATABASE_NAME_INSIGHT=${HIVE_DATABASE_NAME_INSIGHT} --hivevar HIVE_TABLE=${HIVE_TABLE} --hivevar HIVE_DATABASE_NAME_WORK=${HIVE_DATABASE_NAME_WORK} --hivevar TEMP_TABLE_NAME=${TEMP_TABLE_NAME} --hivevar FILE_LOAD_TIMESTAMP="${FILE_LOAD_TIMESTAMP}" -f ${MODULE_HOME}/hive/insert_${HIVE_TABLE}_td_export.hql

hive_res=$?
if [ $hive_res -ne 0 ]
then
    fn_log_warn "MOVING DATA TO TEMP TABLE FAILED"
    fn_log_warn "Marking job : $RUNNING_JOB_INSTANCE_ID failed"
    fn_fail_running_job_instance "$TD_EXPORT_JOB"
    fn_log_error "TDCH Export [FAILED]"
    exit 1
fi

############## TRUNCATE TERADATA WORK TABLE #############################

SQOOP_EVAL_CMD="sqoop eval \
--connect \"${TD_EXPORT_CONNECTION_URL}\" \
--username \"${TD_EXPORT_USER}\" \
--password ${HDP_EDL_PASSWD} \
--connection-manager org.apache.sqoop.teradata.TeradataConnManager  \
--query \"DELETE FROM ${TD_DB_WORK}.${TD_EXPORT_WORK_TABLE} ALL\""

SQOOP_EVAL_CMD_INFO=`echo ${SQOOP_EVAL_CMD} | sed "s/--password *$HDP_EDL_PASSWD*/--password xxxxxxxxxxx /g"`

fn_log_info "${SQOOP_EVAL_CMD_INFO}"

eval $SQOOP_EVAL_CMD

RES=$?
if [ $RES -ne 0 ]
then
    fn_log_error "FAILED TO DELETE DATA AT TERADATA WORK TABLE"
    fn_log_error "Marking job : $RUNNING_JOB_INSTANCE_ID failed"
    fn_fail_running_job_instance "$TD_EXPORT_JOB"
    fn_log_error "TDCH Export [FAILED]"
    exit 1
fi

SOURCEPATH=`executeHiveQuery -e "describe extended ${HIVE_DATABASE_NAME_WORK}.${TEMP_TABLE_NAME}" |  perl -n -e'while(/location:([\_0-9a-zA-Z:\/\/-]+),/g) {print "$1\n"}'`

############## TDCH JOB #################################################

tdch_command="hadoop jar $TDCH_JAR_PATH com.teradata.connector.common.tool.ConnectorExportTool \
-D mapreduce.task.timeout=12000000 \
-D mapreduce.job.queuename=${QUEUE_NAME} \
-D mapreduce.map.java.opts=-Xmx3584m \
-url \"${TD_EXPORT_CONNECTION_URL},CHARSET=UTF8\" \
-username \"${TD_EXPORT_USER}\" \
-password ${HDP_EDL_PASSWD} \
-classname com.teradata.jdbc.TeraDriver \
-fileformat textfile \
-jobtype hdfs \
-targettable \"${TD_DB_WORK}.${TD_EXPORT_WORK_TABLE}\" \
-sourcepaths \"${SOURCEPATH}\" \
-nummappers ${MAPPERS} \
-separator \"\\u0001\" \
-nullstring \"null\" \
-nullnonstring \"null\" \
-errortabledatabase \"${TD_EXPORT_ERROR_DB}\" \
-errortablename DDH_EX_ERR"

tdch_info_command=`echo ${tdch_command} | sed "s/-password *$HDP_EDL_PASSWD*/-password xxxxxxxxxxx /g"`

fn_log_info "$tdch_info_command"

eval $tdch_command

RC=$?
if [ $RC -ne 0 ];
then
    fn_log_info "JOB FAILED!!!!!!"
    fn_log_error "DROPPING TEMP TABLE"
    fn_run_hive_and_return_exit_status --hivevar HIVE_DATABASE_NAME_WORK="${HIVE_DATABASE_NAME_WORK}" --hivevar TEMP_TABLE_NAME="${TEMP_TABLE_NAME}"  -f ${MODULE_HOME}/hive/drop_${HIVE_TABLE}_td_export.hql
    fn_log_warn "Marking job : $RUNNING_JOB_INSTANCE_ID failed"
    fn_fail_running_job_instance "$TD_EXPORT_JOB"
    fn_log_error "TDCH Export [FAILED]"
    exit 1
else
    fn_log_info "JOB SUCCESSFUL"
    fn_log_info "DROPPING TEMP TABLE"
    fn_run_hive_and_return_exit_status --hivevar HIVE_DATABASE_NAME_WORK="${HIVE_DATABASE_NAME_WORK}" --hivevar TEMP_TABLE_NAME="${TEMP_TABLE_NAME}"  -f ${MODULE_HOME}/hive/drop_${HIVE_TABLE}_td_export.hql
    fn_log_info "Marking job : $RUNNING_JOB_INSTANCE_ID successful"
    fn_successful_running_job_instance "$TD_EXPORT_JOB"
    fn_log_info "TDCH Export [SUCCESSFUL]"
fi
