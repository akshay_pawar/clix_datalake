#!/bin/bash
###############################################################################
#                               Documentation                                 #
###############################################################################
#                                                                             #
# Description                                                                 #
#     : This script contains declaration of all dependent scripts             #
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################
#                               Declarations                                  #
###############################################################################

COMMON_MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"
PROJECT_HOME="`dirname "${COMMON_MODULE_HOME}"`"

. ${COMMON_MODULE_HOME}/bin/namespace_functions.sh
user_namespace_param_file=$(getNamespaceFileForCurrentUser)
echo "user_namespace_param_file = $user_namespace_param_file"

. ${PROJECT_HOME}/etc/${user_namespace_param_file}
. ${COMMON_MODULE_HOME}/bin/functions.sh
. ${COMMON_MODULE_HOME}/bin/utility_functions.sh
. ${COMMON_MODULE_HOME}/bin/constants.sh
. ${COMMON_MODULE_HOME}/bin/module_functions.sh
. ${COMMON_MODULE_HOME}/bin/project_functions.sh
. ${COMMON_MODULE_HOME}/bin/log-functions.sh
. ${COMMON_MODULE_HOME}/bin/common-functions.sh
. ${COMMON_MODULE_HOME}/bin/hadoop-functions.sh

###############################################################################
#                                     End                                     #
###############################################################################