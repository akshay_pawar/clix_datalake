#!/bin/bash
###############################################################################
#                               Documentation                               #
###############################################################################
#                                                                             #
# Description                                                                 #
#     : This script consists of all the hadoop related utility functions.      #
#                                                                             #
# Note                                                                        #
#     : 1) If function argument ends with * then its required argument.       #
#       2) If function argument ends with ? then its optional argument.       #
#                                                                             #
###############################################################################
#                             Function Definitions                            #
###############################################################################

###
# Delete HDFS directory
#
# Arguments:
#   directory*
#     - path of the directory to be created
#   fail_on_error?
#     - Flag to decide, in case of failure of this operation, wheather to exit the process
#       with error code or just write error message and return.
#
function fn_delete_hdfs_directory(){

  fn_assert_executable_exists "hdfs" "${BOOLEAN_TRUE}"

  directory=$1

  fail_on_error=$2

  fn_assert_variable_is_set "directory" "${directory}"

  hdfs dfs -test -e "${directory}"

  exit_code=`fn_get_exit_code $?`

  if [ "${exit_code}" == "$EXIT_CODE_SUCCESS" ]
  then

    hdfs dfs -rm -r "${directory}"

    exit_code=`fn_get_exit_code $?`

    success_message="Deleted hdfs directory ${directory}"

    failure_message="Failed to delete hdfs directory ${directory}"

    fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

  else

    fn_log_info "HDFS directory ${directory} does not exist"

  fi

}

###
# Create HDFS directory
#
# Arguments:
#   directory*
#     - path of the directory to be created
#   fail_on_error?
#     - Flag to decide, in case of failure of this operation, wheather to exit the process
#       with error code or just write error message and return.
#
function fn_create_hdfs_directory(){

  fn_assert_executable_exists "hdfs" "${BOOLEAN_TRUE}"

  directory=$1

  fail_on_error=$2

  fn_assert_variable_is_set "directory" "${directory}"

  hdfs dfs -test -e "${directory}"

  exit_code=`fn_get_exit_code $?`

  if [ "${exit_code}" != "$EXIT_CODE_SUCCESS" ]
  then

    hdfs dfs -mkdir -p "${directory}"

    exit_code=`fn_get_exit_code $?`

    success_message="Created HDFS directory ${directory}"

    failure_message="Failed to create HDFS directory ${directory}"

    fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

  else

    fn_log_info "HDFS directory ${directory} already exists"

  fi
}


function fn_chmod_hdfs_directory(){

  fn_assert_executable_exists "hdfs" "${BOOLEAN_TRUE}"

  directory=$1

  dir_chmod=$2

  recursive=$3

  fail_on_error=$4

  fn_assert_variable_is_set "directory" "${directory}"

  hdfs dfs -test -e "${directory}"

  exit_code=`fn_get_exit_code $?`

  if [ "${exit_code}" != "$EXIT_CODE_SUCCESS" ]
  then

    fn_log_warn "HDFS directory ${directory} does not exists"

  else

    if [ "${recursive}" != "$BOOLEAN_TRUE" ]
    then

      hdfs dfs -chmod ${dir_chmod} "${directory}"

    else

      hdfs dfs -chmod -R ${dir_chmod} "${directory}"

    fi

    exit_code=`fn_get_exit_code $?`

    success_message="Updated chmod for HDFS directory ${directory} to ${dir_chmod}"

    failure_message="Failed to update chmod for HDFS directory ${directory} to ${dir_chmod}"

    fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

  fi
}


function fn_chown_hdfs_directory(){

  fn_assert_executable_exists "hdfs" "${BOOLEAN_TRUE}"

  directory=$1

  user_group=$2

  recursive=$3

  fail_on_error=$4

  fn_assert_variable_is_set "directory" "${directory}"

  hdfs dfs -test -e "${directory}"

  exit_code=`fn_get_exit_code $?`

  if [ "${exit_code}" != "$EXIT_CODE_SUCCESS" ]
  then

    fn_log_warn "HDFS directory ${directory} does not exists"

  else

    if [ "${recursive}" != "$BOOLEAN_TRUE" ]
    then

      hdfs dfs -chown ${user_group} "${directory}"

    else

      hdfs dfs -chown -R ${user_group} "${directory}"

    fi

    exit_code=`fn_get_exit_code $?`

    success_message="Updated chown for HDFS directory ${directory} to ${user_group}"

    failure_message="Failed to update chown for HDFS directory ${directory} to ${user_group}"

    fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

  fi
}


###
# Create Kafka Topic
#
# Arguments:
#
#   fail_on_error?
#     - Flag to decide, in case of failure of this operation, wheather to exit the process
#       with error code or just write error message and return.
#
function fn_execute_kafka_topics_command(){

  fn_assert_variable_is_set "KAFKA_HOME" "${KAFKA_HOME}"

  fn_log_info "Executing kafka-topics.sh command with arguments $@"

  ${KAFKA_HOME}/bin/kafka-topics.sh "$@"

  exit_code=`fn_get_exit_code $?`

  fail_on_error=${BOOLEAN_TRUE}

  success_message="Successfully executed kafka-topics.sh command"

  failure_message="Failed to execute kafka-topics.sh command"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}


function fn_create_external_hive_database(){

  fn_assert_executable_exists "hive" "${BOOLEAN_TRUE}"

  db_name=$1

  db_location=$2

  fail_on_error=$3

  fn_assert_variable_is_set "db_name" "${db_name}"

  fn_assert_variable_is_set "db_location" "${db_location}"

  hive -i "${CONFIG_HOME}/hive-env.properties" -e "CREATE DATABASE IF NOT EXISTS ${db_name} LOCATION '${db_location}'"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully created ${db_name} database"

  failure_message="Failed to create ${db_name} database"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}


function fn_run_hive(){

  fn_assert_executable_exists "hive" "${BOOLEAN_TRUE}"

  module_home="${1}"

  fn_assert_variable_is_set "Module Home" "${1}"

  hive_initialization_script="$2"

  fn_assert_file_exists "${hive_initialization_script}" "${BOOLEAN_TRUE}"

  hive_script="$3"

  fn_assert_file_exists "${hive_script}" "${BOOLEAN_TRUE}"

  fail_on_error=$4

  fn_assert_variable_is_set "hive_script" "${hive_script}"

  hive -i "${CONFIG_HOME}/hive-env.properties" -i "${hive_initialization_script}" -f "${hive_script}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully executed hive script ${hive_script}"

  failure_message="Failed to execute hive script ${hive_script}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}


function fn_update_hive_partition_metadata(){

  fn_assert_executable_exists "hive" "${BOOLEAN_TRUE}"

  hive_table="$1"

  fn_assert_variable_is_set "hive_table" "${hive_table}"

  fail_on_error="$2"

  hive -i "${CONFIG_HOME}/hive-env.properties" -e "MSCK REPAIR TABLE ${hive_table}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully updated hive table ${hive_table} partition metadata"

  failure_message="Failed to update hive table ${hive_table} partition metadata"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}


###
# Create Kafka Topic
#
# Arguments:
#
#   fail_on_error?
#     - Flag to decide, in case of failure of this operation, wheather to exit the process
#       with error code or just write error message and return.
#
function fn_create_kafka_topic(){

  fn_assert_variable_is_set "KAFKA_HOME" "${KAFKA_HOME}"

  fn_assert_variable_is_set "KAFKA_ZOOKEEPER_CONNECTION_STRING" "${KAFKA_ZOOKEEPER_CONNECTION_STRING}"

  topic_name=$1

  no_of_partitions=$2

  replication_factor=$3

  fn_assert_variable_is_set "topic_name" "${topic_name}"

  fn_assert_variable_is_set "no_of_partitions" "${no_of_partitions}"

  fn_assert_variable_is_set "replication_factor" "${replication_factor}"

  fail_on_error=$4

  ${KAFKA_HOME}/bin/kafka-topics.sh --zookeeper ${KAFKA_ZOOKEEPER_CONNECTION_STRING} --list | grep -Fx  "${topic_name}"

  topic_exists=`fn_get_exit_code $?`

  if [ "${topic_exists}" == "$EXIT_CODE_SUCCESS" ]
  then

    fn_log_warn "Topic ${topic_name} already exists"

  else

    fn_log_info "Creating kafka topic with arguments $@"

    ${KAFKA_HOME}/bin/kafka-topics.sh \
    --zookeeper ${KAFKA_ZOOKEEPER_CONNECTION_STRING} \
    --create \
    --topic ${topic_name} \
    --partitions ${no_of_partitions} \
    --replication-factor ${replication_factor}

    exit_code=`fn_get_exit_code $?`

    success_message="Successfully created  kafka topic ${topic_name}"

    failure_message="Failed to create kafka topic ${topic_name}"

    fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

  fi

}


function fn_run_kafka_mirror(){

  fn_assert_variable_is_set "KAFKA_HOME" "${KAFKA_HOME}"

  fn_assert_variable_is_set "Consumer Config File" "$1"

  fn_assert_variable_is_set "Producer Config File" "$2"

  fn_assert_variable_is_set "Number of Streams" "$3"

  fn_assert_variable_is_set "Topic Whitelist" "$4"

  ${KAFKA_HOME}/bin/kafka-run-class.sh kafka.tools.MirrorMaker --consumer.config "$1" --producer.config "$2" --num.streams $3 --whitelist "$4"

  exit_code=`fn_get_exit_code $?`

  fn_exit "${exit_code}"

}


function fn_run_camus(){

  fn_assert_executable_exists "hadoop" "${BOOLEAN_TRUE}"

  fn_assert_variable_is_set "CAMUS_HOME" "${CAMUS_HOME}"

  fn_assert_variable_is_set "MODULE_HOME" "$1"

  fn_assert_variable_is_set "Camus Properties" "$2"

  fn_assert_variable_is_set "Output table name" "$3"

  ${CAMUS_HOME}/bin/camus.sh \
  -Dconf.dir=${MODULE_HOME}/etc/camus \
  -P "$2"

  exit_code=`fn_get_exit_code $?`

  if [ ${exit_code} -eq $EXIT_CODE_SUCCESS ]
  then

    fn_update_hive_partition_metadata "$3" "${BOOLEAN_TRUE}"

  else

    fn_exit "${exit_code}"

  fi


}



function fn_run_pig(){

  fn_assert_executable_exists "pig" "${BOOLEAN_TRUE}"

  module_home="${1}"

  pig_properties_file="${2}"

  pig_script_file="${3}"

  fn_assert_variable_is_set "module_home" "${module_home}"

  fn_assert_variable_is_set "pig_properties_file" "${pig_properties_file}"

  fn_assert_variable_is_set "pig_script_file" "${pig_script_file}"

  PIG_ADDITIONAL_PARAMS="-param MODULE_HOME=${module_home} ${PIG_ADDITIONAL_PARAMS}"

  PIG_ADDITIONAL_JARS="${SHARED_LIB}/piggy-bank.jar"

  for i in $module_home/lib/*.jar; do
    PIG_ADDITIONAL_JARS=$PIG_ADDITIONAL_JARS:$i
  done

  pig -Dudf.import.list=com.kohls.bigdata.ecom.piggybank \
  -Dpig.additional.jars=${PIG_ADDITIONAL_JARS} \
  -useHCatalog ${PIG_ADDITIONAL_PARAMS} \
  -param_file "${CONFIG_HOME}/pig-env.properties" \
  -param_file "${pig_properties_file}" "${pig_script_file}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully executed pig script ${pig_script_file}"

  failure_message="Failed to execute pig script ${pig_script_file}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${BOOLEAN_TRUE}"


}


function fn_hadoop_download_file(){

  hadoop_file="$1"

  target_file="$2"

  fail_on_error="$3"

  fn_assert_executable_exists "hadoop" "${BOOLEAN_TRUE}"

  fn_assert_variable_is_set "hadoop_file" "${hadoop_file}"

  fn_assert_variable_is_set "target_file" "${target_file}"

  hadoop fs -copyToLocal "${hadoop_file}" "${target_file}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully downloaded hadoop file ${hadoop_file}"

  failure_message="Failed to download hadoop file ${hadoop_file} to local file ${target_file}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}


function fn_run_hive_to_redshift_export(){

  fn_assert_variable_is_set "TOOLS_INSTALL_DIR" "${TOOLS_INSTALL_DIR}"

  fn_assert_executable_exists "${TOOLS_INSTALL_DIR}/redshift-export/bin/redshift-export.sh" "${BOOLEAN_TRUE}"

  module_home="${1}"

  configuration_file="${2}"

  app_name="${3}"

  input_table="${4}"

  output_table="${5}"

  fn_assert_variable_is_set "module_home" "${module_home}"

  fn_assert_variable_is_set "configuration_file" "${configuration_file}"

  fn_assert_variable_is_set "app_name" "${app_name}"

  fn_assert_variable_is_set "input_table" "${input_table}"

  fn_assert_variable_is_set "output_table" "${output_table}"

  ${TOOLS_INSTALL_DIR}/redshift-export/bin/redshift-export.sh \
  "${configuration_file}" \
  "${app_name}" \
  "${input_table}" \
  "${output_table}"
}

function fn_export_history_to_backup(){


  mount_path="${1}"

  export_path="${2}"

  timestamp=$(date +%Y%m%d%H%M)

  fn_assert_variable_is_set "mount_path" "${mount_path}"

  fn_assert_variable_is_set "export_path" "${export_path}"

  hdfs dfs -mkdir ${export_path}/${timestamp}

  hdfs dfs -cp -f "${mount_path}" \
  ${export_path}/${timestamp}

  exit_code=`fn_get_exit_code $?`

  if [ ${exit_code} -eq $EXIT_CODE_SUCCESS ]
  then

    hdfs dfs -rm -r "${mount_path}/*"

  else

    fn_exit "${exit_code}"

  fi

  success_message="Successfully transferred History load to  Backup ${export_path} "

  failure_message="Failed to transfer to Backup ${export_path}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"
}


function fn_copy_file_from_local_to_hadoop(){

  local_file="$1"

  hadoop_directory="$2"

  overwrite=${3:-false}

  fail_on_error="$4"

  fn_assert_variable_is_set "local_file" "${local_file}"

  fn_assert_variable_is_set "hadoop_directory" "${hadoop_directory}"

  fn_assert_variable_is_set "overwrite" "${overwrite}"

  if [ "${overwrite}" == "false" ]
  then

    hdfs dfs -copyFromLocal  "${local_file}" "${hadoop_directory}"

  else

    if [ "${overwrite}" == "true" -o "TRUE" ]
    then

      hdfs dfs -copyFromLocal -f  "${local_file}" "${hadoop_directory}"

    fi
  fi


  exit_code=`fn_get_exit_code $?`

  success_message="Successfully copied local file ${local_file} to hadoop directory ${hadoop_directory}"

  failure_message="Failed to copy local file ${local_file} to hadoop directory ${hadoop_directory}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}

function fn_drop_hive_partition(){

  table_name="$1"

  partition_name="$2"

  partition_value="$3"

  fail_on_error="$4"

  fn_assert_variable_is_set "table_name" "${table_name}"

  fn_assert_variable_is_set "partition_name" "${partition_name}"

  fn_assert_variable_is_set "partition_value" "${partition_value}"

  hive -i "${CONFIG_HOME}/hive-env.properties" -e "ALTER TABLE ${table_name} DROP IF EXISTS PARTITION ( ${partition_name} = ${partition_value} );"

  exit_code=`fn_get_exit_code $?`

  success_message="Table Partition deleted successfully"

  failure_message="Failed to delete the table partition"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${fail_on_error}"

}


function fn_run_redshift_script(){

  fn_assert_variable_is_set "TOOLS_INSTALL_DIR" "${TOOLS_INSTALL_DIR}"

  fn_assert_executable_exists "${TOOLS_INSTALL_DIR}/redshift-connector/bin/redshift-connector.sh" "${BOOLEAN_TRUE}"

  redshift_credentials_file="${1}"

  redshift_script="${2}"

  fn_assert_variable_is_set "redshift_credentials_file" "${redshift_credentials_file}"

  fn_assert_variable_is_set "redshift_script" "${redshift_script}"

  ${TOOLS_INSTALL_DIR}/redshift-connector/bin/redshift-connector.sh \
  "${redshift_credentials_file}" \
  "${redshift_script}"

  exit_code=`fn_get_exit_code $?`

  success_message="Successfully executed redshift script ${redshift_script}"

  failure_message="Failed to execute redshift script ${redshift_script}"

  fn_handle_exit_code "${exit_code}" "${success_message}" "${failure_message}" "${BOOLEAN_TRUE}"

}

#Read Parameters From Azkaban
function fn_read_command_line_parameters() {
  fn_log_info "Parsing args ..."
  for item in "$@"; do
    case $item in
    (*=*) eval $item;;
    esac
  done
  fn_log_info "Done parsing args ..!!!"
}

function fn_get_formatted_date() {
  local IN_DATE="$1"
  fn_assert_variable_is_set "IN_DATE" "${IN_DATE}"
  formattedDate=$(date -d "$1" +"%Y%m%d")
  echo ${formattedDate}
}

#function to validate dates
function fn_validate_start_and_end_dates () {
  local START_DATE="$1";
  local END_DATE="$2";

  fn_assert_variable_is_set "START_DATE" "${START_DATE}"
  fn_assert_variable_is_set "END_DATE" "${END_DATE}"

  local F_START_DATE=$(fn_get_formatted_date "$START_DATE")
  local F_END_DATE=$(fn_get_formatted_date "$END_DATE")
  fn_log_info "Start Date is $START_DATE and End Date is $END_DATE "
  if [ "$F_START_DATE" -gt "$F_END_DATE" ];
  then
    fn_log_error "Start date must be lesser than end date.";
    exit 100;
  fi
}

function fn_run_hive_and_return_exit_status(){

  fn_assert_executable_exists "hive" "${BOOLEAN_TRUE}"

  #Assigning default mapreduce job queuename if QUEUE_NAME is empty
  if [ -z "${QUEUE_NAME}" ]; then
    QUEUE_NAME="long_running"
  fi

  COMMAND=`echo "hive --hiveconf mapreduce.job.queuename="${QUEUE_NAME}" -i "${CONFIG_HOME}/hive-env.properties" "$@"" | sed 's/  */ /g'`

  echo "Executing Command : ${COMMAND}"

  hive --hiveconf mapreduce.job.queuename="${QUEUE_NAME}" -i "${CONFIG_HOME}/hive-env.properties" "$@"

  exit_code=`fn_get_exit_code $?`

  return ${exit_code}

}


function fn_run_pig_and_return_exit_status(){

  fn_assert_executable_exists "pig" "${BOOLEAN_TRUE}"

  #Assigning default mapreduce job queuename if QUEUE_NAME is empty
  if [ -z "${QUEUE_NAME}" ]; then
    QUEUE_NAME="long_running"
  fi

  COMMAND=`echo "pig -Dudf.import.list=com.kohls.bigdata.ecom.piggybank \
  -Dmapreduce.job.queuename="${QUEUE_NAME}" \
  -useHCatalog \
  -param_file "${CONFIG_HOME}/pig-env.properties" "$@"" | sed 's/  */ /g'`

  echo "Executing Command : ${COMMAND}"

  pig -Dudf.import.list=com.kohls.bigdata.ecom.piggybank \
  -Dmapreduce.job.queuename="${QUEUE_NAME}" \
  -useHCatalog \
  -param_file "${CONFIG_HOME}/pig-env.properties" "$@"

  exit_code=`fn_get_exit_code $?`

  return ${exit_code}

}

# Function to execute hive commands
function executeHiveQuery(){

  #Assigning default mapreduce job queuename if QUEUE_NAME is empty
  if [ -z "${QUEUE_NAME}" ]; then
    QUEUE_NAME="long_running"
  fi

  hive --hiveconf mapreduce.job.queuename="${QUEUE_NAME}" "$@"

}

function fn_check_and_exit_error_response(){

  response="$1"
  success_message="$2"
  failure_message="$3"

  echo ${response} | grep '"error":'
  if [ $? -eq 0 ]; then
    fn_log_info "$failure_message"
    fn_log_error "Response : ${response}"
    exit 100;
  else
    fn_log_info "$success_message"
  fi

}

# THIS FUNCTION CHECKS VALID DATE AND FORMAT OF THE DATE
# FORMAT CHECK: YYYY-MM-DD
function fn_check_format_and_valid_date() {

local VERIFY_DATE="$1"

echo "${VERIFY_DATE}" | grep "[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}"
if [ $? -ne 0 ]; then
    fn_log_error "INVALID DATE FORMAT i.e ${VERIFY_DATE}. EXPECTED DATE FORMAT IS YYYY-MM-DD. PLEASE CHECK!!"
    exit 100
else
    date "+%Y-%m-%d" -d "${VERIFY_DATE}" > /dev/null
    if [ $? -ne 0 ]; then
    fn_log_error "INVALID DATE PROVIDED i.e ${VERIFY_DATE}. PLEASE CHECK!!"
    exit 100
    fi
fi

}

##################################
# This function is used for Import from Teradata using TDCH for full load
# PARAMETERS - module_home,teradata database, teradata sourcetable name,
# HDFS targetpath, num_mappers,
##############################
function fn_tdch_import_full()
{
    SPLIT_BY_COLUMN="$1"
    SPLIT_METHOD="$2"
    HDFS_TARGET_LOCATION="$3"
    NO_OF_MAPPERS="$4"
    SOURCE_QUERY="$5"

    echo " hadoop jar ${TDCH_JAR_PATH} com.teradata.connector.common.tool.ConnectorImportTool -D mapreduce.job.queuename=${QUEUE_NAME} -url ${TD_CONNECTION_URL}//${TD_HOSTANME}/DATABASE=${TD_DB_ERROR} -classname ${TD_JDBC_CLASS_NAME} -username ${TD_USERNAME} -password xxxxxx -jobtype hdfs -fileformat textfile -splitbycolumn ${SPLIT_BY_COLUMN} -method ${SPLIT_METHOD} -targetpaths ${HDFS_TARGET_LOCATION} -nummappers ${NO_OF_MAPPERS} -separator "${FIELD_DELIMETER}" -sourcequery "${SOURCE_QUERY}" "

    hadoop jar ${TDCH_JAR_PATH} \
        com.teradata.connector.common.tool.ConnectorImportTool \
        -D mapreduce.job.queuename=${QUEUE_NAME} \
        -url ${TD_CONNECTION_URL}//${TD_HOSTANME}/DATABASE=${TD_DB_ERROR} \
        -classname ${TD_JDBC_CLASS_NAME} \
        -username ${TD_USERNAME} \
        -password ${TD_PASSWORD} \
        -jobtype hdfs \
        -fileformat textfile \
        -separator "${FIELD_DELIMETER}" \
        -splitbycolumn ${SPLIT_BY_COLUMN} \
        -method ${SPLIT_METHOD} \
        -targetpaths ${HDFS_TARGET_LOCATION} \
        -nummappers ${NO_OF_MAPPERS} \
        -sourcequery "${SOURCE_QUERY}"

    exit_code=`fn_get_exit_code $?`
    return ${exit_code}
}


##################################
# This function is used for Import from Teradata using TDCH for delta load
# PARAMETERS - teradata sourcetable name, HDFS targetpath, num_mappers, filter condition
##############################

function fn_tdch_import_incremental()
{
    SRC_TABLE_NAME="$1"
    HDFS_TARGET_LOCATION="$2"
    NO_OF_MAPPERS="$3"
    FILTER_CONDITION=${4}

    echo " hadoop jar ${TDCH_JAR_PATH} com.teradata.connector.common.tool.ConnectorImportTool -D mapreduce.job.queuename=${QUEUE_NAME} -url ${TD_CONNECTION_URL}//${TD_HOSTANME}/DATABASE=${TD_DB_DIV} -classname ${TD_JDBC_CLASS_NAME} -username ${TD_USERNAME} -password xxxxxx -jobtype hdfs -fileformat textfile -separator "${FIELD_DELIMETER}" -sourcetable ${TD_DB_DIV}.${SRC_TABLE_NAME} -targetpaths ${HDFS_TARGET_LOCATION} -nummappers ${NO_OF_MAPPERS} -sourceconditions "$FILTER_CONDITION" "

    hadoop jar ${TDCH_JAR_PATH} \
        com.teradata.connector.common.tool.ConnectorImportTool \
        -D mapreduce.job.queuename=${QUEUE_NAME} \
        -url ${TD_CONNECTION_URL}//${TD_HOSTANME}/DATABASE=${TD_DB_DIV} \
        -classname ${TD_JDBC_CLASS_NAME} \
        -username ${TD_USERNAME} \
        -password ${TD_PASSWORD} \
        -jobtype hdfs \
        -fileformat textfile \
        -separator "${FIELD_DELIMETER}" \
        -sourcetable ${TD_DB_DIV}.${SRC_TABLE_NAME} \
        -targetpaths ${HDFS_TARGET_LOCATION} \
        -nummappers ${NO_OF_MAPPERS} \
        -sourceconditions "$FILTER_CONDITION"

    exit_code=`fn_get_exit_code $?`
    return ${exit_code}
}

function fn_delete_old_partition(){
 usage="Usage: fn_delete_old_partition [hdfs_dir_path] [latest_partition_value]"

  HDFS_DIR_PATH="$1"
  fn_assert_variable_is_set "HDFS_DIR_PATH" "${HDFS_DIR_PATH}"
  echo "HDFS_DIR_PATH : ${HDFS_DIR_PATH}"

  LATEST_PARTITION_VALUE="$2"
  fn_assert_variable_is_set "LATEST_PARTITION_VALUE" "${LATEST_PARTITION_VALUE}"
  echo "LATEST_PARTITION_VALUE : ${LATEST_PARTITION_VALUE}"

  LATEST_PATH="${HDFS_DIR_PATH}/${LATEST_PARTITION_VALUE}"
  echo "Latest Path : ${LATEST_PATH}"

  # Loop through files
  hadoop fs -ls ${HDFS_DIR_PATH} | grep ${HDFS_DIR_PATH} | while read f; do
    # Get File Date and File Name
    FILE_NAME=`echo $f | awk '{print $8}'`;

    if [ "${FILE_NAME}" != "${LATEST_PATH}" ]; then
      # Insert delete logic here
      echo "The file to delete : ${FILE_NAME}"
      fn_delete_hdfs_directory "${FILE_NAME}"

    fi
  done
}
################################################################################
#                                     End                                      #
################################################################################
