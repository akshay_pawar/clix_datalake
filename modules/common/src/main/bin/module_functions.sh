#!/usr/bin/env bash

function fn_get_common_hadoop_jar_version() {
    for jar_version in `ls $COMMON_HOME/../libs/ | grep 'Common' | grep '.jar' | cut -d '-' -f2 | sort -u -r | head -1`;
    do
        VERSION=`echo $jar_version` ;
    done
}

function fn_validate_status_and_abort() {

local STATUS="$1"
local SUCCESS_MSG="$2"
local FAILURE_MSG="$3"

if [ "$STATUS" -eq 0 ]; then
    fn_log_info "${SUCCESS_MSG}"
else
    fn_log_error "${FAILURE_MSG}"
    exit 1
fi

}

function fn_check_hdfs_dir_exists() {

local HDFS_DIR_PATH="$1"

fn_assert_variable_is_set "HDFS_DIR_PATH" "${HDFS_DIR_PATH}"

fn_log_info "CHECKING IF HDFS DIRECTORY ${HDFS_DIR_PATH} EXISTS"
hadoop fs -test -d ${HDFS_DIR_PATH}
if [ $? -eq 0 ]; then
    fn_log_info "HDFS DIRECTORY ${HDFS_DIR_PATH} EXISTS."
    return 0
else
    fn_log_error "HDFS DIRECTORY ${HDFS_DIR_PATH} DOES NOT EXIST."
    return 1
fi

}

function fn_check_batch_id() {

local BATCH_ID="$1"

echo ${BATCH_ID} | grep "^[0-9]\{13\}$" >> /dev/null
fn_validate_status_and_abort "$?" "BATCH ID ${BATCH_ID} IS NUMERIC" "BATCH ID ${BATCH_ID} IS NOT NUMERIC/NOT IN PROPER FORMAT. ABORTING THE JOB!!"

}

function fn_validate_batch_ids() {

local START_BATCH_ID="$1"
local END_BATCH_ID="$2"

if [ "${START_BATCH_ID}" -gt "${END_BATCH_ID}" ]; then
    fn_log_error "START_BATCH_ID IS GREATER THAN END_BATCH_ID. PLEASE CHECK!!"
    exit 100
elif [ "${START_BATCH_ID}" -eq "${END_BATCH_ID}" ]; then
    fn_log_error "START_BATCH_ID IS EQUALS TO END_BATCH_ID. NO NEW BATCHES FOUND AFTER LAST SUCCESSFUL EXECUTION. ABORTING THE JOB"
    exit 100
else
    fn_log_info "START AND END BATCH IDS VALIDATION COMPLETED SUCCESSFULLY."
fi

}

function fn_get_table_nth_batch_id() {

local HIVE_DATABASE_NAME="$1"
local HIVE_TABLE_NAME="$2"
local BATCH_ID="$3"
local NTH_NUMBER="$4"
local __VARIABLE_NAME="$5"

fn_assert_variable_is_set "HIVE_DATABASE_NAME" "${HIVE_DATABASE_NAME}"
fn_assert_variable_is_set "HIVE_TABLE_NAME" "${HIVE_TABLE_NAME}"
fn_assert_variable_is_set "BATCH_ID" "${BATCH_ID}"
fn_assert_variable_is_set "NTH_NUMBER" "${NTH_NUMBER}"
fn_assert_variable_is_set "__VARIABLE_NAME" "${__VARIABLE_NAME}"

local TABLE_HDFS_PATH=`executeHiveQuery -e "use $HIVE_DATABASE_NAME; describe extended $HIVE_TABLE_NAME" | perl -n -e'while(/(\w*location:hdfs:\/\/\w*\/[0-9a-zA-Z\/_=\.]*)/g) {print "$1\n"}' | tr -s ' ' | cut -d ':' -f2,3`

fn_check_hdfs_dir_exists "${TABLE_HDFS_PATH}"
fn_validate_status_and_abort "$?" "HDFS DIRECTORY CHECK SUCCESSFUL" "HDFS DIRECTORY CHECK FAILED. ABORTING THE JOB!!"

NTH_BATCH_ID=`hadoop fs -ls ${TABLE_HDFS_PATH} | grep -B ${NTH_NUMBER} "${BATCH_ID}" | grep "batch_id=[0-9]\{13\}" | sed 's/^.*batch_id=\([0-9]\{13\}\)$/\1/g' | sort | tail -1`
if [ -z ${NTH_BATCH_ID} ]; then
    fn_log_info "COULD NOT FIND NTH BATCH_ID WITH NTH VALUE ${NTH_NUMBER}. SO, FETCHING THE LATEST BATCH_ID OF THE TABLE"
    NTH_BATCH_ID=`hadoop fs -ls ${TABLE_HDFS_PATH} | grep "batch_id=[0-9]\{13\}" | sed 's/^.*batch_id=\([0-9]\{13\}\)$/\1/g' | sort | tail -1`
fi

fn_check_batch_id "${NTH_BATCH_ID}"

eval $__VARIABLE_NAME="'$NTH_BATCH_ID'"

}

function fn_get_table_latest_batch_id() {

local HIVE_DATABASE_NAME="$1"
local HIVE_TABLE_NAME="$2"
local __VARIABLE_NAME="$3"

fn_assert_variable_is_set "HIVE_DATABASE_NAME" "${HIVE_DATABASE_NAME}"
fn_assert_variable_is_set "HIVE_TABLE_NAME" "${HIVE_TABLE_NAME}"

local TABLE_HDFS_PATH=`executeHiveQuery -e "use $HIVE_DATABASE_NAME; describe extended $HIVE_TABLE_NAME" | perl -n -e'while(/(\w*location:hdfs:\/\/\w*\/[0-9a-zA-Z\/_=\.]*)/g) {print "$1\n"}' | tr -s ' ' | cut -d ':' -f2,3`

fn_check_hdfs_dir_exists "${TABLE_HDFS_PATH}"
fn_validate_status_and_abort "$?" "HDFS DIRECTORY CHECK SUCCESSFUL" "HDFS DIRECTORY CHECK FAILED. ABORTING THE JOB!!"

LATEST_BATCH_ID=`hadoop fs -ls ${TABLE_HDFS_PATH} | grep "batch_id=[0-9]\{13\}" | sed 's/^.*batch_id=\([0-9]\{13\}\)$/\1/g' | sort | tail -1`
fn_check_batch_id "${LATEST_BATCH_ID}"

eval $__VARIABLE_NAME="'$LATEST_BATCH_ID'"

}

function fn_set_to_default_if_empty() {

local TABLE_NAME="$1"
local VARIABLE_VALUE="$2"
local __VARIABLE_NAME="$3"

if [ -z "${VARIABLE_VALUE}" ]; then
    fn_log_info "LAST SUCCESSFUL BATCH_ID FOR ${TABLE_NAME} IS EMPTY."
    fn_log_info "SETTING DEFAULT VALUES TO ${TABLE_NAME}_START_BATCH_ID"
    VARIABLE_VALUE="0000000000000"
fi

eval $__VARIABLE_NAME="'$VARIABLE_VALUE'"

}

function fn_check_number() {
  local NUMBER_TO_TEST=$1
  re='^[0-9]+$'
  if ! [[ $NUMBER_TO_TEST =~ $re ]] ; then
  return 0
  else
  return 1
  fi
}