#!/bin/bash
################################################################################
#                               General Details                                #
################################################################################
#                                                                              #
# Name                                                                         #
#     : Create flow script                                                     #
# File                                                                         #
#     : create_flow.sh                                                         #
#                                                                              #
# Description                                                                  #
#     : this script is created to create flow                                  #
#                                                                              #
# Author                                                                       #
#     : Pradeep Chandra                                                        #
#                                                                              #
################################################################################
#                           Script Environment Setup                           #
################################################################################

SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# Extract SCRIPT_HOME
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`

# Set module, project, subject area home paths.
MODULE_HOME=`dirname ${SCRIPT_HOME}`
PROJECT_HOME=`dirname ${MODULE_HOME}`
SUBJECT_AREA_HOME=`dirname ${PROJECT_HOME}`
HOME=`dirname ${SUBJECT_AREA_HOME}`
COMMON_HOME=${HOME}/Common/common

# Load namespace property file
. ${COMMON_HOME}/shell_functions/bin/namespace_functions.sh

# Load all environment properties files. Order Should not be changed.
user_namespace_param_file=$(getNamespaceFileForCurrentUser)
. ${HOME}/etc/${user_namespace_param_file}
. ${HOME}/etc/default.env.properties
. ${SUBJECT_AREA_HOME}/etc/subject-area.env.properties
. ${PROJECT_HOME}/etc/project.env.properties
. ${MODULE_HOME}/etc/module.env.properties

# Load all utility functions. Order should not be changed.
. ${COMMON_HOME}/shell_functions/bin/utility_functions.sh
. ${COMMON_HOME}/shell_functions/bin/environment_functions.sh
. ${COMMON_HOME}/shell_functions/bin/audit_functions.sh
. ${COMMON_HOME}/coordinator_functions/bin/functions.sh

###############################################################################
#                               Module Start                                  #
###############################################################################

USER_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $USER_PATH`

readParameters "$@"

info "FLOW_ID=${FLOW_ID}"
info "FLOW_NAME=${FLOW_NAME}"
info "FLOW_DESC=${FLOW_DESC}"

#create flow
command_response=$(fn_create_flow ${FLOW_ID} "${FLOW_NAME}" "${FLOW_DESC}")
fn_check_and_exit_error_response "${command_response}" "Flow ${FLOW_ID} created successfully" "Failed to create flow ${FLOW_ID}"

###############################################################################
#                               Module End                                    #
###############################################################################
