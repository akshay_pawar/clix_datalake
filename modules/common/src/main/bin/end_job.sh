#!/bin/bash
################################################################################
#                               General Details                                #
################################################################################
#                                                                              #
# Name                                                                         #
#     : end job script                                                         #   
# File                                                                         #
#     : end_job.sh                                                             #
#                                                                              #
# Description                                                                  #
#     : this script is created for making the running batch and flowinstance   #
#       as successful and make entries in mysql                                #
#       this script expects 1 parameters as                                    #
#       first argument being FLOW_ID                                           #
#                                                                              #
# Author                                                                       #
#     :                                                                        #
#                                                                              #
################################################################################
#                           Script Environment Setup                           #
################################################################################

SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# Extract SCRIPT_HOME
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`

# Set module, project, subject area home paths.
MODULE_HOME=`dirname ${SCRIPT_HOME}`
PROJECT_HOME=`dirname ${MODULE_HOME}`

# Load namespace property file
. ${MODULE_HOME}/bin/namespace_functions.sh

# Load all environment properties files. Order Should not be changed.
user_namespace_param_file=$(getNamespaceFileForCurrentUser)

. ${PROJECT_HOME}/etc/${user_namespace_param_file}
. ${PROJECT_HOME}/etc/default.env.properties

# Load all utility functions. Order should not be changed.
. ${MODULE_HOME}/bin/utility_functions.sh
. ${MODULE_HOME}/bin/functions.sh

###############################################################################
#                               Module Start                                  #
###############################################################################

FLOW_ID="$1"

assert_variable_is_set "FLOW_ID" "${FLOW_ID}"

#get running batch id
RUNNING_BATCH_ID=$(fn_get_running_batch_id $FLOW_ID)
fn_check_and_exit_error_response "${RUNNING_BATCH_ID}" "Successfully parsed RUNNING_BATCH_ID" "Unable to get RUNNING_BATCH_ID"

#success running batch
command_response=$(fn_successful_batch $RUNNING_BATCH_ID $FLOW_ID)
fn_check_and_exit_error_response "${command_response}" "Running batch is made successful" "Unable to make running batch successful."

#success flow instance
command_response=$(fn_successful_running_flow_instance $FLOW_ID)
fn_check_and_exit_error_response "${command_response}" "Running flow instance is made successful" "Unable to make running flow instance successful."
