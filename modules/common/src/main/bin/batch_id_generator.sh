#!/bin/bash
################################################################################
#                               General Details                                #
################################################################################
#                                                                              #
# Name                                                                         #
#     : Batch id generator script                                              #   
# File                                                                         #
#     : batch_id_generator.sh                                                  #
#                                                                              #
# Description                                                                  #
#     : this script is created for creating a batchId and a new flowinstance   #
#       and make entries in mysql                                              #
#       this script expects 1 parameters as                                    #
#       first argument being FLOW_ID                                           #
#                                                                              #
# Author                                                                       #
#     :                                                                        #
#                                                                              #
################################################################################
#                           Script Environment Setup                           #
################################################################################

SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# Extract SCRIPT_HOME
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`

# Set module, project, subject area home paths.
MODULE_HOME=`dirname ${SCRIPT_HOME}`
PROJECT_HOME=`dirname ${MODULE_HOME}`

# Load namespace property file
. ${MODULE_HOME}/bin/namespace_functions.sh

# Load all environment properties files. Order Should not be changed.
user_namespace_param_file=$(getNamespaceFileForCurrentUser)

. ${PROJECT_HOME}/etc/${user_namespace_param_file}
. ${PROJECT_HOME}/etc/default.env.properties

# Load all utility functions. Order should not be changed.
. ${MODULE_HOME}/bin/utility_functions.sh
. ${MODULE_HOME}/bin/functions.sh

##############################################################################
#                               Module Start                                  #
###############################################################################

FLOW_ID="$1"
INSTANCE_ALREADY_RUNNING=-11

assert_variable_is_set "FLOW_ID" "${FLOW_ID}"

#check for instance running
FLOW_INSTANCE_ID=$(fn_check_for_instance_running $FLOW_ID);
fn_check_and_exit_error_response "${FLOW_INSTANCE_ID}" "Running Instance checked successfully" "Failed to check running instance"

if [[ -z "$FLOW_INSTANCE_ID" ]]
then
	#run flow
	FLOW_INSTANCE_ID=$(fn_run_flow_instance $FLOW_ID);
	fn_check_and_exit_error_response "${FLOW_INSTANCE_ID}" "New flow instance is started" "Unable to start new flow instance"

    #get batch id
	BATCH_ID=$(fn_generate_batch_id);
	fn_check_and_exit_error_response "${BATCH_ID}" "New batch id ${BATCH_ID} is created" "Unable to create new batch id"

    #run batch
	command_response=$(fn_run_new_batch $BATCH_ID $FLOW_ID $FLOW_INSTANCE_ID)
	fn_check_and_exit_error_response "${command_response}" "New batch run is started" "Unable to run new batch"
else

	echo "Flow " $FLOW_ID " with flow instance id : " $FLOW_INSTANCE_ID "is already running."
	fn_exit_with_failure_message "${INSTANCE_ALREADY_RUNNING}" "Flow " $FLOW_ID " with flow instance id : " $FLOW_INSTANCE_ID "is already running."
fi
