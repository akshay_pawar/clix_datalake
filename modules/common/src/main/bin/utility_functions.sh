#!/bin/bash

#Function which gives hour from timestamp
function getHourFromTimeStamp () {
    local getTimeStamp=$1;
    local getDate=`echo "${getTimeStamp}"|cut -d ' ' -f 1`
    local getHour=`echo "${getTimeStamp}"|cut -d ' ' -f 2 | cut -d ':' -f 1`
    local getMinute=`echo "${getTimeStamp}"|cut -d ' ' -f 2 | cut -d ':' -f 2`
    local exportMinute="`getMinuteFromTimeStamp "$getMinute"`"
    local exportHour="$getDate $getHour:$exportMinute:00.00"
    echo $exportHour;
}

#Function which gives minute from timestamp
function getMinuteFromTimeStamp () {
    local getMinute=$1;
    if [ "$getMinute" = "" ];
    then
            local setMinute="00"
            echo $setMinute;
    elif [ "$getMinute" -lt "30" ]
    then
            local setMinute="00"
            echo $setMinute;
    elif [ "$getMinute" -ge "30" ]
    then
            local setMinute="30"
            echo $setMinute;
    else
            local setMinute="00"
            echo $setMinute;
    fi
}

# Convert the Input string to lower case
function convertToLowerCase() {
    OUTPUT_STRING=`echo $1 | tr '[:upper:]' '[:lower:]'`
    echo $OUTPUT_STRING
}

# Convert the Input string to UPPER case
function convertToUpperCase() {
    OUTPUT_STRING=`echo $1 | tr '[:lower:]' '[:upper:]'`
    echo $OUTPUT_STRING
}

function echoCommand() {
    echo "Executing Command : $1"
}

function execute() {
    evalString1="$1"
    echo -e "Executing Command : $evalString1" | sed 's/ \+ /\t/g' | tr "\t" "\n" |sed "/^$/d"| sed ':a;N;$!ba;s/\n/\n\t\t         /g'
    eval $1
}
#For information
function info() {
    echo -e "[INFO] $1"
}

#For Error
function error() {
    echo -e "[ERROR] $1"
}

#For Error
function warn() {
    echo -e "[WARN] $1"
}

#Current date time
function getFormattedCurrentDate() {
    current_timestamp="`date +"%Y-%m-%d %H:%M:%S"`";
    echo ${current_timestamp}
}

function getFormattedDate() {
    formattedDate=$(date -d $1 +"%Y%m%d")
    echo ${formattedDate}
}

#Read Parameters From Azkaban
function readParameters() {
    info "Parsing args ..."
    for item in "$@"; do
        case $item in
        (*=*) eval $item;;
        esac
    done
    info "Done parsing args ..!!!"
}

#Read parameters from command line and export
function exportParameters() {
    echo "Parsing args ..."
    for item in "$@"; do
        export "${item}"
    done
    echo "Done parsing args ..!!!"
}

#Checks the exit status
function validateExitStatus() {
    if [ $1 -eq 0 ];
    then
        info "$2";
    else
        error "$3";
        exit 100;
    fi
}

#function to validate dates
function validateDates () {
    local START_DATE=`getFormattedDate $1` ;
    local END_DATE=`getFormattedDate $2`;
    echo "Start Date is $START_DATE and End Date is $END_DATE "
    if [ $START_DATE -gt $END_DATE ];
    then
        echo "Start date must be lesser than end date.";
        exit 100;
    fi
}

# Check directory exists
function checkDirExist() {
    DIR_TO_CHECK=$1
    if [ -d "$DIR_TO_CHECK" ]; then
        return 0;
    else
        return 1;
    fi
}

# Check if file exist
function checkFileExist() {
    FILE_PATH=$1
    if [ -e $FILE_PATH ]
    then
        return 0
    else
        return 1
    fi
}


# Print the usage of the sqoop utility
function printSqoopUsage() {
    echo -e "[USAGE] $ sqoop.sh -d ORACLE_SALES_USR1 -t PRODUCT_SALES -c /appl/sales/product_phase1/ -l FULL -a 1
                                -d Datasource Connection conf file
                                -t Table Name to Import
                                -c Configuration directory
                                -l Load type [Full/Delta]
                                -n Job Name [OPTIONAL]
                                -a Audit Flag, set to 1 to enable auditing [OPTIONAL]
                                -m Overriding Mapper/Parallel count [OPTIONAL]
                                -w Where condition [OPTIONAL]
                                -f Override Fetch Size [OPTIONAL]
                                -q Override Job submission Queue Name [OPTIONAL]"
}

# Exit function to be called if variable is not set by printing the variable name
function variablesNotSetExit() {
    NOT_SET_VARIABLE_NAME=$1
    error "$NOT_SET_VARIABLE_NAME is not set. Validate the options."
    exit 1;
}

# Check if the given variable is set or not
function checkVariableIfSet() {
    VARIABLE_NAME=$1
    VARIABLE_VALUE=$2
    if [ ! -z "$VARIABLE_VALUE" ]; then
        info "$VARIABLE_NAME set to $VARIABLE_VALUE"
    else
        variablesNotSetExit $VARIABLE_NAME
fi
}

function checkVariableIfSetCondition() {
    VARIABLE_VALUE=$1
    if [ ! -z "$VARIABLE_VALUE" ]; then
        return 0
    else
        return 1
    fi
}

# Check exit code of the command
function checkAndLogExistStatus() {
    MESSAGE=$1
    RETURN_CODE=$2
    if [ $RETURN_CODE -eq 0 ]
    then
        info "$MESSAGE successful"
    else
        error "$MESSAGE failed"
        exit $RETURN_CODE
    fi
}


# Load configuration from file
function loadConfigsFromFile() {
    CONFIG_FILE_PATH=$1
    if checkFileExist $CONFIG_FILE_PATH;then
    . $CONFIG_FILE_PATH
        info "Successfully loaded $CONFIG_FILE_PATH"
    else
        warning "$CONFIG_FILE_PATH does not exist. Switching to use defaults."
    fi
}

function replaceDotWithUnderscore() {
    INPUT_STRING=$1
    OUTPUT_STRING=`echo $1 | sed 's/\./_/g'`
    echo $OUTPUT_STRING
}

function getYearFromDate () {
    YEAR=`echo $1|cut -d'-' -f 1`
    echo $YEAR
}

function getMonthFromDate () {
    MONTH=`echo $1|cut -d'-' -f 2`
    echo $MONTH
}

function getDayFromDate() {
    DAY=`echo $1|cut -d'-' -f 3`
    echo $DAY
}

function getYearMonthDayFromDate () {
    #PASSED_DATE=$1;
    export $2=`getYearFromDate $1`
    export $3=`getMonthFromDate $1`
    export $4=`getDayFromDate $1`

}

function createLogDirIfNotExists(){

    checkVariableIfSet "LOG_DIR_MODULE" "${LOG_DIR_MODULE}"

    if [ ! -d "${LOG_DIR_MODULE}" ]; then

        mkdir -p ${LOG_DIR_MODULE}
        exit_code=$?
        success_message="Created log directory ${LOG_DIR_MODULE}"
        failure_message="Failed to create log directory ${LOG_DIR_MODULE}"

        validateExitStatus "${exit_code}" "${success_message}" "${failure_message}"
    fi

}

function executePig(){

  createLogDirIfNotExists

  ### Fetching pig script name from the arguments
  local index
  for (( i=1; i<=$#; i++ )); do
    if [ "${!i}" == "-f" ]; then
      index="${i}"
    fi
  done
  index=$((index + 1))
  eval PIG_FILE_NAME=\${$index}
  LOG_FILE_NAME_MODULE=`basename ${PIG_FILE_NAME} | cut -d'.' -f1`
  ### End of fetching pig script name from arguments

  checkVariableIfSet "LOG_FILE_NAME_MODULE" "${LOG_FILE_NAME_MODULE}"

  current_timestamp=`date +'%Y%m%d%H%M%S'`
  module_type="pig"
  module_log_file="${LOG_DIR_MODULE}/${module_type}_${LOG_FILE_NAME_MODULE}_${current_timestamp}.log"
  module_out_file="${LOG_DIR_MODULE}/${module_type}_${LOG_FILE_NAME_MODULE}_${current_timestamp}.out"

  info "Log file : ${module_log_file}"
  info "Out file : ${module_out_file}"

  user_namespace_param_file=$(getNamespaceFileForCurrentUser)
  user_namespace_param_file_option="${HOME}/etc/${user_namespace_param_file}"

  #Assigning default mapreduce job queuename if queue name is empty
  if [ -z "${MAPREDUCE_JOB_QUEUENAME}" ]; then
    MAPREDUCE_JOB_QUEUENAME="dataplatform"
  fi

  COMMAND=`echo "pig -Dudf.import.list="${PIG_UDF_IMPORT_LIST}"  \
      -Dunique.property.prefix="${UNIQUE_PROPERTY_PREFIX}" \
      -Dmapreduce.job.queuename="${MAPREDUCE_JOB_QUEUENAME}" \
      -D${UNIQUE_PROPERTY_PREFIX}.subject.area.name="${SUBJECT_AREA_NAME}" \
      -D${UNIQUE_PROPERTY_PREFIX}.project.name="${PROJECT_NAME}" \
      -D${UNIQUE_PROPERTY_PREFIX}.module.name="${MODULE_NAME}" \
      -Djob.name="${PROJECT_NAME}_${MODULE_NAME}" \
      -useHCatalog \
      -logfile "${module_log_file}" \
      -m "${user_namespace_param_file_option}" \
      -m "${HOME}/etc/default.env.properties" \
      -m "${HOME}/etc/default.pig.properties" \
      -m "${SUBJECT_AREA_HOME}/etc/subject-area.pig.properties" \
      -m "${PROJECT_HOME}/etc/project.pig.properties" \
      -m "${MODULE_HOME}/etc/module.pig.properties" \
      "$@" 2>&1 | tee "${module_out_file}"" | sed 's/  */ /g'`

  echoCommand "${COMMAND}"

  pig -Dudf.import.list="${PIG_UDF_IMPORT_LIST}"  \
      -Dunique.property.prefix="${UNIQUE_PROPERTY_PREFIX}" \
      -Dmapreduce.job.queuename="${MAPREDUCE_JOB_QUEUENAME}" \
      -D${UNIQUE_PROPERTY_PREFIX}.subject.area.name="${SUBJECT_AREA_NAME}" \
      -D${UNIQUE_PROPERTY_PREFIX}.project.name="${PROJECT_NAME}" \
      -D${UNIQUE_PROPERTY_PREFIX}.module.name="${MODULE_NAME}" \
      -Djob.name="${MODULE_NAME}" \
      -useHCatalog \
      -logfile "${module_log_file}" \
      -m "${user_namespace_param_file_option}" \
      -m "${HOME}/etc/default.env.properties" \
      -m "${HOME}/etc/default.pig.properties" \
      -m "${SUBJECT_AREA_HOME}/etc/subject-area.pig.properties" \
      -m "${PROJECT_HOME}/etc/project.pig.properties" \
      -m "${MODULE_HOME}/etc/module.pig.properties" \
      "$@" 2>&1 | tee "${module_out_file}"

  exit_code=${PIPESTATUS[0]}
  return ${exit_code}

}

function generateHiveInitializationFile(){

  rm -f ${HIVE_INITIALISATION_FILE}
  validateExitStatus "$?" "${HIVE_INITIALISATION_FILE} removed successfully" "Failed to remove file ${HIVE_INITIALISATION_FILE}"

  echo "" >> "${HIVE_INITIALISATION_FILE}"
  cat "${HOME}/etc/default.hive.properties" >> "${HIVE_INITIALISATION_FILE}"
  echo "" >> "${HIVE_INITIALISATION_FILE}"
  cat "${SUBJECT_AREA_HOME}/etc/subject-area.hive.properties" >> "${HIVE_INITIALISATION_FILE}"
  echo "" >> "${HIVE_INITIALISATION_FILE}"
  cat "${PROJECT_HOME}/etc/project.hive.properties" >> "${HIVE_INITIALISATION_FILE}"
  echo "" >> "${HIVE_INITIALISATION_FILE}"
  cat "${MODULE_HOME}/etc/module.hive.properties" >> "${HIVE_INITIALISATION_FILE}"
  echo "" >> "${HIVE_INITIALISATION_FILE}"

}

function executeHive(){

  createLogDirIfNotExists

  ### Fetching hive script name from the arguments
  local index
  for (( i=1; i<=$#; i++ )); do
    if [ "${!i}" == "-f" ]; then
      index="${i}"
    fi
  done
  index=$((index + 1))
  eval HIVE_FILE_NAME=\${$index}
  LOG_FILE_NAME_MODULE=`basename ${HIVE_FILE_NAME} | cut -d '.' -f1`
  ### End of fetching hive script name from arguments

  checkVariableIfSet "LOG_FILE_NAME_MODULE" "${LOG_FILE_NAME_MODULE}"

  local current_timestamp=`date +'%Y%m%d%H%M%S'`
  module_type="hive"
  module_log_file="${LOG_DIR_MODULE}/${module_type}_${LOG_FILE_NAME_MODULE}_${current_timestamp}.log"
  module_out_file="${LOG_DIR_MODULE}/${module_type}_${LOG_FILE_NAME_MODULE}_${current_timestamp}.out"
  HIVE_INITIALISATION_FILE="${LOG_DIR_MODULE}/${module_type}_${LOG_FILE_NAME_MODULE}.rc"

  generateHiveInitializationFile

  info "Log file : ${module_log_file}"
  info "Out file : ${module_out_file}"

  #Assigning default mapreduce job queuename if queue name is empty
  if [ -z "${MAPREDUCE_JOB_QUEUENAME}" ]; then
    MAPREDUCE_JOB_QUEUENAME="dataplatform"
  fi

  COMMAND=`echo "hive \
      --hiveconf mapreduce.job.queuename="${MAPREDUCE_JOB_QUEUENAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.subject.area.name="${SUBJECT_AREA_NAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.project.name="${PROJECT_NAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.module.name="${MODULE_NAME}" \
      --hiveconf hive.querylog.location="${module_log_file}" \
      -i "${HIVE_INITIALISATION_FILE}" \
      "$@" 2>&1 | tee "${module_out_file}"" | sed 's/  */ /g'`

  echoCommand "${COMMAND}"

  hive \
      --hiveconf mapreduce.job.queuename="${MAPREDUCE_JOB_QUEUENAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.subject.area.name="${SUBJECT_AREA_NAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.project.name="${PROJECT_NAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.module.name="${MODULE_NAME}" \
      --hiveconf hive.querylog.location="${module_log_file}" \
      -i "${HIVE_INITIALISATION_FILE}" \
      "$@" 2>&1 | tee "${module_out_file}"

  exit_code=${PIPESTATUS[0]}
  return ${exit_code}

}


#Funtion to send the email alerts
function SendEmailAlert () {
        local email_content=$1;
        local email_subject=$2;
        local recipient_email_id=$CBA_ALERT_EMAIL_ID; #this is set in param_files
        echo "$email_content"|mail -s "${email_subject}" $recipient_email_id;
}

#Function to send the email alerts for Carrier
function emailAlertCarrier () {
        local email_content=$1;
        local email_subject=$2;
        local recipient_email_id=$3;
        echo "$email_content"| mail -s "${email_subject}" $recipient_email_id;
}

#Function to send the email alerts generic
function emailAlert () {
    project_name=$1
    job_name=$2
    job_status=$3
    subject=$4
    error_msg=$5
    mail_list=$6
        echo -e "
                <p>PROJECT_NAME\t : $project_name</p>
                <p>JOB_NAME\t\t : $job_name</p>
                <html>
                        <body>
                        <font color='red'>Job Status : $job_status
                        </font>
                        <p> Error Message : </p>
                        <p> $error_msg </p>
                        </body>
                </html> " | mailx -s "$(printf "%s\n" "$subject" "Content-Type: text/html")" $mail_list
}

function CheckClickstreamrecordCount () {
	local start_date=$1;
	local end_date=$2;
	local loop_end_date=$3;
	local job_name=$4;
	local count="";

	if [ "$start_date" == "$end_date" ]
	then
		local proc_year=`echo $start_date|cut -d'-' -f 1`
		local proc_month=`echo $start_date|cut -d'-' -f 2`
		local proc_day=`echo $start_date|cut -d'-' -f 3`
		local count=`readKeyValue --jobid "2" --key "COUNT_${proc_year}-${proc_month}-${proc_day}"`;
		if [ "" == "$count" ];
		then
			SendEmailAlert " Feed count is 0 for the date ${proc_year}-${proc_month}-${proc_day}. Job is being terminated. " "$job_name"
			echo "Feed count is 0";
			exit 100;
		fi
		echo "count is $count"
	else
		local flag_count=""
		while [ "$start_date" != "$loop_end_date" ]
		do
			local proc_year=`echo $start_date|cut -d'-' -f 1`
			local proc_month=`echo $start_date|cut -d'-' -f 2`
			local proc_day=`echo $start_date|cut -d'-' -f 3`
			local count=`readKeyValue --jobid "2" --key "COUNT_${proc_year}-${proc_month}-${proc_day}"`
			if [ "" != "$count" ]
			then
				local flag_count=$count;
			fi

			local start_date=$(/bin/date --date "$start_date 1 day" +%Y-%m-%d)
		done
		if [ "" == "$flag_count" ]
		then
			echo "flag_count is 0"
			SendEmailAlert " Feed count is 0 for all the dates between $1 - $2 , job is being terminated. " "$job_name";
			exit 100;
		fi
	fi
	return 0;
}


#Function to get record count for specific date
function getRecordCountForDate () {
	local start_date=$1;
	local hive_db=$2;
	local hive_table=$3;
	local count="";
	local proc_year=`echo $start_date|cut -d'-' -f 1`
	local proc_month=`echo $start_date|cut -d'-' -f 2`
	local proc_day=`echo $start_date|cut -d'-' -f 3`
        local db_table="$2.$3"
	local count=`hive -hiveconf mapreduce.job.queuename=$MAPREDUCE_LONG_RUNNING_QUEUE -S -e "select count(1) from $db_table where year=$proc_year and month=$proc_month and day=$proc_day"`
	echo $count;
}


function GetDirSize () {
	local dir_path=$1;
	#echo $dir_path;
	local dir_size=`hadoop fs -count $dir_path|awk '{print $3}'`;
	echo $dir_size;
	#return $dir_size;
}

function executeBeeline(){

  createLogDirIfNotExists

  ### Fetching beeline script name from the arguments
  local index
  for (( i=1; i<=$#; i++ )); do
    if [ "${!i}" == "-f" ]; then
      index="${i}"
    fi
  done
  index=$((index + 1))
  eval BEELINE_FILE_NAME=\${$index}
  LOG_FILE_NAME_MODULE=`basename ${BEELINE_FILE_NAME} | cut -d '.' -f1`
  ### End of fetching beeline script name from arguments

  checkVariableIfSet "LOG_FILE_NAME_MODULE" "${LOG_FILE_NAME_MODULE}"

  local current_timestamp=`date +'%Y%m%d%H%M%S'`
  module_type="beeline"
  module_log_file="${LOG_DIR_MODULE}/${module_type}_${LOG_FILE_NAME_MODULE}_${current_timestamp}.log"
  module_out_file="${LOG_DIR_MODULE}/${module_type}_${LOG_FILE_NAME_MODULE}_${current_timestamp}.out"


  info "Log file : ${module_log_file}"
  info "Out file : ${module_out_file}"

  #Assigning default mapreduce job queuename if queue name is empty
  if [ -z "${MAPREDUCE_JOB_QUEUENAME}" ]; then
    MAPREDUCE_JOB_QUEUENAME="dataplatform"
  fi

  COMMAND=`echo "beeline -u \"${JDBC_CONNECTION_URL}\" --silent=${SILENT} --verbose=${VERBOSE} --showWarnings=${SHOW_WARNINGS} --showNestedErrs=${SHOW_NESTED_ERRS} \
      --hiveconf mapreduce.job.queuename="${MAPREDUCE_JOB_QUEUENAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.subject.area.name="${SUBJECT_AREA_NAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.project.name="${PROJECT_NAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.module.name="${MODULE_NAME}" \
      --hiveconf beeline.querylog.location="${module_log_file}" \
      "$@" 2>&1 | tee "${module_out_file}"" | sed 's/  */ /g'`

  echoCommand "${COMMAND}"

  beeline -u "${JDBC_CONNECTION_URL}" --silent=${SILENT} --verbose=${VERBOSE} --showWarnings=${SHOW_WARNINGS} --showNestedErrs=${SHOW_NESTED_ERRS} \
      --hiveconf mapreduce.job.queuename="${MAPREDUCE_JOB_QUEUENAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.subject.area.name="${SUBJECT_AREA_NAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.project.name="${PROJECT_NAME}" \
      --hiveconf ${UNIQUE_PROPERTY_PREFIX}.module.name="${MODULE_NAME}" \
      --hiveconf beeline.querylog.location="${module_log_file}" \
      "$@" 2>&1 | tee "${module_out_file}"

  exit_code=${PIPESTATUS[0]}
  return ${exit_code}


}

# Function to execute hive commands
function executeHiveQuery(){

  #Assigning default mapreduce job queuename if MAPREDUCE_JOB_QUEUENAME is empty
  if [ -z "${MAPREDUCE_JOB_QUEUENAME}" ]; then
    MAPREDUCE_JOB_QUEUENAME="dataplatform"
  fi

  hive --hiveconf mapreduce.job.queuename="${MAPREDUCE_JOB_QUEUENAME}" "$@"

}

# to get the next month by passing an argument as 201602 (YYYYMM)
function getNextMonth(){
entered_yearmonth=$1
length_entered_yearmonth=`expr length $entered_yearmonth`
if [ $length_entered_yearmonth -eq 6 ]
then
strt_dt="01"
full_dt="$entered_yearmonth$strt_dt"
date "+%Y%m" -d "$full_dt" 2>1 > /dev/null
is_valid=$?
if [ $is_valid -eq 0 ]
then
echo `date -d "$full_dt 1 month" +%Y%m`
else
echo "invalid date"
fi
else
echo "invalid date"
fi
}
# to get the previous month by passing an argument as 201602 (YYYYMM)
function getPreviousMonth(){
entered_yearmonth=$1
length_entered_yearmonth=`expr length $entered_yearmonth`
if [ $length_entered_yearmonth -eq 6 ]
then
strt_dt="01"
full_dt="$entered_yearmonth$strt_dt"
date "+%Y%m" -d "$full_dt" 2>1 > /dev/null
is_valid=$?
if [ $is_valid -eq 0 ]
then
echo `date -d "$full_dt -1 month" +%Y%m`
else
echo "invalid date"
fi
else
echo "invalid date"
fi
}
# to get the start date of given month by passing an argument as 201602 (YYYYMM)
function getMonthStartDate(){
entered_yearmonth=$1
length_entered_yearmonth=`expr length $entered_yearmonth`
if [ $length_entered_yearmonth -eq 6 ]
then
strt_dt="01"
full_dt="$entered_yearmonth$strt_dt"
date "+%Y%m" -d "$full_dt" 2>1 > /dev/null
is_valid=$?
if [ $is_valid -eq 0 ]
then
year=${entered_yearmonth:0:4}
month=${entered_yearmonth:4}
echo $year-$month-$strt_dt
else
echo "invalid date"
fi
else
echo "invalid date"
fi
}
# to get the end date of given month by passing an argument as 201602 (YYYYMM)
function getMonthEndDate(){
entered_yearmonth=$1
length_entered_yearmonth=`expr length $entered_yearmonth`
if [ $length_entered_yearmonth -eq 6 ]
then
strt_dt="01"
full_dt="$entered_yearmonth$strt_dt"
date "+%Y%m" -d "$full_dt" 2>1 > /dev/null
is_valid=$?
if [ $is_valid -eq 0 ]
then
year=${entered_yearmonth:0:4}
month=${entered_yearmonth:4}
d=$(cal $month $year | paste -s - | awk '{print $NF}')
echo $year-$month-$d
else
echo "invalid date"
fi
else
echo "invalid date"
fi
}

#To check if passed variable is numeric or not
function fn_check_numeric_da() {
	local var1=$1;
	if [[ ${var1} =~ ^[0-9]+$ ]]
	then
    	echo "BATCH_ID : ${var1} is an numeric"
	else

		echo "BATCH_ID : ${var1} is not  an numeric"
		exit 1
	fi
}

function fn_create_work_ddls() {

table_name=$1
if [ "$(ls -A /datalake/InfoLease/$table_name)" ];
then
latest_file=`ls -rt /datalake/InfoLease/$table_name|tail -1`
header=`cat /datalake/InfoLease/$table_name/$latest_file |head -1`
header=`echo $header |sed 's///g'`
hive -e "show create table db_gold.infolease_$table_name" > /tmp/${table_name}_temp.txt
IFS=',' read -r -a array <<< "$header"
echo "set hive.msck.path.validation=ignore;" >> /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl
echo "DROP TABLE IF EXISTS db_work.infolease_${table_name};CREATE EXTERNAL TABLE IF NOT EXISTS db_work.infolease_${table_name}(" >> /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl
table_name_upper=`echo $table_name|tr '[:lower:]' '[:upper:]'`
if [ "$table_name_upper" == "INDIA_NEA_REPORT"  ] || [ "$table_name_upper" == "DELINQUENCY_REPORT" ]
then
for element in "${array[@]}"
do
#       echo $element
	element=`echo $element|sed 's/[- %/.]//g'`
    grep -i "\b$element\b" /tmp/${table_name}_temp.txt
        if [ $? -ne 0 ]
        then
        echo "$element string," >> /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl
        else
        echo `grep -i "\b$element\b" /tmp/${table_name}_temp.txt|sed 's/|//g'` >> /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl
fi
done
else
for element in "${array[@]}"
do
#       echo $element
    grep -i "\b$element\b" /tmp/${table_name}_temp.txt
        if [ $? -ne 0 ]
        then
        echo "$element string," >> /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl
        else
        echo `grep -i "\b$element\b" /tmp/${table_name}_temp.txt|sed 's/|//g'` >> /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl
fi
done
fi
last_char=`tail -c 2 /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl`
if [ $last_char=="," ]
then
#sed -i 's/`tail -c 2 ./${table_name}.ddl`//g' ./${table_name}.ddl
sed -i '$ s/.$//g' /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl
echo "Yes"
else
echo "no"
fi
echo ")  PARTITIONED BY (batch_id string,data_date string) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'WITH SERDEPROPERTIES (   \"separatorChar\" = \",\",   \"quoteChar\"     = \"\\\"\",   \"escapeChar\"    = \"\\\\\")STORED AS TEXTFILE LOCATION '/raw/INFOLEASE/db_work/version=0/$table_name' tblproperties (\"skip.header.line.count\"=\"1\") ;msck repair table db_work.INFOLEASE_$table_name;">> /clix/artifacts/clix-datalake/infolease_gold_ingestion/hive/db_work/${table_name}.ddl
else
echo "/datalake/InfoLease/$table_name is EMPTY !!!!!!!!!!!!..SO FLOW WILL FAIL"
fi
}
