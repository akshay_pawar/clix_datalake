
#Funtion to get the parameter file name ie: uat/dev/prod.
function getNamespaceFileForCurrentUser () {

	local CURRENT_USER=`echo $USER`;
	if [ $CURRENT_USER == "nzhdusr" ]; then
	    echo "namespace.dev.properties";
	elif [ $CURRENT_USER == "dm_infra" ]; then
	    echo "namespace.prod.properties";
	elif [ $CURRENT_USER == "nzhdhc1" ]; then
	    echo "namespace.uat.properties";
	else
	    echo "Parameter file is not defined for the user ${CURRENT_USER}. Please check!!";
    fi

}
