import openpyxl as op
import csv
import pandas as pd
import os
import time
import datetime
import subprocess
from decimal import Decimal
import sys

from openpyxl import load_workbook



def run_command(args_list):
        # import subprocess
        print('Running system command: {0}'.format(' '.join(args_list)))
        proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        s_output, s_err = proc.communicate()
        s_return =  proc.returncode
        return s_return, s_output, s_err

curr_batch=sys.argv[1]
partition = '/raw/INDUS/db_work/version=0/INDUS_KPI_DPD/batch_id='+curr_batch

create_dir = run_command(['mkdir','/datalake/DPD_file_bkp/'+curr_batch])
create_tmp_dir = run_command(['mkdir','/tmp/KPI_DPD_csvs/'+curr_batch])



fileList=[]
for root, dirs, files in os.walk("/datalake/DPD_file"):
    for filename in files:
        fileList.append(filename)



def execute(file_name):
    wb=load_workbook('/datalake/DPD_file/'+file_name,data_only=True)
    path = '/datalake/DPD_file/'+file_name
    print(wb.sheetnames)
    E2E=wb['Data']   
    total_cols=E2E.max_column
    total_rows=E2E.max_row
    with open('/tmp/KPI_DPD_csvs/'+curr_batch+'/KPI_DPD.csv', 'w', newline="") as f:
      c=csv.writer(f)
      #fileList=[]
      for i in range(1,total_rows+1):
       fileList=[]
       for j in range(1,total_cols+1):
          cell_obj = E2E.cell(row=i,column=j)
          fileList.append(str(cell_obj.value).replace(",","").replace("\n"," "))
      	  #c.writerow(str(cell_obj.value).replace(",","").replace("\n"," "))
       c.writerow(fileList)
      f.close()
#    for i in wb.sheetnames:
#        if (i=="E2E Tracker"):
#            sh=wb['E2E Tracker']
#            with open('/home/dm_infra/HFS_EE/CSVs/HFS_EE.csv', 'w', newline="") as f: 
#                c = csv.writer(f)
#                for r in sh.rows:
#                    c.writerow([cell.value for cell in r])
#            f.close()
    






for name in fileList:
    if name.endswith(".xlsx"):
        print(name)
        execute(name)
    else:
        print(str(name)+" - This is not xlsx format")


out = run_command(['hadoop', 'fs', '-mkdir', partition])
