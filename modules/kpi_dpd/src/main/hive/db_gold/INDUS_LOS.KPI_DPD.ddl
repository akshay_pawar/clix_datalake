DROP TABLE IF EXISTS db_gold.INDUS_LOS_KPI_DPD; CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_KPI_DPD(
Month string,
Cust_CON_PROJ string,
CustCONProj string,
DIVISION string,
ENTITY string,
Revised_Product string,
Product string,
CUST_NAME string,
System_DPD string,
System_Bucket string,
Revised_DPD string,
Bucket string,
Bucket_2 string,
FIN_NEA string,
AGREEMENT_DATE string,
MAT_DT string,
MOB string,
0_plus string,
30_plus string,
90_plus string,
file_load_timestamp string
)
PARTITIONED BY (batch_id string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS  PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_KPI_DPD'
tblproperties ("parquet.compression"="SNAPPY");
