DROP TABLE IF EXISTS db_work.INDUS_LOS_KPI_DPD; CREATE EXTERNAL TABLE IF NOT EXISTS db_work.INDUS_LOS_KPI_DPD(
Month string,
Cust_CON_PROJ string,
CustCONProj string,
DIVISION string,
ENTITY string,
Revised_Product string,
Product string,
CUST_NAME string,
System_DPD string,
System_Bucket string,
Revised_DPD string,
Bucket string,
Bucket_2 string,
FIN_NEA string,
AGREEMENT_DATE string,
MAT_DT string,
MOB string,
0_plus string,
30_plus string,
90_plus string
)
partitioned by (batch_id string) ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' WITH SERDEPROPERTIES (   "separatorChar" = ",",   "quoteChar"     = "\"",   "escapeChar"    = "\\n") STORED AS TEXTFILE LOCATION '/raw/INDUS/db_work/version=0/INDUS_KPI_DPD'  tblproperties ("skip.header.line.count"="1");
msck repair table db_work.INDUS_LOS_KPI_DPD;
