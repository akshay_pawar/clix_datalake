#!/bin/bash
###############################################################################
#                               General Details                               #
###############################################################################
#                                                                             #
# Name         : EIT-RWD-EVNT-l2.sh                                           #
#                                                                             #
# Description  : This script ingests data from ATG table and puts on L1 layer #
#                                                                             #
# Author       : <KAJAL SINGH>                                                #
#                                                                             #
# Create Date  :                                                              #
#                                                                             #
###############################################################################
#                          Script Environment Setup                           #
###############################################################################

kinit -Vkt /home/dm_infra/dm_infra.keytab dm_infra@CLIXCDH.COM
SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# EXTRACT MODULE HOME FROM SCRIPT PATH
MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"
echo "MODULE_HOME = $MODULE_HOME"

###############################################################################
#                           Import Dependencies                               #
###############################################################################

. ${MODULE_HOME}/../common/bin/import-dependecies.sh
. ${MODULE_HOME}/etc/module.env.properties
. ${MODULE_HOME}/etc/column.list.properties
. ${MODULE_HOME}/../etc/default.env.properties
. ${MODULE_HOME}/../etc/namespace.prod.properties

###############################################################################
#                               Module Start                                  #
###############################################################################

# READING PARAMETERS FROM AZKABAN
fn_read_command_line_parameters "$@"

echo "OWNER : ${OWNER}"
echo "TABLE_NAME : ${TABLE_NAME}"

L1_FLOW_ID=l1_flow_${TABLE_NAME}_${INGESTION_ID}
L1_JOB_ID=l1_job_${TABLE_NAME}

L2_FLOW_ID=l2_flow_${TABLE_NAME}_${INGESTION_ID}
L2_JOB_ID=l2_job_${TABLE_NAME}

echo "L1_FLOW_ID : ${L1_FLOW_ID}"
echo "L1_JOB_ID : ${L1_JOB_ID}"

echo "L2_FLOW_ID : ${L2_FLOW_ID}"
echo "L2_JOB_ID : ${L2_JOB_ID}"

# RUN JOB INSTANCE
fn_log_info "-------------- RUNNING JOB INSTANCE --------------"
RUNNING_JOB_INSTANCE_ID=$(fn_run_job ${L2_FLOW_ID} ${L2_JOB_ID})
fn_check_and_exit_error_response "${RUNNING_JOB_INSTANCE_ID}" "COORDINATOR COMMAND SUCCESSFUL" "COORDINATOR COMMAND FAILED"
fn_log_info "RUNNING_JOB_INSTANCE_ID : $RUNNING_JOB_INSTANCE_ID"




# GET CURRENT RUNNING BATCH
fn_log_info "-------------- GETTING CURRENT RUNNING BATCH --------------"
RUNNING_BATCH_ID_L2=$(fn_get_running_batch_id "${L2_FLOW_ID}")
fn_check_batch_id ${RUNNING_BATCH_ID_L2}


# FETCH LAST_SUCCESSFUL_BATCH OF L1 FLOW
fn_log_info "-------------- FETCHING LAST_SUCCESSFUL_BATCH OF L1 FLOW --------------"
LAST_SUCCESSFUL_L1_BATCH_ID=$(fn_get_last_successful_batch_id ${L1_FLOW_ID} ${L1_JOB_ID})
if [ -z "${LAST_SUCCESSFUL_L1_BATCH_ID}" ] || [ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = "" ] || [[ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = *"error"* ]] || [[ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = *"ERROR"* ]] ;
then
     fn_log_error "LAST_SUCCESSFUL_L1_BATCH_ID : ${LAST_SUCCESSFUL_L1_BATCH_ID}"
     fn_log_error "LAST_SUCCESSFUL_L1_BATCH_ID IS NULL. FIRST RUN L1 FLOW THEN RUN L2 FLOW. THERE IS NO DATA TO LOAD IN GOLD. ABORTING THE JOB !!"
     response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
     fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
     exit 100
else
    fn_log_info "LAST_SUCCESSFUL_L1_BATCH_ID : ${LAST_SUCCESSFUL_L1_BATCH_ID}"
fi


#CREATING PARTITION

fn_log_info "RAW_PATH : ${RAW_PATH}/${TABLE_NAME}_new/batch_id=${LAST_SUCCESSFUL_L1_BATCH_ID}/"

for file_name in /datalake/NEA_REPORT/$TABLE_NAME/*csv;do
fdate=`echo $file_name |sed 's/[^0-9]*//g'`
echo $fdate
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
-e "alter table db_gold.${TABLE_NAME} drop if exists partition(data_date=$fdate)"
done

hdfs dfs -ls -t ${RAW_PATH}/${TABLE_NAME}_new/batch_id=${LAST_SUCCESSFUL_L1_BATCH_ID}/ |   tr -s " "    |    cut -d' ' -f8 | cut -d'/' -f8|grep -Eo '[[:digit:]]{4}[[:digit:]]{2}[[:digit:]]{2}'|while read data_date;do hadoop fs -mkdir ${RAW_PATH}/${TABLE_NAME}_new/batch_id=${LAST_SUCCESSFUL_L1_BATCH_ID}/data_date=${data_date};hadoop fs -mv ${RAW_PATH}/${TABLE_NAME}_new/batch_id=${LAST_SUCCESSFUL_L1_BATCH_ID}/*_${data_date}.csv ${RAW_PATH}/${TABLE_NAME}_new/batch_id=${LAST_SUCCESSFUL_L1_BATCH_ID}/data_date=${data_date}/; done



if [ $? -eq 0 ] ;
then
    fn_log_info "SUCCESSFULLY MOVED DATA TO DATA_DATE PARTITION";
else
    fn_log_error "UNABLE TO MOVE DATA TO DATA_DATE PARTITION";
    response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
    exit 100
fi


#fn_create_work_ddls "$TABLE_NAME"
# EXPOSE A TEMPORARY TABLE AT L1 LOCATION
fn_log_info "-------------- EXPOSING A TEMPORARY TABLE AT L1 LOCATION --------------"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
 --hivevar DB_WORK="${HIVE_DATABASE_NAME_WORK}" \
     --hivevar WORK_TABLE=${TABLE_NAME} \
     -f ${MODULE_HOME}/hive/db_work/${TABLE_NAME}.ddl
if [ $? -eq 0 ] ;
then
    fn_log_info "SUCCESSFULLY EXPOSED TEMP TABLE ${HIVE_DATABASE_NAME_WORK}.${TABLE_NAME} AT L1 LOCATION.";
#  rm ${MODULE_HOME}/hive/db_work/${TABLE_NAME}.ddl
else
    fn_log_error "FAILED TO EXPOSE TEMP TABLE ${HIVE_DATABASE_NAME_WORK}.${OWNER}_${TABLE_NAME} AT L1 LOCATION. ABORTING THE JOB !!";
    response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
#	rm ${MODULE_HOME}/hive/db_work/${TABLE_NAME}.ddl
    exit 100
fi

#FILE INGESTION DATA CHECK
WORK_TABLE_PATH=`beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -e "show create table db_work.${TABLE_NAME}_new"| grep "LOCATION" -A1|grep 'hdfs'|sed "s/'//g"`

fn_log_info "WORK_TABLE_PATH :: $WORK_TABLE_PATH "

ISEMPTY=$(hdfs dfs -count ${WORK_TABLE_PATH}/batch_id=${LAST_SUCCESSFUL_L1_BATCH_ID} | awk '{print $2}')

if [[ $ISEMPTY -eq 0 ]];then
   fn_log_error  "NO NEW FILE TO BE PROCESSED . HENCE EXITING ..."
   exit 102
else
   fn_log_info "HDFS FILE PATH NOT EMPTY"
  NUMBER_OF_FILES=$(hdfs dfs -ls ${WORK_TABLE_PATH}/batch_id=${LAST_SUCCESSFUL_L1_BATCH_ID} | wc -l)
 if [[ $NUMBER_OF_FILES -eq 2 ]];then
          NUMBER_OF_LINES=$(hdfs dfs -cat ${WORK_TABLE_PATH}/batch_id=${LAST_SUCCESSFUL_L1_BATCH_ID} | wc -l)

          if [[ $NUMBER_OF_LINES -eq 1 ]];
            then
              fn_log_error  "NO DATA PRESENT IN FILE . SO BATCH NOT BEING PROCESSED"
               exit 103
             fi
           
fi

fi




# FETCH LAST_SUCCESSFUL_BATCH OF L2 FLOW
file_load_tmst=`date +"%Y-%m-%d %H:%M:%S"`

# drop if date partition already exists


# INSERT DATA INTO L2 TABLE
fn_log_info "-------------- INSERT DATA INTO L2 TABLE --------------"
COL=${TABLE_NAME}_new_COL
hadoop fs -mv /hc/INDUS/hive/db_gold/${TABLE_NAME}_new/* /hc/INDUS/hive/db_gold/${TABLE_NAME}_new_bkp/
echo "COL = $COL"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
 --hivevar DB_WORK="${HIVE_DATABASE_NAME_WORK}" \
     --hivevar WORK_TABLE="${TABLE_NAME}_new" \
     --hivevar DB_GOLD="${HIVE_DATABASE_NAME_GOLD}" \
     --hivevar GOLD_TABLE="${TABLE_NAME}" \
     --hivevar L1_BATCH="${LAST_SUCCESSFUL_L1_BATCH_ID}" \
     --hivevar L2_BATCH="${RUNNING_BATCH_ID_L2}" \
     --hivevar COLUMNS="${!COL}" \
     --hivevar file_load_timestamp="${file_load_tmst}" \
     -f ${MODULE_HOME}/hive/insert_into_full_nea.hql



     if [ $? -ne 0 ];
then
    fn_log_error "FAILED TO PERFORM INSERT INTO TABLE : ${TABLE_NAME} "

    # DROP TEMP TABLE FROM L1 PATH
    fn_log_info "-------------- DROPPING TEMP TABLE FROM L1 PATH --------------"
    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM"  --hivevar DB_WORK="${HIVE_DATABASE_NAME_WORK}" --hivevar WORK_TABLE="${TABLE_NAME}" -f ${MODULE_HOME}/hive/drop_table.hql
    if [ $? -eq 0 ];
    then
        fn_log_info "DROPPED TABLE ${HIVE_DATABASE_NAME_WORK}.${TABLE_NAME} FROM L1 PATH."
    else
        fn_log_warn "FAILED TO DROP TABLE ${HIVE_DATABASE_NAME_WORK}.${TABLE_NAME} FROM L1 PATH."
    fi

    # REVERT MOVE

    # FAIL JOB INSTANCE
    fn_log_info "-------------- MARKING THE JOBINSTANCE FAILED --------------"
    response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 100
else
    fn_log_info "SUCCESSFULLY PERFORMED INSERT INTO TABLE : ${OWNER}_${TABLE_NAME} "
fi

fn_log_info   "MOVING THE SUCCESSFULLY LOADED FILES FROM SOURCE LOCATION: ${FILE_ARCHIVE_PATH}/${TABLE_NAME} TO ARCHIVE LOCATION : ${FILE_ARCHIVE_PATH}/${TABLE_NAME}"

    if [ -d "${FILE_ARCHIVE_PATH}/${TABLE_NAME}" ]; then

     mv ${SOURCE_FILE_PATH}/${TABLE_NAME}/${TABLE_NAME}_*.csv ${FILE_ARCHIVE_PATH}/${TABLE_NAME}
         if [ $? -eq 0 ];
          then
             fn_log_info "SUCCESSFULLY MOVED FILES TO ARCHIVE LOCATION"

          else

          fn_log_info "FAILED TO MOVE THE FILES AS NO FILES PRESENT"

         fi

    else

     mkdir /${FILE_ARCHIVE_PATH}/${TABLE_NAME}

     mv ${SOURCE_FILE_PATH}/${TABLE_NAME}/${TABLE_NAME}_*.csv ${FILE_ARCHIVE_PATH}/${TABLE_NAME}

    fi





#DATA PURGING


# DROP TEMP TABLE FROM L1 PATH
fn_log_info "-------------- DROPPING TEMP TABLE FROM L1 PATH --------------"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
 --hivevar DB_WORK="${HIVE_DATABASE_NAME_WORK}" --hivevar WORK_TABLE="${TABLE_NAME}" -f ${MODULE_HOME}/hive/drop_table.hql
if [ $? -eq 0 ];
then
    fn_log_info "DROPPED TABLE ${HIVE_DATABASE_NAME_WORK}.${TABLE_NAME} FROM L1 PATH."
else
    fn_log_warn "FAILED TO DROP TABLE ${HIVE_DATABASE_NAME_WORK}.${TABLE_NAME} FROM L1 PATH."

fi



# MARK THE JOBINSTANCE SUCCESSFUL
fn_log_info "-------------- MARKING THE JOBINSTANCE SUCCESSFUL --------------"
response=$(fn_successful_running_job_instance "${L2_JOB_ID}")
fn_check_and_exit_error_response "${response}" "The job instance is made successful" "Unable to make the job instance successful"


###############################################################################
#                               Module End                                    #
###############################################################################
