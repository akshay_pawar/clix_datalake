kinit -Vkt /home/dm_infra/dm_infra.keytab dm_infra@CLIXCDH.COM
SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# EXTRACT MODULE HOME FROM SCRIPT PATH
MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"
echo "MODULE_HOME = $MODULE_HOME"

###############################################################################
#                           Import Dependencies                               #
###############################################################################

. ${MODULE_HOME}/../common/bin/import-dependecies.sh
. ${MODULE_HOME}/etc/module.env.properties
. ${MODULE_HOME}/etc/column.list.properties
. ${MODULE_HOME}/../etc/default.env.properties
. ${MODULE_HOME}/../etc/namespace.prod.properties

###############################################################################
#                               Module Start                                  #
###############################################################################

# READING PARAMETERS FROM AZKABAN
fn_read_command_line_parameters "$@"

echo "OWNER : ${OWNER}"
echo "TABLE_NAME : ${TABLE_NAME}"

L1_FLOW_ID=l1_flow_${TABLE_NAME}_${INGESTION_ID}
L1_JOB_ID=l1_job_${TABLE_NAME}

L2_FLOW_ID=l2_flow_${TABLE_NAME}_${INGESTION_ID}
L2_JOB_ID=l2_job_${TABLE_NAME}

echo "L1_FLOW_ID : ${L1_FLOW_ID}"
echo "L1_JOB_ID : ${L1_JOB_ID}"

echo "L2_FLOW_ID : ${L2_FLOW_ID}"
echo "L2_JOB_ID : ${L2_JOB_ID}"

bash ${COMMON_MODULE_HOME}/bin/batch_id_generator.sh ${L1_FLOW_ID}
# RUN JOB INSTANCE
fn_log_info "-------------- RUNNING JOB INSTANCE --------------"
RUNNING_JOB_INSTANCE_ID=$(fn_run_job ${L1_FLOW_ID} ${L1_JOB_ID})
fn_check_and_exit_error_response "${RUNNING_JOB_INSTANCE_ID}" "COORDINATOR COMMAND SUCCESSFUL" "COORDINATOR COMMAND FAILED"
fn_log_info "RUNNING_JOB_INSTANCE_ID : $RUNNING_JOB_INSTANCE_ID"




# GET CURRENT RUNNING BATCH
fn_log_info "-------------- GETTING CURRENT RUNNING BATCH --------------"
RUNNING_BATCH_ID_L1=$(fn_get_running_batch_id "${L1_FLOW_ID}")
fn_check_batch_id ${RUNNING_BATCH_ID_L1}

#CREATING PARTITION

fn_log_info "RAW_PATH : ${RAW_PATH}/${TABLE_NAME}/batch_id=${RUNNING_BATCH_ID_L1}/"
hadoop fs -mkdir ${RAW_PATH}/${TABLE_NAME}_new/batch_id=${RUNNING_BATCH_ID_L1}
hadoop fs -copyFromLocal $SOURCE_FILE_PATH/${TABLE_NAME}/*.csv ${RAW_PATH}/${TABLE_NAME}_new/batch_id=${RUNNING_BATCH_ID_L1}/ 

if [ $? -eq 0 ] ;
then
    fn_log_info "SUCCESSFULLY MOVED DATA TO HDFS RAW LOCATION";
else
    fn_log_error "UNABLE TO MOVE DATA TO HDFS RAW LOCATION";
    response=$(fn_fail_running_job_instance "${L1_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
    exit 100
fi

# MARK THE JOBINSTANCE SUCCESSFUL
fn_log_info "-------------- MARKING THE JOBINSTANCE SUCCESSFUL --------------"
response=$(fn_successful_running_job_instance "${L1_JOB_ID}")
fn_check_and_exit_error_response "${response}" "The job instance is made successful" "Unable to make the job instance successful"

# RUN END_JOB
bash ${COMMON_MODULE_HOME}/bin/end_job.sh "${L1_FLOW_ID}"
