#!/bin/bash
###############################################################################
#                               General Details                               #
###############################################################################
#                                                                             #
# Name         : EIT-RWD-EVNT-l2.sh                                           #
#                                                                             #
# Description  : This script ingests data from ATG table and puts on L1 layer #
#                                                                             #
# Author       : <KAJAL SINGH>                                                #
#                                                                             #
# Create Date  :                                                              #
#                                                                             #
###############################################################################
#                          Script Environment Setup                           #
###############################################################################

kinit -Vkt /home/dm_infra/dm_infra.keytab dm_infra@CLIXCDH.COM
SCRIPT_START_TIME="`date +"%Y-%m-%d %H:%M:%S"`"

# EXTRACT MODULE HOME FROM SCRIPT PATH
#MODULE_HOME="$(dirname "$(dirname "$(readlink -f ${BASH_SOURCE[0]})")")"
MODULE_HOME=/clix/artifacts/clix-datalake/indus_gold_ingestion
echo "MODULE_HOME = $MODULE_HOME"

###############################################################################
#                           Import Dependencies                               #
###############################################################################

. ${MODULE_HOME}/../common/bin/import-dependecies.sh
. ${MODULE_HOME}/etc/module.env.properties
. ${MODULE_HOME}/etc/column.list.properties

###############################################################################
#                               Module Start                                  #
###############################################################################

# READING PARAMETERS FROM AZKABAN
fn_read_command_line_parameters "$@"

echo "OWNER_NAME : ${OWNER_NAME}"
echo "TABLE_NAME : ${TABLE_NAME}"

L1_FLOW_ID=l1_flow_CLIX_LOS_PROD.${TABLE_NAME}_id5_0
L1_JOB_ID=l1_job_CLIX_LOS_PROD.${TABLE_NAME}

L2_FLOW_ID=l2_flow_CLIX_LOS_PROD.${TABLE_NAME}_id5_0
L2_JOB_ID=l2_job_CLIX_LOS_PROD.${TABLE_NAME}

echo "L1_FLOW_ID : ${L1_FLOW_ID}"
echo "L1_JOB_ID : ${L1_JOB_ID}"

echo "L2_FLOW_ID : ${L2_FLOW_ID}"
echo "L2_JOB_ID : ${L2_JOB_ID}"

# RUN JOB INSTANCE
fn_log_info "-------------- RUNNING JOB INSTANCE --------------"
RUNNING_JOB_INSTANCE_ID=$(fn_run_job ${L2_FLOW_ID} ${L2_JOB_ID})
fn_check_and_exit_error_response "${RUNNING_JOB_INSTANCE_ID}" "COORDINATOR COMMAND SUCCESSFUL" "COORDINATOR COMMAND FAILED"
fn_log_info "RUNNING_JOB_INSTANCE_ID : $RUNNING_JOB_INSTANCE_ID"

# GET CURRENT RUNNING BATCH
fn_log_info "-------------- GETTING CURRENT RUNNING BATCH --------------"
RUNNING_BATCH_ID_L2=$(fn_get_running_batch_id "${L2_FLOW_ID}")
fn_check_batch_id ${RUNNING_BATCH_ID_L2}

# EXPOSE A TEMPORARY TABLE AT L1 LOCATION
fn_log_info "-------------- EXPOSING A TEMPORARY TABLE AT L1 LOCATION --------------"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	--hivevar DB_WORK="db_work" \
     --hivevar WORK_TABLE=${OWNER_NAME}_${TABLE_NAME} \
     -f ${MODULE_HOME}/hive/db_work/${OWNER_NAME}.${TABLE_NAME}.ddl
if [ $? -eq 0 ] ;
then
    fn_log_info "SUCCESSFULLY EXPOSED TEMP TABLE db_work.${OWNER_NAME}_${TABLE_NAME} AT L1 LOCATION.";
else
    fn_log_error "FAILED TO EXPOSE TEMP TABLE db_work.${OWNER_NAME}_${TABLE_NAME} AT L1 LOCATION. ABORTING THE JOB !!";
    response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
    exit 100
fi

# FETCH LAST_SUCCESSFUL_BATCH OF L2 FLOW
fn_log_info "-------------- FETCHING LAST_SUCCESSFUL_BATCH OF L2 FLOW --------------"
LAST_SUCCESSFUL_L2_BATCH_ID=$(fn_get_last_successful_batch_id ${L2_FLOW_ID} ${L2_JOB_ID})
if [ -z "${LAST_SUCCESSFUL_L2_BATCH_ID}" ] || [ "${LAST_SUCCESSFUL_L2_BATCH_ID}" = "" ] || [[ "${LAST_SUCCESSFUL_L2_BATCH_ID}" = *"error"* ]] || [[ "${LAST_SUCCESSFUL_L2_BATCH_ID}" = *"ERROR"* ]] ;
then
     fn_log_error "LAST_SUCCESSFUL_L2_BATCH_ID : ${LAST_SUCCESSFUL_L2_BATCH_ID}"
     fn_log_error "LAST_SUCCESSFUL_L2_BATCH_ID IS NULL ...!! THERE IS NO DATA TO LOAD IN HISTORY !!"
else
    fn_log_info "LAST_SUCCESSFUL_L2_BATCH_ID : ${LAST_SUCCESSFUL_L2_BATCH_ID}"
fi




# FETCH LAST_SUCCESSFUL_BATCH OF L1 FLOW
fn_log_info "-------------- FETCHING LAST_SUCCESSFUL_BATCH OF L1 FLOW --------------"
LAST_SUCCESSFUL_L1_BATCH_ID=$(fn_get_last_successful_batch_id ${L1_FLOW_ID} ${L1_JOB_ID})
if [ -z "${LAST_SUCCESSFUL_L1_BATCH_ID}" ] || [ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = "" ] || [[ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = *"error"* ]] || [[ "${LAST_SUCCESSFUL_L1_BATCH_ID}" = *"ERROR"* ]] ;
then
     fn_log_error "LAST_SUCCESSFUL_L1_BATCH_ID : ${LAST_SUCCESSFUL_L1_BATCH_ID}"
     fn_log_error "LAST_SUCCESSFUL_L1_BATCH_ID IS NULL. FIRST RUN L1 FLOW THEN RUN L2 FLOW. THERE IS NO DATA TO LOAD IN GOLD. ABORTING THE JOB !!"
     response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
     fn_check_and_exit_error_response "${response}" "THE JOB INSTANCE IS MADE FAILED" "UNABLE TO MAKE THE JOB INSTANCE FAILED"
     exit 100
else
    fn_log_info "LAST_SUCCESSFUL_L1_BATCH_ID : ${LAST_SUCCESSFUL_L1_BATCH_ID}"
fi

file_load_tmst=`date +"%Y-%m-%d %H:%M:%S"`

# INSERT DATA INTO L2 TABLE
fn_log_info "-------------- INSERT DATA INTO L2 TABLE --------------"
COL=${OWNER_NAME}_${TABLE_NAME}_COL
echo "COL = $COL"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	--hivevar DB_WORK="db_work" \
     --hivevar WORK_TABLE="${OWNER_NAME}_${TABLE_NAME}" \
     --hivevar DB_GOLD="db_gold" \
     --hivevar GOLD_TABLE="${OWNER_NAME}_${TABLE_NAME}" \
     --hivevar L1_BATCH="${LAST_SUCCESSFUL_L1_BATCH_ID}" \
     --hivevar L2_BATCH="${RUNNING_BATCH_ID_L2}" \
     --hivevar COLUMNS="${!COL}" \
     --hivevar file_load_timestamp="${file_load_tmst}" \
     -f ${MODULE_HOME}/hive/insert_into_full.hql
if [ $? -ne 0 ];
then
    fn_log_error "FAILED TO PERFORM INSERT INTO TABLE : ${OWNER_NAME}_${TABLE_NAME} "

    # DROP TEMP TABLE FROM L1 PATH
    fn_log_info "-------------- DROPPING TEMP TABLE FROM L1 PATH --------------"
    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	--hivevar DB_WORK="db_work" \
	--hivevar WORK_TABLE="${OWNER_NAME}_${TABLE_NAME}" \
	-f ${MODULE_HOME}/hive/drop_table.hql
    if [ $? -eq 0 ];
    then
        fn_log_info "DROPPED TABLE db_work.${OWNER_NAME}_${TABLE_NAME} FROM L1 PATH."
    else
        fn_log_warn "FAILED TO DROP TABLE db_work.${OWNER_NAME}_${TABLE_NAME} FROM L1 PATH."
    fi

    # REVERT MOVE
    fn_log_info "-------------- REVERT MOVE --------------"
    hdfs dfs -mv ${history_table_path}/batch_id=${LAST_SUCCESSFUL_L2_BATCH_ID} ${gold_table_path}/
    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
	-e "use db_gold; ALTER TABLE ${OWNER_NAME}_${TABLE_NAME}_history DROP IF EXISTS PARTITION(batch_id=${LAST_SUCCESSFUL_L2_BATCH_ID})"

    # FAIL JOB INSTANCE
    fn_log_info "-------------- MARKING THE JOBINSTANCE FAILED --------------"
    response=$(fn_fail_running_job_instance "${L2_JOB_ID}")
    fn_check_and_exit_error_response "${response}" "The job instance is made failed" "Unable to make the job instance failed"
    exit 100
else
    fn_log_info "SUCCESSFULLY PERFORMED INSERT INTO TABLE : ${OWNER_NAME}_${TABLE_NAME} "
	fn_log_info "REMOVING OLD BATCH IDS FROM HDFS"
        hadoop fs -rm -r /hc/INDUS/hive/db_gold/INDUS_LOS_V_CLIX_NEA_REPORT/batch_id=${LAST_SUCCESSFUL_L2_BATCH_ID}
	 fn_log_info "CSV DUMP STARTED"
	 hadoop fs -mv /tmp/NEA_data/batch_id=${LAST_SUCCESSFUL_L2_BATCH_ID} /tmp/NEA_data_bkp
        /usr/bin/spark-submit /home/dm_infra/nea_export_csv.py ${RUNNING_BATCH_ID_L2}
        csvdate=`date +"%Y%m%d"`
        hadoop fs -copyToLocal /tmp/NEA_data/batch_id=${RUNNING_BATCH_ID_L2}/part-00000 /home/dm_infra/nea_csv/NEA_REPORT_${csvdate}.csv
	cp /home/dm_infra/nea_csv/NEA_REPORT_${csvdate}.csv /datalake/NEA_TEST_CSV
        fn_log_info "PLEASE CHECK  CSV AT /datalake/NEA_TEST_CSV "
	day=`date +%e`
        if [ $day == '1' ];
        then
                fn_log_info " ------------------ TODAY IS FIRST DAY OF THE MONTH .. HENCE MOVING DATA INTO HISTORY TABLE ALSO"
                hadoop fs -cp /hc/INDUS/hive/db_gold/INDUS_LOS_V_CLIX_NEA_REPORT/batch_id=${RUNNING_BATCH_ID_L2} /hc/INDUS/hive/db_gold/INDUS_LOS_V_CLIX_NEA_REPORT_history
                if [ $? -ne 0 ];
                then
                    fn_log_error " ####################### FAILED TO MOVE DATA INTO HISTORY TABLE : ${OWNER_NAME}_${TABLE_NAME}_HISTORY ############################"
                else
                    fn_log_info " ########################## SUCCESSFULLY MOVED DATA INTO HISTORY TABLE : ${OWNER_NAME}_${TABLE_NAME}_HISTORY ##########################"
                    beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" -n dm_infra -e "msck repair table db_gold.INDUS_LOS_V_CLIX_NEA_REPORT_history"
                fi
        fi

fi


# DROP TEMP TABLE FROM L1 PATH
fn_log_info "-------------- DROPPING TEMP TABLE FROM L1 PATH --------------"
beeline --verbose=true -u "jdbc:hive2://clixprgndccms02.clix.com:10000/default;principal=hive/clixprgndccms02.clix.com@CLIXCDH.COM" \
 --hivevar DB_WORK="db_work" \
 --hivevar WORK_TABLE="${OWNER_NAME}_${TABLE_NAME}" \
 -f ${MODULE_HOME}/hive/drop_table.hql
if [ $? -eq 0 ];
then
    fn_log_info "DROPPED TABLE db_work.${OWNER_NAME}_${TABLE_NAME} FROM L1 PATH."
else
    fn_log_warn "FAILED TO DROP TABLE db_work.${OWNER_NAME}_${TABLE_NAME} FROM L1 PATH."
fi

# MARK THE JOBINSTANCE SUCCESSFUL
fn_log_info "-------------- MARKING THE JOBINSTANCE SUCCESSFUL --------------"
response=$(fn_successful_running_job_instance "${L2_JOB_ID}")
fn_check_and_exit_error_response "${response}" "The job instance is made successful" "Unable to make the job instance successful"



###############################################################################
#                               Module End                                    #
###############################################################################
