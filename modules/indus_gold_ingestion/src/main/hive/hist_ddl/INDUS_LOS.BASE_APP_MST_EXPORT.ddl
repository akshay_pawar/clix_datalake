DROP TABLE IF EXISTS db_gold.INDUS_LOS_BASE_APP_MST_EXPORT_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_BASE_APP_MST_EXPORT_history ( SZORG_ID string, SZTYPE string, SZTYPEDEFINEDFOR string, SZDESCRIPTION string, SZMODIFIED_BY string, DTMODIFIED_ON string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_BASE_APP_MST_EXPORT_history'
 
tblproperties ("parquet.compression"="SNAPPY")
