DROP TABLE IF EXISTS db_gold.INDUS_LOS_MST_LANG_TRANSLATION_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_MST_LANG_TRANSLATION_history ( SZLANGUAGEKEY string, SZCAPTION string, SZENCODEDCAPTION string, SZCREATEDBY string, DTCREATEDON string, SZUPDATEDBY string, DTUPDATEDON string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_MST_LANG_TRANSLATION_history'
 
tblproperties ("parquet.compression"="SNAPPY")
