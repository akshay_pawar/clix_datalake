DROP TABLE IF EXISTS db_gold.INDUS_LOS_SD_XREF_CONTROLEVENTS_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_SD_XREF_CONTROLEVENTS_history ( SZORGID string, SZCONTROLID string, SZEVENTID string, SZEVENTNAME string, SZFUNCTIONNAME string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_SD_XREF_CONTROLEVENTS_history'
 
tblproperties ("parquet.compression"="SNAPPY")
