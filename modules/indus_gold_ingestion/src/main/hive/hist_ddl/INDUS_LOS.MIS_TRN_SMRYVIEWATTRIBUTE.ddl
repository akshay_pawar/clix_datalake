DROP TABLE IF EXISTS db_gold.INDUS_LOS_MIS_TRN_SMRYVIEWATTRIBUTE_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_MIS_TRN_SMRYVIEWATTRIBUTE_history ( SZORGID string, SZSMRYVIEWID string, SZATTRIBUTEID string, CTYPE string, IORDER decimal(10,0), SZUSERID string, DTUPDTIMESTAMP string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_MIS_TRN_SMRYVIEWATTRIBUTE_history'
 
tblproperties ("parquet.compress"="SNAPPY")
