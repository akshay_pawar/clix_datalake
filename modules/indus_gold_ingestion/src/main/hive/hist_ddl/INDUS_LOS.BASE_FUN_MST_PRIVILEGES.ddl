DROP TABLE IF EXISTS db_gold.INDUS_LOS_BASE_FUN_MST_PRIVILEGES_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_BASE_FUN_MST_PRIVILEGES_history ( SZPRIVILEGE_ID string, SZFUNCTION_ID string, SZOBJECT_TYPE_CODE string, SZOBJECT_ID string, IPRIVILEGE_DEPTH_MASK decimal(38,0) , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_BASE_FUN_MST_PRIVILEGES_history'
 
tblproperties ("parquet.compression"="SNAPPY")
