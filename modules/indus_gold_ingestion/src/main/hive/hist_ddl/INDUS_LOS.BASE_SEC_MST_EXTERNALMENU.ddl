DROP TABLE IF EXISTS db_gold.INDUS_LOS_BASE_SEC_MST_EXTERNALMENU_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_BASE_SEC_MST_EXTERNALMENU_history ( SZID string, SZTYPE string, SZPARENTID string, SZCAPTION string, SZPARENTTYPE string, ISEQUENCE decimal(38,0), SZUSERID string, DTUPDTIMESTAMP string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_BASE_SEC_MST_EXTERNALMENU_history'
 
tblproperties ("parquet.compression"="SNAPPY")
