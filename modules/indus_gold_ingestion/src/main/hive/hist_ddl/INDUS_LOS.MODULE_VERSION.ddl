DROP TABLE IF EXISTS db_gold.INDUS_LOS_MODULE_VERSION_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_MODULE_VERSION_history ( SZ_PRODUCT string, SZ_MODULE_NAME string, SZ_VERSION string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_MODULE_VERSION_history'
 
tblproperties ("parquet.compression"="SNAPPY")
