DROP TABLE IF EXISTS db_gold.INDUS_LOS_BASE_SEC_ADT_PASSWORDHISTORY_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_BASE_SEC_ADT_PASSWORDHISTORY_history ( PASSWORD_ID string, SZUSER_ID string, SZPASSWORD string, DTCHANGED_ON string, SZCHANGED_BY string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_BASE_SEC_ADT_PASSWORDHISTORY_history'
 
tblproperties ("parquet.compression"="SNAPPY")
