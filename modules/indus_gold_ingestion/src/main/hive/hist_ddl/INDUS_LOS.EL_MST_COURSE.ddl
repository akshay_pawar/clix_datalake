DROP TABLE IF EXISTS db_gold.INDUS_LOS_EL_MST_COURSE_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_EL_MST_COURSE_history ( SZCOURSEID string, SZORGID string, SZCOURSESHORTNAME string, SZCOURSEDESCRIPTION string, SZCREATEDBY string, DTCREATEDON string, SZUPDATEDBY string, DTUPDATEDON string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_EL_MST_COURSE_history'
 
tblproperties ("parquet.compression"="SNAPPY")
