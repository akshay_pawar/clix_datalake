DROP TABLE IF EXISTS db_gold.INDUS_LOS_M_CIBIL_ACCOUNT_STATUS_history ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_M_CIBIL_ACCOUNT_STATUS_history ( SZ_ORG_CODE string, SZ_CODE string, SZ_DESCRIPTION string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_M_CIBIL_ACCOUNT_STATUS_history'
 
tblproperties ("parquet.compression"="SNAPPY")
