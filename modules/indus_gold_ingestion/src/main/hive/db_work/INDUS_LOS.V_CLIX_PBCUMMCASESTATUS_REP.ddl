DROP TABLE IF EXISTS db_work.INDUS_LOS_V_CLIX_PBCUMMCASESTATUS_REP;CREATE EXTERNAL TABLE IF NOT EXISTS db_work.INDUS_LOS_V_CLIX_PBCUMMCASESTATUS_REP(
APPLICATION_NO string,
APPLICATION_DATE string,
PARTNER_NAME string,
CUST_NAME string,
DOB string,
PAN_NUMBER string,
EMAILID string,
MOBILENO string,
CUST_CITY string,
PIN_CODE string,
LOAN_AMOUNT decimal(38,2),
APPLIEDTENOR decimal(38,2),
PROPOSED_USE string,
ROI decimal(38,2),
PROCESSINGFEE decimal(38,2),
CASE_STATUS string,
IDEN_SCORE decimal(38,2),
UID_SCORE decimal(38,2),
IDEN_STATUS string,
EXPERIAN_SCORE decimal(38,2),
UNDERWRITING_CRITERI_STATUS string,
HUNTER_STATUS string,
DEROG_STATUS string,
DPD_STATUS string,
NEGATIVE_AREA string,
DEDUPE_POSITIVE string,
AML_STATUS string,
PATH_STATUS string,
FF_PARTNER string,
LOS_STATUS string,
DISBURSEMENT_DATE string
)
PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS TEXTFILE LOCATION '/raw/INDUS/db_work/version=0/CLIX_LOS_PROD_V_CLIX_PBCUMMCASESTATUS_REP' ;msck repair table db_work.INDUS_LOS_V_CLIX_PBCUMMCASESTATUS_REP;
