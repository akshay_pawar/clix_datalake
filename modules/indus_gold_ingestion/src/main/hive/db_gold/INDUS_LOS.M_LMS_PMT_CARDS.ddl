DROP TABLE IF EXISTS db_gold.INDUS_LOS_M_LMS_PMT_CARDS ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_M_LMS_PMT_CARDS ( SZ_ORG_CODE string, SZ_CARD_CODE string, SZ_CARD_DESC string, C_ACTIVE string, DT_LASTUPDATED string, DT_USERDATETIME string, SZ_USERID string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_M_LMS_PMT_CARDS'
 
tblproperties ("parquet.compression"="SNAPPY")
