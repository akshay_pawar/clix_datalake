DROP TABLE IF EXISTS db_gold.INDUS_LOS_CBL_TRN_TURF_PT ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_CBL_TRN_TURF_PT ( SZORGID string, IENQRYSRNO decimal(38,0), IRESPSRNO decimal(38,0), ISRNO decimal(5,0), SZTELEPHONENO string, SZTELEPHONEEXT string, SZTELEPHONETYPE string, SZUSERID string, DTLASTUPDATED string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_CBL_TRN_TURF_PT'
 
tblproperties ("parquet.compression"="SNAPPY")
