DROP TABLE IF EXISTS db_gold.INDUS_LOS_TMP_M_LMS_PORTFOLIO ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_TMP_M_LMS_PORTFOLIO ( SZ_PORTFOLIO_CODE string, SZ_PORTFOLIO_DESC string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_TMP_M_LMS_PORTFOLIO'
 
tblproperties ("parquet.compression"="SNAPPY")
