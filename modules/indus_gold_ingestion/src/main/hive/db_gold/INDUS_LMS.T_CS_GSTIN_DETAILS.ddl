DROP TABLE IF EXISTS db_gold.INDUS_LMS_T_CS_GSTIN_DETAILS ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LMS_T_CS_GSTIN_DETAILS ( SZ_ORG_CODE string, SZ_CUSTOMER_NO string, SZ_STATE_CODE string, SZ_GSTIN string, SZ_USERID string, DT_USER_DATETIME string, DT_LAST_UPDATED string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LMS_T_CS_GSTIN_DETAILS'
 
tblproperties ("parquet.compression"="SNAPPY")
