DROP TABLE IF EXISTS db_gold.INDUS_LOS_FNF_TRN_ACTIVITY ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_FNF_TRN_ACTIVITY ( SZORGID string, SZACTIVITYID string, SZREFID string, SZREFTYPE string, SZFUNCTIONID string, SZUSERID string, DTUPDTIMESTAMP string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_FNF_TRN_ACTIVITY'
 
tblproperties ("parquet.compression"="SNAPPY")
