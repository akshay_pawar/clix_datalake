DROP TABLE IF EXISTS db_gold.INDUS_LOS_M_LMS_AUDIT_SECTION ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_M_LMS_AUDIT_SECTION ( SZ_AUDIT_SECTION_CODE string, SZ_DESCRIPTION string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_M_LMS_AUDIT_SECTION'
 
tblproperties ("parquet.compression"="SNAPPY")
