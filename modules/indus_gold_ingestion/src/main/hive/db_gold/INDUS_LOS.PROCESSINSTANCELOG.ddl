DROP TABLE IF EXISTS db_gold.INDUS_LOS_PROCESSINSTANCELOG ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_PROCESSINSTANCELOG ( ID decimal(19,0), END_DATE string, PROCESSID string, PROCESSINSTANCEID decimal(19,0), START_DATE string, OUTCOME string, PARENTPROCESSINSTANCEID decimal(19,0), STATUS decimal(10,0) , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_PROCESSINSTANCELOG'
 
tblproperties ("parquet.compression"="SNAPPY")
