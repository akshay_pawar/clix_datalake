DROP TABLE IF EXISTS db_gold.INDUS_LOS_CBL_MST_RESPONSE_CONFIG ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_CBL_MST_RESPONSE_CONFIG ( ISRNO decimal(10,0), SZSEGMENT string, SZTAG string, SZTABLE string, SZFIELD string, SZDATATYPE string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_CBL_MST_RESPONSE_CONFIG'
 
tblproperties ("parquet.compression"="SNAPPY")
