DROP TABLE IF EXISTS db_gold.INDUS_LOS_BASE_SEC_MST_ROLE_PRIVI_REV ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_BASE_SEC_MST_ROLE_PRIVI_REV ( SZREV_ID string, SZROLE_ID string, REVISION decimal(4,0), CFLAG string, DTCREATED_ON string, SZCREATED_BY string, DTMODIFIED_ON string, SZMODIFIED_BY string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_BASE_SEC_MST_ROLE_PRIVI_REV'
 
tblproperties ("parquet.compression"="SNAPPY")
