DROP TABLE IF EXISTS db_gold.INDUS_LOS_DEPLY_ERR_LOG ;  CREATE EXTERNAL TABLE IF NOT EXISTS db_gold.INDUS_LOS_DEPLY_ERR_LOG ( SRL_NO decimal(38,0), FUNCTION_NAME string, TABLE_NAME string, COLUMN_NAME string, ORA_CODE decimal(38,0), ORA_ERR string, DATETIMESTAMP string , file_load_timestamp STRING ) PARTITIONED BY (batch_id string) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\001' LINES TERMINATED BY '\n' STORED AS PARQUET LOCATION '/hc/INDUS/hive/db_gold/INDUS_LOS_DEPLY_ERR_LOG'
 
tblproperties ("parquet.compression"="SNAPPY")
