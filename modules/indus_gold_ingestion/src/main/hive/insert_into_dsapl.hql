insert overwrite table db_gold.DSAPL_timestamp SELECT ATA.szDisplayApplicationNo,VTSD.szWorkflowDesc,VTSD.szBUDescription,VTSD.dtStartDate,VTSD.dtEndDate,VTSD.dtTimeInQueue,VTSD.szUser_Name,VTSD.szDescription,VTSD.TaskStatus,VTSD.szTaskNameDesc  FROM db_gold.indus_los_V_Task_Summary_Data VTSD,db_gold.indus_los_App_Trn_Application ATA WHERE VTSD.szOrgId = ATA.szOrgId AND VTSD.szField1 = ATA.szApplicationId  AND ATA.szDisplayApplicationNo like '%DP%';

drop temporary function if exists to_map;
create temporary function to_map as 'hivemall.tools.map.UDAFToMap';
insert overwrite table db_gold.indus_los_v_dsapl_eod_rep partition(batch_id=${hivevar:L2_BATCH})
select w.Login_Date,w.DSA_Group,w.DSA_User,w.Sales_Manager,w.Application_Id,w.LAN_Number,w.Customer_Name,w.Age,w.Declared_Income_per_Month,w.Applied_Loan_Amount,w.Applied_Tenure,w.Mobile_Number,w.Current_Address,w.Company_Name,w.Company_Name_When_Others,w.Total_Work_Exp_in_Months,w.Curr_Company_Exp_in_Months,w.Email_Id,w.Approval_Date,w.Disbursal_Date,w.FTR_Status,w.Verified_Income,w.Approved_Loan_Amount,w.Approved_Tenure,w.ROI,w.PF,w.FI_Status,w.FI_Initiation_Date_Time,w.FI_Received_Date_Time,w.FCU_Status,w.Current_System_App_Status,w.FCU_Initiation_Date_Time,w.FCU_Received_Date_Time,w.Hunter_Status,w.IDEN_Address_Score,w.IDEN_PAN_Status,w.Dedupe_Status,w.Bureau_Score,w.Bureau_Name,w.CA_Score,w.CB_Score,w.Risk_Segment,w.Approved_By_Underwriter_Name,w.Deviations_Captured,w.EMI_Amount,w.Fund_Out_Date,w.First_EMI_Date,w.Reject_Reason,w.Perfios_Pass_Fail,w.PAN_Score,w.UID_Score,w.Address_Score,w.UTR_No,w.UTR_Updated,w.Initial_Offer_Amount,w.Obligations_as_per_Bureau,w.Loan_Creation_Date_Time,w.Initial_Offer_Date_Time,w.Revised_Offer_Date_Time,w.BRANCH,w.DOCS_UPLOAD_TIME_DATE,w.Sent_To_RM_By,r1.Await_Detail_Data_Entry_start_time,r1.Await_Applicant_Consent_start_time,r1.Soft_Approval_start_time,r1.Post_Eligib_Under_Rev_start_time,r1.Pending_Disbursement_start_time,r1.Pending_Doc_Upload_start_time,r1.Sales_Clarifications_1_start_time,r1.Sales_Clarifications_2_start_time,r1.Approved_start_time,r1.Additional_Details_Await_start_time,r1.Sales_Clarifications_3_start_time,r1.Fraud_Check_start_time,r1.Doc_Upload_Pending_start_time,r1.Post_Eligib_in_proc_start_time,r1.FI_Stage_start_time,r1.Dedupe_Review_start_time,r1.Pre_disb_Doc_Pending_start_time,r1.Reject_Review_UA_start_time,r1.Await_Detail_Data_Entry_end_time,r1.Await_Applicant_Consent_end_time,r1.Soft_Approval_end_time,r1.Post_Eligib_Under_Rev_end_time,r1.Pending_Disbursement_end_time,r1.Pending_Doc_Upload_end_time,r1.Sales_Clarifications_1_end_time,r1.Sales_Clarifications_2_end_time,r1.Approved_end_time,r1.Additional_Details_Await_end_time,r1.Sales_Clarifications_3_end_time,r1.Fraud_Check_end_time,r1.Doc_Upload_Pending_end_time,r1.Post_Eligib_in_proc_end_time,r1.FI_Stage_end_time,r1.Dedupe_Review_end_time,r1.Pre_disb_Doc_Pending_end_time,r1.Reject_Review_UA_end_time,r1.Clix_Ops_Maker1,r1.Clix_Ops_Maker2,r1.Clix_Ops,"${hivevar:file_load_timestamp}" AS file_load_timestamp
from

(select * from db_work.indus_los_v_dsapl_eod_rep where batch_id = ${hivevar:L1_BATCH}) w

left join

(SELECT szdisplayapplicationno,
 mp['Demographic Details(UA)1'] as Await_Detail_Data_Entry_start_time,
 mp['Dummy Wait (UA)1'] as Await_Applicant_Consent_start_time,
 mp['Dummy Wait (UA)2'] as Soft_Approval_start_time,
 mp['Dummy Wait (UA)3'] as Approved_start_time,
 mp['Dummy Wait (UA)4'] as Additional_Details_Await_start_time,
 mp['Decision (UA)1'] as Post_Eligib_Under_Rev_start_time,
 mp['Disbursement Approval (UA)1'] as Pending_Disbursement_start_time,
 mp['Document(UA)1'] as Pending_Doc_Upload_start_time,
 mp['Document(UA)2'] as Sales_Clarifications_1_start_time,
 mp['Document(UA)3'] as Sales_Clarifications_2_start_time,
 mp['Document(UA)4'] as Pre_disb_Doc_Pending_start_time,
 mp['Document(UA)5'] as Sales_Clarifications_3_start_time,
 mp['Hunter Review(UA)1'] as Fraud_Check_start_time,
 mp['Ops Document(UA)1'] as Doc_Upload_Pending_start_time,
 mp['Perfios Details (UA)1'] as Post_Eligib_in_proc_start_time,
 mp['Verifcation(UA)1'] as FI_Stage_start_time,
 mp['Dedupe Review1'] as Dedupe_Review_start_time,
 mp['Reject Review(UA)1'] as Reject_Review_UA_start_time,
 ops_mp['Clix_Ops_Maker1'] as Clix_Ops_Maker1,
 ops_mp['Clix_Ops_Maker2'] as Clix_Ops_Maker2,
 ops_mp['Clix_Ops1'] as Clix_Ops,
 mp_enddate['Demographic Details(UA)1'] as Await_Detail_Data_Entry_end_time,
 mp_enddate['Dummy Wait (UA)1'] as Await_Applicant_Consent_end_time,
 mp_enddate['Dummy Wait (UA)2'] as Soft_Approval_end_time,
 mp_enddate['Dummy Wait (UA)3'] as Approved_end_time,
 mp_enddate['Dummy Wait (UA)4'] as Additional_Details_Await_end_time,
 mp_enddate['Decision (UA)1'] as Post_Eligib_Under_Rev_end_time,
 mp_enddate['Disbursement Approval (UA)1'] as Pending_Disbursement_end_time,
 mp_enddate['Document(UA)1'] as Pending_Doc_Upload_end_time,
 mp_enddate['Document(UA)2'] as Sales_Clarifications_1_end_time,
 mp_enddate['Document(UA)3'] as Sales_Clarifications_2_end_time,
 mp_enddate['Document(UA)4'] as Pre_disb_Doc_Pending_end_time,
 mp_enddate['Document(UA)5'] as Sales_Clarifications_3_end_time,
 mp_enddate['Hunter Review(UA)1'] as Fraud_Check_end_time,
 mp_enddate['Ops Document(UA)1'] as Doc_Upload_Pending_end_time,
 mp_enddate['Perfios Details (UA)1'] as Post_Eligib_in_proc_end_time,
 mp_enddate['Verifcation(UA)1'] as FI_Stage_end_time,
 mp_enddate['Dedupe Review1'] as Dedupe_Review_end_time,
 mp_enddate['Reject Review(UA)1'] as Reject_Review_UA_end_time
  FROM

(
SELECT szdisplayapplicationno, to_map(desc, dtstartdate) as mp , to_map(ops,szuser_name) as ops_mp , to_map(desc1,dtenddate) as mp_enddate FROM
(
SELECT szdisplayapplicationno,dtstartdate,dtenddate,szuser_name,CONCAT(sztasknamedesc,rnk) as desc, CONCAT(szdescription,rnk2) as ops,CONCAT(sztasknamedesc,rnk3) as desc1
FROM (
select szdisplayapplicationno,dtstartdate,dtenddate,sztasknamedesc,szuser_name,szdescription,
RANK() OVER (partition by szdisplayapplicationno,sztasknamedesc order by dtstartdate) as rnk,
RANK() OVER (partition by szdisplayapplicationno,szdescription order by dtstartdate) as rnk2,
RANK() OVER (partition by szdisplayapplicationno,sztasknamedesc order by dtenddate) as rnk3
from db_gold.DSAPL_timestamp
) a
) b GROUP BY szdisplayapplicationno
) c
)r1

ON w.application_Id == r1.szdisplayapplicationno;

ALTER TABLE db_gold.INDUS_LOS_V_DSAPL_EOD_REP DROP IF EXISTS PARTITION (batch_id < ${hivevar:L2_BATCH}) ;
