set mapred.reduce.tasks=20;
set mapreduce.map.memory.mb=16000;
set mapreduce.map.java.opts=-Xmx32000M;
set mapreduce.reduce.memory.mb=16000;
set mapreduce.reduce.java.opts=-Xmx32000M;
set hive.auto.convert.join=false;
with work_data as 
(
select ${hivevar:ALL_COLUMNS},"${hivevar:file_load_timestamp}" AS file_load_timestamp
from (
SELECT ${hivevar:ALL_COLUMNS},  ROW_NUMBER() over (PARTITION BY ${hivevar:PRIMARY_COLUMNS} ORDER BY batch_id DESC) AS row_num FROM ${hivevar:DB_WORK}.${hivevar:WORK_TABLE} WHERE batch_id > ${hivevar:L1_BATCH}) work_data
where row_num=1
),
only_gold as 
(
SELECT ${hivevar:ALL_COLUMNS_GOLD}, "${hivevar:file_load_timestamp}" AS file_load_timestamp FROM ${hivevar:DB_GOLD}.${hivevar:HIST_TABLE} h WHERE batch_id=${hivevar:L2_BATCH} and h.${hivevar:PRIMARY_COLUMNS} not in (select w.${hivevar:PRIMARY_COLUMNS} from work_data w)
)
INSERT INTO TABLE ${hivevar:DB_GOLD}.${hivevar:GOLD_TABLE} PARTITION (batch_id=${hivevar:CURRENT_L2_BATCH})
select * from work_data
union all
select * from only_gold
