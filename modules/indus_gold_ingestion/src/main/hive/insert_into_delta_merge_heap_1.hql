set mapreduce.map.memory.mb=23312;
set mapreduce.map.java.opts=-Xmx20650M;
set hive.auto.convert.join=false;
with work_data as
(
select szorgid,szdigidocumentid,szfoldername,szdocumentname,szdocumenttype,szfromemail,sztoemail,szfromfax,sztofax,szemailsubject,szemailbody,szsourcechannel,dttimeinsystem,inumberofpages,cprocessedstatusyn,szindexfilerecord,szdomaindocumenttype,szcontentid,szrefcontentid,carchivedyn,szuserlocked,szdocstatus,dtdocstatus,szdocmode,szreasoncode,dtpurgedate,bdocument,cattachmentyn,szcreatedby,dtcreatedon,szupdatedby,dtupdatedon
,"2018-06-14 02:27:09" AS file_load_timestamp
from (
SELECT szorgid,szdigidocumentid,szfoldername,szdocumentname,szdocumenttype,szfromemail,sztoemail,szfromfax,sztofax,szemailsubject,szemailbody,szsourcechannel,dttimeinsystem,inumberofpages,cprocessedstatusyn,szindexfilerecord,szdomaindocumenttype,szcontentid,szrefcontentid,carchivedyn,szuserlocked,szdocstatus,dtdocstatus,szdocmode,szreasoncode,dtpurgedate,bdocument,cattachmentyn,szcreatedby,dtcreatedon,szupdatedby,dtupdatedon
,  ROW_NUMBER() over (PARTITION BY szdigidocumentid
 ORDER BY batch_id DESC) AS row_num FROM db_work.INDUS_LOS_DIGI_TRN_DOCUMENT WHERE batch_id > 1528143050179) work_data
where row_num=1
),
only_gold as
(
SELECT szorgid,h.szdigidocumentid,szfoldername,szdocumentname,szdocumenttype,szfromemail,sztoemail,szfromfax,sztofax,szemailsubject,szemailbody,szsourcechannel,dttimeinsystem,inumberofpages,cprocessedstatusyn,szindexfilerecord,szdomaindocumenttype,szcontentid,szrefcontentid,carchivedyn,szuserlocked,szdocstatus,dtdocstatus,szdocmode,szreasoncode,dtpurgedate,bdocument,cattachmentyn,szcreatedby,dtcreatedon,szupdatedby,dtupdatedon
, "2018-06-14 02:27:09" AS file_load_timestamp FROM db_gold.INDUS_LOS_DIGI_TRN_DOCUMENT_history h WHERE batch_id=1528204789089 and h.szdigidocumentid
 not in (select w.szdigidocumentid
 from work_data w)
)
INSERT INTO TABLE db_gold.INDUS_LOS_DIGI_TRN_DOCUMENT  PARTITION (batch_id=1528923010234)
select * from work_data
union all
select * from only_gold
